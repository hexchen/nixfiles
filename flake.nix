{
  description = "hexchen nixOS config management";

  inputs.nixpkgs.url = "github:hexchen/nixpkgs/nixos-unstable";
  inputs.nix-darwin = {
    url = "github:LnL7/nix-darwin/master";
    inputs.nixpkgs.follows = "/nixpkgs";
  };
  inputs.colmena = {
    url = "github:zhaofengli/colmena/main";
    inputs.nixpkgs.follows = "/nixpkgs";
  };
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.sops-nix = {
    url = "github:Mic92/sops-nix";
    inputs.nixpkgs.follows = "/nixpkgs";
  };
  inputs.home-manager = {
    url = "github:nix-community/home-manager";
    inputs.nixpkgs.follows = "/nixpkgs";
  };

  inputs.nixos-mailserver = {
    url = "gitlab:simple-nixos-mailserver/nixos-mailserver";
    inputs.nixpkgs.follows = "/nixpkgs";
  };

  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };

  inputs.nixos-hardware.url = "github:NixOS/nixos-hardware/master";

  inputs.pnpm2nix.url = "github:TSRBerry/pnpm2nix";
  inputs.pnpm2nix.flake = false;

  inputs.authentik-nix = {
    url = "github:nix-community/authentik-nix";
    inputs.nixpkgs.follows = "/nixpkgs";
  };

  inputs.catppuccin.url = "github:catppuccin/nix";

  inputs.niri-flake = {
    url = "github:sodiboo/niri-flake";
    inputs.nixpkgs.follows = "/nixpkgs";
  };

  nixConfig = {
    extra-substituters = "https://attic.chaoswit.ch/nixfiles";
    extra-trusted-public-keys = "nixfiles:KBWMSJuH8+suFFoddtR9gN/S2uv3GlRH8ZwgWN16jf8=";
  };

  outputs = { self, nixpkgs, nix-darwin, colmena, flake-utils, sops-nix, home-manager, ...
    }@inputs:
    let
      modList = import ./lib/modules.nix;
      specialArgs = {
        modules = modList {
          modulesDir = ./modules;
          importAll = true;
        };
        profiles = modList { modulesDir = ./profiles; };
        users = modList { modulesDir = ./users; };
        overlays = (modList {
          modulesDir = ./overlays;
          importAll = true;
        }) // {
          pnpm2nix = final: prev: (import inputs.pnpm2nix { pkgs = prev; });
        };
        evalDarwinConfig = extraSpecial: system: config:
          nix-darwin.lib.darwinSystem {
            inherit system;
            modules = [
              config

              home-manager.darwinModules.home-manager
            ];

            specialArgs = {
              inherit (specialArgs) modules overlays profiles users evalConfig evalDarwinConfig;
              inherit hosts;
              sources = inputs;
            } // extraSpecial;
          };
        evalConfig = extraSpecial: system: config:
          nixpkgs.lib.nixosSystem {
            inherit system;
            modules = [
              config

              self.nixosModules.network.nftables
              sops-nix.nixosModules.sops
              home-manager.nixosModules.home-manager
              { home-manager.sharedModules = [ sops-nix.homeManagerModules.sops ]; }
            ];
            specialArgs = {
              inherit (specialArgs) modules overlays profiles users evalConfig evalDarwinConfig;
              inherit hosts;
              sources = inputs;
            } // extraSpecial;
          };
      };
      hostsDir = "${./.}/hosts";
      hostNames = with nixpkgs.lib;
        attrNames (filterAttrs (name: type: type == "directory")
          (builtins.readDir hostsDir));
      hostMeta = host:
        if builtins.pathExists "${hostsDir}/${host}/meta.nix" then
          (import "${hostsDir}/${host}/meta.nix")
          (inputs // { inherit (specialArgs) evalConfig evalDarwinConfig; })
        else
          { };
      hostConfig = host:
        nixpkgs.lib.recursiveUpdateUntil (path: lhs: rhs:
          !(builtins.isAttrs lhs && builtins.isAttrs rhs) || rhs == { }) {
            nixosConfiguration = specialArgs.evalConfig { hostname = host; } "x86_64-linux" {
              imports = [ "${hostsDir}/${host}/configuration.nix" ];
              networking.hostName = host;
            };
            deployment = {
              allowLocalDeployment = true;
              targetHost = "${host}.chaoswit.ch";
              targetPort = 54160;
              targetUser = null;
            };
          } (hostMeta host);
      hosts = with nixpkgs.lib;
        listToAttrs
        (map (name: nameValuePair name (hostConfig name)) hostNames);

      colmenaHiveMeta = {
        allowApplyAll = true;
        description = "hexchen's deployment";
        machinesFile = null;
        name = "hive";
      };

      nixosHosts = nixpkgs.lib.filterAttrs (_: c: !(c.meta.darwin or false)) hosts;
      darwinHosts = nixpkgs.lib.filterAttrs (_: c: c.meta.darwin or false) hosts;
    in {
      nixosConfigurations = builtins.mapAttrs (host: config:
        config.nixosConfiguration // {
          meta = hostMeta host;
        }) nixosHosts;

      darwinConfigurations = builtins.mapAttrs (host: config:
        config.darwinConfiguration // {
          meta = config.meta or { };
        }) darwinHosts;

      deploy.nodes = builtins.mapAttrs (host: config: config.deploy) hosts;

      colmenaHive = import ./lib/colmena-compat.nix { inherit nixpkgs hosts colmena colmenaHiveMeta; };

      gitlab-ci-yaml = let
        lib = nixpkgs.lib;
        initScript = [
          ''
            mkdir -p ~/.config/nix
            cat >> ~/.config/nix/nix.conf <<EOF
            experimental-features = nix-command flakes
            EOF
          ''
          "set -eo pipefail"
          "export XDG_CACHE_HOME=$PWD/.cache"
        ];
      in lib.foldl lib.recursiveUpdate {} (lib.mapAttrsToList (hostname: host:
        {
          "eval-${hostname}" = {
            tags = ["nix"];
            needs = [];
            stage = "eval";
            script = initScript ++ [
              ''nix eval --accept-flake-config -v --raw '.#nixosConfigurations."${hostname}".config.system.build.toplevel.drvPath' > drv''
            ];
            cache = {
              key = "nix";
              paths = [".cache/nix"];
            };
            artifacts.paths = ["drv"];
          };
          "build-${hostname}" = {
            tags = ["nix"];
            needs = ["eval-${hostname}"];
            dependencies = ["eval-${hostname}"];
            stage = "build";
            script = initScript ++ [
              ''nix build  --accept-flake-config --print-out-paths '.#nixosConfigurations."${hostname}".config.system.build.toplevel' > out''
              "nix develop .#ci -c attic login hexchen https://attic.chaoswit.ch $ATTIC_TOKEN"
              "nix develop .#ci -c attic push nixfiles $(cat out)"
            ];
            timeout = "6 hours";
            artifacts.paths = ["out"];
          };
        }
      ) self.nixosConfigurations) // {
          stages = ["eval" "build" "push"];
      };

      nixosModules = specialArgs.modules // {
        inherit (specialArgs) profiles users;
      };
      overlays = specialArgs.overlays;
    } // flake-utils.lib.eachSystem ([
      "x86_64-linux"
      "x86_64-darwin"
      "aarch64-linux"
      "aarch64-darwin"
    ]) (system:
    let
      pkgs = (import nixpkgs {
        inherit system;
        overlays = builtins.attrValues inputs.self.overlays;
      });
    in {
        legacyPackages = pkgs;
        devShells.default = pkgs.mkShell {
          buildInputs = [
            colmena.packages.${system}.colmena
            sops-nix.packages.${system}.sops-init-gpg-key
            pkgs.age
            pkgs.sops
            pkgs.cachix
          ];
        };
        devShells.ci = pkgs.mkShell {
          name = "ci-shell";
          buildInputs = [
            pkgs.attic-client
            pkgs.jq
          ];
        };
      });
}
