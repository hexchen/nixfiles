{ config, lib, pkgs, options, ... }:

{
  # this is a huge hack to avoid infinite recursion

  users.users.hsattler = lib.mkMerge (
    (
      map (
        def: def.hexchen or {}
      ) options.users.users.definitions
    )
    ++ [
    {
      uid = lib.mkForce 1001;
      description = lib.mkForce "Hex Sattler";
    }
  ]);

  home-manager.users.hsattler = lib.mkMerge (
    (
      map (
        def: def.hexchen or {}
      ) options.home-manager.users.definitions
    )
    ++ [
      {
        home.stateVersion = "22.05";
        programs.git = {
          userName = lib.mkForce "Hex Sattler";
          # userEmail = lib.mkForce "tbd";
        };
      }
    ]
  );
}
