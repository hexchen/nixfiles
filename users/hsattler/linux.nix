{ config, lib, pkgs, ... }:

{
  home-manager.users.hsattler = {
    wayland.windowManager.sway.config.output."*".bg = lib.mkForce "${./cybert.png} fill";
    wayland.windowManager.sway.config.keybindings."Mod4+x" = lib.mkForce "exec swaylock -i ${./cybert.png} -s fill";

    programs.ssh.matchBlocks."*".identityFile = "~/.ssh/work_ed25519";
  };
}
