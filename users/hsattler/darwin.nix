{ config, lib, pkgs, ... }:

{
  home-manager.users.hsattler = {
    programs.ssh.matchBlocks."*".extraOptions.IdentityAgent = ''"~/Library/Group Containers/2BUA8C4S2C.com.1password/t/agent.sock"'';
  };
  users.users.hsattler.home = lib.mkForce "/Users/hsattler";
}
