{
  base = ./base.nix;
  linux = ./linux.nix;
  darwin = ./darwin.nix;
}
