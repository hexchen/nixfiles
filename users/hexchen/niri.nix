{ config, lib, pkgs, sources, ... }:

{
  imports = [
    sources.niri-flake.nixosModules.niri
  ];

  niri-flake.cache.enable = false;
  programs.niri = {
    enable = true;
    package = pkgs.niri;
  };

  home-manager.users.hexchen = args: let hmconfig = args.config; in {
    programs.niri = {
      settings = {
        prefer-no-csd = true;
        hotkey-overlay.skip-at-startup = true;
        screenshot-path = "~/tmp/screenshots/niri-%Y%m%d-%H%M%S.png";
        input = {
          focus-follows-mouse.enable = true;
          warp-mouse-to-focus = true;
          keyboard.xkb.options = "ctrl:nocaps,compose:sclk";
          touchpad = {
            click-method = "clickfinger";
            dwt = true;
            scroll-method = "two-finger";
            tap = true;
            tap-button-map = "left-right-middle";
          };
        };
        workspaces = lib.listToAttrs (map (name: { inherit name; value = {}; }) [ "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" ]);
        outputs = let
          top = {
            mode.width = 3840;
            mode.height = 2160;
            position.x = 1920;
            position.y = 0;
            scale = 1.5;
          };
          bottom = {
            mode.width = 3840;
            mode.height = 2160;
            position.x = 1920;
            position.y = 1440;
            scale = 1.5;
          };
        in {
          "eDP-1" = {
            mode.width = 1920;
            mode.height = 1080;
            position.x = 0;
            position.y = 1800;
            scale = 1;
          };
          "DP-3" = top;
          "DP-4" = top;
          "DP-5" = bottom;
          "DP-7" = bottom;
        };
        layout = {
          gaps = 4;
          struts = {
            top = 0;
            left = 2;
            bottom = 2;
            right = 2;
          };
          focus-ring = {
            width = 1;
            # catppuccin frappe lavender
            active.color = "rgb(186, 187, 241)";
            # catppuccin frappe overlay0
            inactive.color = "rgb(115, 121, 148)";
          };
        };
        cursor = {
          size = 28;
          theme = "Catppuccin-Frappe-Dark-Cursors";
        };
        environment = {
          SDL_VIDEODRIVER = "wayland";
          MOZ_ENABLE_WAYLAND = toString 1;
          QT_QPA_PLATFORM = "wayland";
          QT_WAYLAND_DISABLE_WINDOWDECORATION = toString 1;
          QT_AUTO_SCREEN_SCALE_FACTOR = toString 1;
          NIXOS_OZONE_WL = toString 1;
          GDK_BACKEND = "wayland";
          DISPLAY = ":0";
        };
        window-rules = [
          {
            matches = [
              { app-id = "1Password"; }
              { app-id = "signal"; }
              { app-id = "Slack"; }
              { app-id = "thunderbird"; }
              { title = "^​Cinny — Mozilla Firefox$"; }
              { title = "^​Telegram Web — Mozilla Firefox$"; }
              { title = "Lix Lix Chat — Mozilla Firefox$"; }
              { title = "Slack — Mozilla Firefox$"; }
            ];
            block-out-from = "screen-capture";
          }
        ];
        binds = with hmconfig.lib.niri.actions; let
          sh = spawn "sh" "-c";
        in {
          "Mod+Return".action.spawn = "${pkgs.kitty}/bin/kitty";

          "Mod+Left".action = focus-column-left;
          "Mod+Right".action = focus-column-right;
          "Mod+Up".action = focus-window-or-workspace-up;
          "Mod+Down".action = focus-window-or-workspace-down;

          "Mod+Shift+Left".action = move-column-left;
          "Mod+Shift+Right".action = move-column-right;
          "Mod+Shift+Up".action = move-column-to-workspace-up;
          "Mod+Shift+Down".action = move-column-to-workspace-down;
          "Mod+Ctrl+Up".action = move-window-up-or-to-workspace-up;
          "Mod+Ctrl+Down".action = move-window-down-or-to-workspace-down;

          "Mod+Shift+F".action = fullscreen-window;
          "Mod+F".action = maximize-column;
          "Mod+Ctrl+F".action = reset-window-height;
          "Mod+R".action = switch-preset-column-width;
          "Mod+C".action = consume-or-expel-window-left;

          "Mod+Shift+Q".action = close-window;

          "Mod+F1".action = show-hotkey-overlay;

          "Mod+J".action = move-workspace-to-monitor-left;
          "Mod+L".action = move-workspace-to-monitor-right;
          "Mod+I".action = move-workspace-to-monitor-up;
          "Mod+K".action = move-workspace-to-monitor-down;
          "Mod+Shift+J".action = move-column-to-monitor-left;
          "Mod+Shift+L".action = move-column-to-monitor-right;
          "Mod+Shift+I".action = move-column-to-monitor-up;
          "Mod+Shift+K".action = move-column-to-monitor-down;


          "Mod+Print".action = screenshot;
          "Mod+Shift+Print".action = screenshot-window;

          "XF86AudioRaiseVolume" = {
            allow-when-locked = true;
            action.spawn = [ "${pkgs.pamixer}/bin/pamixer" "-i" "5" "--allow-boost" ];
          };
          "XF86AudioLowerVolume" = {
            allow-when-locked = true;
            action.spawn = [ "${pkgs.pamixer}/bin/pamixer" "-d" "5" ];
          };
          "XF86AudioMute" = {
            allow-when-locked = true;
            action.spawn = [ "${pkgs.pamixer}/bin/pamixer" "-t" ];
          };
          "XF86AudioMicMute".action.spawn = [ "${pkgs.pamixer}/bin/pamixer" "--default-source" "-t" ];
          "XF86MonBrightnessUp".action.spawn = [ "${pkgs.light}/bin/light" "-A" "5" ];
          "XF86MonBrightnessDown".action.spawn = [ "${pkgs.light}/bin/light" "-U" "5" ];

          "Mod+D".action.spawn = [ "rofi" "-show" "drun" ];
          "Mod+X".action.spawn = [ "swaylock" "-i" (toString ./files/background.jpg) "-s" "fill" ];
          "Mod+Delete".action.spawn = [ "${pkgs.wlogout}/bin/wlogout" ];
        } // (lib.foldl lib.recursiveUpdate { } (map (workspace: with hmconfig.lib.niri.actions; {
          "Mod+${workspace}".action = focus-workspace workspace;
          "Mod+Shift+${workspace}".action = move-window-to-workspace workspace;
        }) [ "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" ]));
      };
    };

    systemd.user.services.waybar.Install.WantedBy = [ "niri.service" ];
    systemd.user.services.mako.Install.WantedBy = [ "niri.service" ];
    systemd.user.services.swayidle.Install.WantedBy = [ "niri.service" ];
    systemd.user.services.swaybg = {
      Unit = {
        PartOf = [ "graphical-session.target" ];
        After = [ "graphical-session-pre.target" ];
      };
      Service.ExecStart = "${pkgs.swaybg}/bin/swaybg -i ${./files/background.jpg} -m fill";
      Service.Restart = "on-failure";
      Install.WantedBy = [ "niri.service" ];
    };
    systemd.user.services.xwayland-satellite = {
      Unit = {
        PartOf = [ "graphical-session.target" ];
        After = [ "graphical-session-pre.target" ];
      };
      Service.ExecStart = "${pkgs.xwayland-satellite}/bin/xwayland-satellite";
      Service.Restart = "on-failure";
      Install.WantedBy = [ "niri.service" ];
    };
  };
}
