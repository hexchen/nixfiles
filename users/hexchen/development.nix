{ config, lib, pkgs, ... }:

{
  # le sigh
  nixpkgs.config.permittedInsecurePackages = [
    "erlang-21.3.8.24"
  ];

  users.users.hexchen.packages = with pkgs; [
    crystal
    shards
    go
    python3
    python3Packages.virtualenv
    pylint
    elixir
    # lfe
    rebar3
    elixir_ls
    erlang
    # erlang-ls
    niv
    nixfmt
    patchelf
    flashrom
    # ifdtool
    # cbfstool
    # nvramtool
    protobuf
    # chromium
    rustup
    llvmPackages_latest.llvm
    llvmPackages_latest.bintools
    llvmPackages_latest.lld
    zlib.out
    kubectl
    cilium-cli
    hubble
    kubernetes-helm
    docker
    gnumake
    probe-rs
  ];
}
