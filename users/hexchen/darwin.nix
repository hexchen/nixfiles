{ config, lib, pkgs, ... }:

{
  users.users.hexchen = {
    createHome = true;
    isHidden = false;
    home = "/Users/hexchen";
    shell = "/run/current-system/sw/bin/fish";

    packages = with pkgs; [
      # _1password
    ];
  };
}
