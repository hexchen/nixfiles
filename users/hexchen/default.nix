rec {
  base = ./base.nix;
  linux = ./linux.nix;
  darwin = ./darwin.nix;
  emacs = ./emacs.nix;
  desktop = ./desktop.nix;
  development = ./development.nix;
  sway = ./sway.nix;
  niri = ./niri.nix;
  server = { imports = [ base development ]; };
  desktopFull = { imports = [ base emacs desktop development sway niri ]; };
}
