{ config, lib, pkgs, modules, overlays, sources, ... }:

let colors = import ./colors.nix;
in {
  imports = [ modules.hh3 ];
  nixpkgs.config = {
    allowUnfree = true;
  };
  nixpkgs.overlays = [ overlays.voc overlays.pnpm2nix ];

  services.tor = {
    enable = true;
    client.enable = true;
    client.dns.enable = true;
    torsocks.enable = true;
  };

  programs._1password-gui.enable = true;
  programs._1password-gui.polkitPolicyOwners = [ "hexchen" ];
  programs._1password.enable = true;

  networking.firewall.allowedTCPPorts = [ 22000 ];
  networking.firewall.allowedUDPPorts = [ 22000 ];

  users.users.hexchen = {
    packages = with pkgs; [
      pulsemixer
      pavucontrol
      firefox
      git
      kitty
      j4-dmenu-desktop
      bemenu
      breeze-qt5
      i3blocks
      mako
      syncplay
      yt-dlp
      wl-clipboard
      mumble
      discord
      #hh3cord
      #hh3cord-canary
      xdg-utils
      slurp
      grim
      libnotify
      thunderbird
      zathura # gnome3.nautilus
      tectonic
      openjdk
      pinentry-qt
      deluge
      darktable
      virt-manager virt-viewer
      ifuse
      chiaki
      remmina
      keepassxc
      qmk
      adoptopenjdk-icedtea-web
      wineWowPackages.stableFull winetricks
      appimage-run
      signal-desktop
      ipmiview
      AusweisApp2
      calibre
      yubikey-manager
      pantheon.pantheon-agent-polkit
      qflipper
      xournalpp
      slack
      ymuse
      obsidian
    ];
    extraGroups = [ "video" "cdrom" "scanner" ];
  };

  home-manager.users.hexchen = hmargs: let
    hmconfig = hmargs.config;
  in {
    imports = [
      sources.catppuccin.homeManagerModules.catppuccin
    ];
    home.file.".gnupg/gpg-agent.conf".text = let
      wrapper = pkgs.writeShellScript "pinentry-bemenu-wrapper" ''
        export BEMENU_OPTS="-m all --fn 'MonoLisa 10' --nb '${colors.black}' --nf '${colors.white}' --sb '${colors.red}' --sf '${colors.white}'"
        exec ${pkgs.pinentry-bemenu}/bin/pinentry-bemenu
      '';
    in ''
      enable-ssh-support
      pinentry-program ${pkgs.pinentry-rofi}/bin/pinentry-rofi
    '';
    sops.age.keyFile = ".age/sops/keys.txt";

    home.file.".gnupg/scdaemon.conf".text = ''
      disable-ccid
    '';

    programs.fish.interactiveShellInit = ''
      export GPG_TTY=(tty)
      gpg-connect-agent /bye
    '';

    catppuccin.flavor = "frappe";
    catppuccin.accent = "mauve";
    catppuccin.pointerCursor = {
      enable = true;
      accent = "dark";
    };
    xdg.enable = true;

    programs.rofi = {
      enable = true;
      catppuccin.enable = true;
      font = "MonoLisa 10";
      terminal = "${pkgs.kitty}/bin/kitty";
    };
    dconf = {
      enable = true;
      settings = {
        "org/gnome/desktop/interface" = {
          color-scheme = "prefer-dark";
        };
      };
    };
    gtk = {
      enable = true;
      font = {
        name = "Fira Sans";
        package = pkgs.fira;
        size = 12;
      };
      iconTheme = {
        name = "elementary";
        package = pkgs.elementary-xfce-icon-theme;
      };
      cursorTheme = {
        name = "Catppuccin-Frappe-Dark-Cursors";
        size = 28;
      };
      gtk2.extraConfig = ''
        gtk-application-prefer-dark-theme=1
      '';
      gtk3.extraConfig.gtk-application-prefer-dark-theme = 1;
      gtk4.extraConfig.gtk-application-prefer-dark-theme = 1;
    };
    programs.kitty = {
      enable = true;
      catppuccin.enable = true;
      font.name = "MonoLisa";
      settings = {
        font_size = "10.0";
        bell_path = toString ./files/bell.wav;
        visual_bell_duration = "0.2";
      } // colors.base16;
    };
    programs.beets = {
      enable = true;
      settings = {
        directory = "~/Music";
        library = "~/.local/share/beets.db";
        plugins =
          lib.concatStringsSep " " [ "bpd" "play" "duplicates" "chroma" ];
        play = {
          command = "${pkgs.mpv}/bin/mpv --no-audio-display";
          warning_threshold = 10000;
        };
      };
    };
    programs.mpv = {
      enable = true;
      catppuccin.enable = true;
      config = {
        script-opts-append = "ytdl_hook-ytdl_path=${pkgs.yt-dlp}/bin/yt-dlp";
        hwdec = "auto-safe";
        vo = "gpu";
        profile = "gpu-hq";
        gpu-context = "wayland";
      };
    };
    programs.hh3 = {
      enable = true;
      modules = [
        "allPresences"
        "alwaysTrust"
        "antiDelete"
        "betterCodeblocks"
        "betterUploadButton"
        "callIdling"
        "callRingingBeat"
        "channelleak"
        "clearUrls"
        "commands"
        "consistentLayout"
        "copyAvatarUrl"
        "crabRave"
        "crashScreen"
        "createEmoji"
        "dblClickEdit"
        "disableF1Help"
        "experiments"
        "fixmentions"
        "guildVideo"
        "hiddenTyping"
        "imageUrls"
        "loadingScreen"
        "noAgeGate"
        "noJoinMessageWave"
        "noReplyChainNag"
        "noconmsg"
        "oldQuote"
        "platformIcons"
        "popoutMutuals"
        "postnet"
        "premiumVideo"
        "pseudoscience"
        "quickDelete"
        "roleTags"
        "sentrynerf"
        "shortlinks"
        "showTag"
        "tardid"
        "ttsLinux"
        "twemojiPatch"
        "typingChannel"
        "unravelMessage"
        "whoJoined"
      ];
      config = {
        modules = {
          antiDelete.options.ignorePluralKit = true;
          experiments.options.staffSettings = true;
          hiddenTyping.options.whenInvisible = true;
          loadingScreen.options.style = "random";
          premiumVideo.options.stereo = true;
        };
      };
    };

    xdg.configFile."swappy/config".text = ''
      [Default]
      save_dir=$HOME/tmp/screenshots
      save_filename_format=swappy-%Y%m%d-%H%M%S.png
      show_panel=false
      line_size=5
      text_size=20
      text_font=sans-serif
      paint_mode=brush
      early_exit=false
      fill_shape=false
    '';

    xdg.configFile."wlogout/layout".text = ''
      {
          "label" : "lock",
          "action" : "swaylock -i ${./files/background.jpg} -s fill",
          "text" : "Lock",
          "keybind" : "x"
      }
      {
          "label" : "logout",
          "action" : "loginctl terminate-user $USER",
          "text" : "Logout",
          "keybind" : "e"
      }
      {
          "label" : "shutdown",
          "action" : "systemctl poweroff",
          "text" : "Shutdown",
          "keybind" : "s"
      }
      {
          "label" : "suspend",
          "action" : "systemctl suspend",
          "text" : "Suspend",
          "keybind" : "u"
      }
      {
          "label" : "reboot",
          "action" : "systemctl reboot",
          "text" : "Reboot",
          "keybind" : "r"
      }
    '';

    services.easyeffects.enable = true;

    # systemd.user.services.bpd = {
    #   Service.ExecStart = "${hmconfig.programs.beets.package}/bin/beet bpd";
    #   Unit.Requires = [ "pipewire.service" ];
    #   Unit.After = [ "graphical-session-pre.target" ];
    #   Unit.PartOf = [ "graphical-session.target" ];
    #   Install.WantedBy = [ "graphical-session.target" ];
    # };

    services.mpd = {
      enable = true;
      musicDirectory = "${hmconfig.home.homeDirectory}/Music";
      network.listenAddress = "any";
      extraConfig = ''
        audio_output {
          type "pipewire"
          name "mpd"
        }
      '';
    };


    services.syncthing.enable = true;
  };

  # allow mpd from tailscale
  networking.firewall.interfaces.tailscale0.allowedTCPPorts = [ 6600 ];

}
