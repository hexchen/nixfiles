{ config, lib, pkgs, ... }:

{
  users.users.hexchen = {
    isNormalUser = true;
    extraGroups = [ "wheel" "systemd-journal" ];
    shell = pkgs.fish;
  };
}
