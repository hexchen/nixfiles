{ config, lib, pkgs, sources, ... }:
{
  users.users.hexchen.packages = [ pkgs.sqlite ];

  home-manager.users.hexchen = {
    # imports = [
    #   sources.nix-doom-emacs.hmModule
    # ];
    # programs.doom-emacs = {
    #   enable = true;
    #   doomPrivateDir = "${./doom.d}";
    #   emacsPackage = pkgs.emacs29;
    #   emacsPackagesOverlay = self: super: {
    #     magit-delta = super.magit-delta.overrideAttrs
    #       (esuper: { buildInputs = esuper.buildInputs ++ [ pkgs.git ]; });
    #     lsp-mode = super.lsp-mode.overrideAttrs
    #       (esuper: { buildInputs = esuper.buildInputs ++ [ pkgs.elixir_ls ]; });
    #   };
    # };
    programs.emacs = {
      enable = true;
      package = pkgs.emacs29-pgtk;
    };

    home.activation.doom = ''
      EMACS="$HOME/.config/emacs"

      if [ ! -d "$EMACS" ]; then
        ${pkgs.git}/bin/git clone https://github.com/hlissner/doom-emacs.git $EMACS
        yes | $EMACS/bin/doom install
      else
        pushd $EMACS
        ${pkgs.git}/bin/git pull --rebase
        popd
      fi
      ${pkgs.rsync}/bin/rsync -r ${./doom.d}/ $HOME/.doom.d
      $EMACS/bin/doom sync || true
    '';
  };
}
