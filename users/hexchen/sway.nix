{ config, pkgs, lib, sources, ... }:

let
  colors = import ./colors.nix;
in {
  fonts.packages = with pkgs; [
    font-awesome
    emacs-all-the-icons-fonts
    nerd-fonts.symbols-only
    source-code-pro
    lato
    fira
    vollkorn
    monolisa
  ];
  users.users.hexchen.packages = with pkgs; [ ];
  programs.sway.enable = true;

  home-manager.users.hexchen = {
    systemd.user.services.mako = {
      Unit = {
        PartOf = [ "graphical-session.target" ];
        After = [ "graphical-session-pre.target" ];
      };
      Service.ExecStart = "${pkgs.mako}/bin/mako";
      Service.Restart = "on-failure";
      Install.WantedBy = [ "sway-session.target" ];
    };

    systemd.user.services.swayidle = {
      Unit = {
        PartOf = [ "graphical-session.target" ];
        After = [ "graphical-session-pre.target" ];
      };
      Service.ExecStart = "${pkgs.swayidle}/bin/swayidle -w before-sleep '${pkgs.swaylock}/bin/swaylock -i ${./files/background.jpg} -s fill'";
      Service.Restart = "on-failure";
      Install.WantedBy = [ "sway-session.target" ];
    };

    services.mako = {
      enable = true;
      catppuccin.enable = true;
      defaultTimeout = 3000;
    };
    wayland.windowManager.sway = {
      enable = true;
      catppuccin.enable = true;
      extraSessionCommands = ''
        export SDL_VIDEODRIVER=wayland

        # Firefox wayland
        export MOZ_ENABLE_WAYLAND=1

        # XDG portal related variables (for screen sharing etc)
        export XDG_SESSION_TYPE=wayland
        export XDG_CURRENT_DESKTOP=sway

        # Run QT programs in wayland
        export QT_QPA_PLATFORM=wayland
        export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
        export QT_AUTO_SCREEN_SCALE_FACTOR=1

        export NIXOS_OZONE_WL=1

        export XCURSOR_SIZE=28
        export GDK_SCALE=1.5
        export GDK_BACKEND=wayland
      '';
      config = let
        dmenu = "rofi -show drun";
          #"${pkgs.bemenu}/bin/bemenu --fn 'MonoLisa 10' --nb '${colors.black}' --nf '${colors.white}' --sb '${colors.red}' --sf '${colors.white}' -l 5 -i";
        lockCommand = "swaylock -i ${./files/background.jpg} -s fill";
        cfg =
          config.home-manager.users.hexchen.wayland.windowManager.sway.config;
      in {
        output = {
          "*".bg = "${./files/background.jpg} fill";
          eDP-1 = {
            pos = "0 1800";
            mode = "1920x1080@60.033Hz";
          };
          "LG Electronics LG HDR 4K 104NTFA7K035" = {
            scale = "1.5";
            pos = "1920 0";
            mode = "3840x2160@60Hz";
          };
          "LG Electronics LG HDR 4K 0x00028000" = {
            scale = "1.5";
            pos = "1920 1440";
            mode = "3840x2160@59.996Hz";
          };
          "Dell Inc. DELL 2007WFP HM06471407WL" = {
            pos = "240 750";
            mode = "1680x1050@59.883Hz";
          };
        };

        input = let
          trackpads = {
            dwt = "enabled";
            tap = "enabled";
            natural_scroll = "enabled";
            middle_emulation = "enabled";
            click_method = "clickfinger";
          };
        in {
          "*" = {
            xkb_layout = "de";
            xkb_options = "ctrl:nocaps,compose:sclk";
          };
          "936:42233:Drop_Planck" = { xkb_layout = "us"; };
          "936:42233:Drop_Planck_Keyboard" = { xkb_layout = "us"; };
          "2:7:SynPS/2_Synaptics_TouchPad" = trackpads;
          "1267:32:Elan_Touchpad" = trackpads;
          "1267:12849:ELAN06A0:00_04F3:3231_Touchpad" = trackpads;
          "2321:21128:XXXX0000:05_0911:5288_Touchpad" = trackpads;
        };

        fonts = {
          names = [ "Fira Sans" ];
          size = 10.0;
        };
        terminal = "${pkgs.kitty}/bin/kitty";
        # TODO: replace with wofi
        menu =
          "${pkgs.rofi-wayland}/bin/rofi -show drun";
        modifier = "Mod4";

        startup = [
          {
            command =
              "${pkgs.swayidle}/bin/swayidle -w before-sleep '${lockCommand}'";
          }
          {
            command =
              "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
          }
          {
            command =
              "xprop -root -f _XWAYLAND_GLOBAL_OUTPUT_SCALE 32c -set _XWAYLAND_GLOBAL_OUTPUT_SCALE 1.5";
          }
          {
            command =
              "dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP";
          }
          {
            command =
              "systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP";
          }
        ];

        window = {
          border = 1;
          titlebar = true;
        };

        bars = [];

        floating.criteria = [
          { class = "1Password"; }
        ];

        keybindings = {
          "${cfg.modifier}+Return" = "exec ${cfg.terminal}";

          "${cfg.modifier}+Left" = "focus left";
          "${cfg.modifier}+Down" = "focus down";
          "${cfg.modifier}+Up" = "focus up";
          "${cfg.modifier}+Right" = "focus right";

          "${cfg.modifier}+Shift+Left" = "move left";
          "${cfg.modifier}+Shift+Down" = "move down";
          "${cfg.modifier}+Shift+Up" = "move up";
          "${cfg.modifier}+Shift+Right" = "move right";

          "${cfg.modifier}+Shift+space" = "floating toggle";
          "${cfg.modifier}+space" = "focus mode_toggle";

          "--locked XF86AudioRaiseVolume" =
            "exec ${pkgs.pamixer}/bin/pamixer -i 5 --allow-boost";
          "--locked XF86AudioLowerVolume" =
            "exec ${pkgs.pamixer}/bin/pamixer -d 5";
          "--locked XF86AudioMute" =
            "exec ${pkgs.pamixer}/bin/pamixer -t";
          "XF86AudioMicMute" =
            "exec ${pkgs.pamixer}/bin/pamixer --default-source -t";
          "--locked XF86MonBrightnessDown" = "exec ${pkgs.light}/bin/light -U 5";
          "--locked XF86MonBrightnessUp" = "exec ${pkgs.light}/bin/light -A 5";
          "${cfg.modifier}+Print" =
            "exec ${pkgs.bash}/bin/bash -c '~/.local/bin/elixiremanager.sh -w'";
          "${cfg.modifier}+Shift+Print" = let
            swappyScript = pkgs.writeShellScript "swappyshot" ''
              ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp)" - | ${pkgs.swappy}/bin/swappy -f -
            '';
          in "exec ${swappyScript}";

          "${cfg.modifier}+d" = "exec ${cfg.menu}";
          "${cfg.modifier}+x" = "exec ${lockCommand}";
          "${cfg.modifier}+Shift+p" = "exec ${pkgs._1password-gui}/bin/1password --toggle";
          "${cfg.modifier}+p" = "exec ${pkgs._1password-gui}/bin/1password --quick-access";

          "${cfg.modifier}+i" = "move workspace to output left";
          "${cfg.modifier}+o" = "move workspace to output right";
          "${cfg.modifier}+b" = "splith";
          "${cfg.modifier}+v" = "splitv";
          "${cfg.modifier}+s" = "layout stacking";
          "${cfg.modifier}+w" = "layout tabbed";
          "${cfg.modifier}+e" = "layout toggle split";
          "${cfg.modifier}+f" = "fullscreen";

          "${cfg.modifier}+Shift+q" = "kill";
          "${cfg.modifier}+Shift+c" = "reload";

          "${cfg.modifier}+r" = "mode resize";
          "${cfg.modifier}+Delete" = "exec ${pkgs.wlogout}/bin/wlogout";
        } // (lib.foldl lib.recursiveUpdate { } (map (workspace: {
          "${cfg.modifier}+${workspace}" = "workspace ${workspace}";
          "${cfg.modifier}+Shift+${workspace}" =
            "move container to workspace ${workspace}";
        }) [ "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" ]));

        keycodebindings = {
          "--no-repeat 107" =
            "exec dbus-send --session --type=method_call --dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.startTalking";
          "--release 107" =
            "exec dbus-send --session --type=method_call --dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.stopTalking";
        };

        gaps.outer = 4;

        colors = {
          focused = {
            childBorder = "$lavender";
            background = "$base";
            text = "$text";
            indicator = "$rosewater";
            border = "$lavender";
          };
          focusedInactive = {
            childBorder = "$overlay0";
            background = "$base";
            text = "$text";
            indicator = "$rosewater";
            border = "$overlay0";
          };
          unfocused = {
            childBorder = "$overlay0";
            background = "$base";
            text = "$text";
            indicator = "$rosewater";
            border = "$overlay0";
          };
          urgent = {
            childBorder = "$peach";
            background = "$base";
            text = "$peach";
            indicator = "$overlay0";
            border = "$peach";
          };
          placeholder = {
            childBorder = "$overlay0";
            background = "$base";
            text = "$text";
            indicator = "$rosewater";
            border = "$overlay0";
          };
          background = "$base";
        };
      };
      wrapperFeatures.gtk = true;
      extraConfig = ''
        seat seat0 xcursor_theme Catppuccin-Frappe-Dark-Cursors 28
        bindswitch --reload --locked lid:on output eDP-1 disable
        bindswitch --reload --locked lid:off output eDP-1 enable
      '';
    };
    programs.waybar = {
      enable = true;
      systemd.enable = true;
      systemd.target = "sway-session.target";
      style = let
        catppucin = pkgs.fetchFromGitHub {
          owner = "catppuccin";
          repo = "waybar";
          rev = "0830796af6aa64ce8bc7453d42876a628777ac68";
          hash = "sha256-9lY+v1CTbpw2lREG/h65mLLw5KuT8OJdEPOb+NNC6Fo=";
        };
      in ''
        @import "${catppucin}/themes/frappe.css";
        ${builtins.readFile ./files/waybar.css}
      '';
      settings = [{
        layer = "top";
        modules-left = [ "sway/workspaces" "sway/mode" ];
        modules-center = [ "clock" "custom/sit" "mpd" ];
        modules-right = [
          "pulseaudio"
          "network"
          "battery"
          "tray"
          "custom/power"
        ];

        battery = {
          states = {
            good = 95;
            warning = 30;
            critical = 15;
          };
          format = "{capacity}% {icon}";
          format-charging = "{capacity}% ";
          format-plugged = "{capacity}% ";
          format-alt = "{time} {icon}";
          format-icons = [ "" "" "" "" "" ];
        };
        network = {
          format-wifi = "{essid} ({signalStrength}%) ";
          format-ethernet = "{ifname} ";
          format-disconnected = "";
          format-alt = "{ifname}: {ipaddr}/{cidr}";
        };
        mpd = {
          format = "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon} {artist} - {album} - {title}";
          format-disconnected = "";
          format-stopped = "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ";
          consume-icons.on = "";
          random-icons.on = "";
          repeat-icons.on = "";
          single-icons.on = "1";
          state-icons.paused = "";
          state-icons.playing = "";
          on-click = "${pkgs.mpc-cli}/bin/mpc toggle";
          on-scroll-down = "${pkgs.mpc-cli}/bin/mpc next";
          on-scroll-up = "${pkgs.mpc-cli}/bin/mpc prev";
          on-click-right = "${pkgs.mpc-cli}/bin/mpc stop";
        };
        clock = {
          tooltip-format = "{:%d. %B %Y (KW%W)}";
        };
        "custom/power" = {
          format = " ";
          on-click = "${pkgs.wlogout}/bin/wlogout";
        };
        "custom/sit" = {
          exec = ''echo -n "@$(( ( ( ( $(date "+%s") + 3600 ) % 86400 ) * 10 ) / 864 ))"'';
          interval = 25;
        };
      }];
    };
  };
}
