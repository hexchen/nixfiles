{ config, lib, pkgs, ... }:

{
  home-manager.users.hexchen = {
    programs.direnv = {
      enable = true;
      nix-direnv.enable = true;
    };
    programs.fish = {
      enable = true;
      shellAliases = {
        icat = "${pkgs.kitty}/bin/kitty +kitten icat --align=left";
        fcurl =
          "curl -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0'";
        fcurlj = "fcurl -H 'Accept: application/json'";
        rot13 = "tr 'A-Za-z' 'N-ZA-Mn-za-m'";
        ops = "eval (op signin)";
        gms = "git checkout (git_default_branch) && git pull && git branch --merged | grep -v (git_default_branch) | xargs git branch -D || true";

        git_default_branch = "git symbolic-ref refs/remotes/origin/HEAD | sed 's@^refs/remotes/origin/@@'";
      };
      functions = {
        sshcd = {
          body = ''ssh -t "$argv[1]" "cd \"$argv[2]\" && exec \$SHELL -l";'';
          description = "ssh and cd";
        };
        fish_right_prompt = {
          body = ''
            if test -n "$nshellpkgs"
              echo -n "("(set_color yellow)nix(set_color white):(set_color green)"$nshellpkgs"(set_color white)")"
              set_color white
            else
              echo -n "("(set_color brblue)sys(set_color white)")"
            end
          '';
          description =
            "Prints the right prompt, containing nix-shell information";
        };
        nex = {
          description = "Execute a command using a nix package";
          body = ''
            argparse -i "f/file=" -- $argv
            if test -z "$_flag_file"
                  set _flag_file "<nixpkgs>"
            end
            nix run -f "$_flag_file" $argv[1] -- $argv[2..-1]
          '';
        };
        nsh = {
          description = "Open a new nix shell with the specified packages";
          body = ''
            set file "<nixpkgs>"
            set -l prevpkgs $nshellpkgs
            set -x nshellpkgs "$nshellpkgs $argv"
            nix shell -f "$file" $argv
            set -x nshellpkgs $prevpkgs
          '';
        };
        qnr = {
          description = "quick system repl";
          body = ''
            begin
              if test -e /etc/set-environment
                bass source /etc/set-environment
              end
              nix repl -f (echo $NIX_PATH | perl -pe 's|.*(/nix/store/.*-source/repl.nix).*|\1|')
            end
          '';
        };
        nre = {
          description = "Opens the nix repl";
          body = ''
            if test $argv[1]
              nix repl $argv
            else if test -e default.nix
              nix repl .
            else
              qnr
            end
          '';
        };
        nev = {
          description = "Evaluates a nix expression";
          body = ''
            argparse -i "f/file=" -- $argv
            set file "$_flag_file"
            if test -z "$file"
              if test -e default.nix
                set file "."
              else
                set file "<nixpkgs>"
              end
            end
            nix eval -f $file $argv
          '';
        };
      };
      interactiveShellInit = ''
        if test ! -n "$SSH_AUTH_SOCK"
          export SSH_AUTH_SOCK=(gpgconf --list-dirs agent-ssh-socket)
        end
        set fish_greeting ""
        bass source /etc/profile
        if test "$TERM" = "xterm-kitty"
          export TERM=xterm-256color
        end
        fish_config theme choose catppuccin
      '';
      plugins = [{
        name = "bass";
        src = pkgs.fetchFromGitHub {
          owner = "edc";
          repo = "bass";
          rev = "d63054b24c2f63aaa3a08fb9ec9d0da4c70ab922";
          sha256 = "0pwci5xxm8308nrb52s5nyxijk0svar8nqrdfvkk2y34z1cg319b";
        };
      }];
    };
    xdg.configFile."fish/themes/catppuccin.theme".source = let
        catppuccin = pkgs.fetchFromGitHub {
          owner = "catppuccin";
          repo = "fish";
          rev = "0ce27b518e8ead555dec34dd8be3df5bd75cff8e";
          hash = "sha256-Dc/zdxfzAUM5NX8PxzfljRbYvO9f9syuLO8yBr+R3qg=";
        };
    in "${catppuccin}/themes/Catppuccin Frappe.theme";

    programs.atuin = {
      enable = true;
      enableFishIntegration = true;
      flags = [ "--disable-up-arrow" ];
      settings = {
        auto_sync = true;
        sync_frequency = "1m";
        sync_address = "https://atuin.chaoswit.ch";
        style = "compact";
      };
    };
  };
}
