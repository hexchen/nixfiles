{ config, lib, pkgs, users, profiles, ... }:

{
  imports = [
    ./hardware.nix
    users.hexchen.base
    profiles.server profiles.nopersist

    ../../services/builder.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostId = "3eee82df";
  networking.hostName = "build01";
  networking.domain = "chaoswit.ch";

  networking.useDHCP = true;
  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.enp1s0.ipv6.addresses = [{
    address = "2a01:4f8:c012:d0f2::1";
    prefixLength = 64;
  }];
  networking.interfaces.enp1s0.ipv6.routes = [{
    address = "::";
    prefixLength = 0;
    via = "fe80::1";
  }];

  hexchen.dns.zones."chaoswit.ch".subdomains = {
    build01.A = [ "188.245.163.57" ];
    build01.AAAA = [ "2a01:4f8:c012:d0f2::1" ];
  };

  services.resolved.enable = lib.mkForce false;
  environment.etc."resolv.conf".text = lib.mkForce ''
    # managed directly by nixos
    options edns0
    nameserver 1.1.1.1
    nameserver 1.0.0.1
  '';

  system.autoUpgrade.allowReboot = true;

  system.stateVersion = "23.05";
}
