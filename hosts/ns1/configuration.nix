{ config, lib, pkgs, users, profiles, ... }:

{
  imports = [
    ./hardware.nix
    profiles.server
    profiles.nopersist

    ../../services/dns
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostId = "fafade06";

  networking.interfaces.ens3.useDHCP = true;
  networking.interfaces.ens3.ipv6.addresses = [{
    address = "2a01:4f9:c010:91fd::1";
    prefixLength = 64;
  }];
  networking.interfaces.ens3.ipv6.routes = [{
    address = "::";
    prefixLength = 0;
    via = "fe80::1";
  }];

  hexchen.dns.zones."chaoswit.ch".subdomains = {
    ns1.A = [ "135.181.24.73" ];
    ns1.AAAA = [ "2a01:4f9:c010:91fd::1" ];
  };

  services.resolved.enable = lib.mkForce false;
  environment.etc."resolv.conf".text = lib.mkForce ''
    # managed directly by nixos
    options edns0
    nameserver 1.1.1.1
    nameserver 1.0.0.1
  '';

  system.autoUpgrade.allowReboot = true;

  system.stateVersion = "23.05";
}
