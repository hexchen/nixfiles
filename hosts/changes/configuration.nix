{ config, lib, pkgs, profiles, users, ... }:

{
  imports = [
    profiles.base.darwin
    users.hexchen.base
    users.hexchen.darwin
    users.hexchen.development
  ];

  services.nix-daemon.enable = true;
  system.checks.verifyBuildUsers = false;

  programs.zsh.enable = true;
  programs.fish.enable = true;

  nixpkgs.config.allowUnfree = true;

  users.users.hexchen.uid = lib.mkOverride 1 501;

  home-manager.users.hexchen.programs.ssh.matchBlocks."*".extraOptions.IdentityAgent = ''"~/Library/Group Containers/2BUA8C4S2C.com.1password/t/agent.sock"'';

  system.stateVersion = 4;
}
