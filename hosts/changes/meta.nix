{ evalDarwinConfig, ... }:

let
  config = evalDarwinConfig {} "aarch64-darwin" ./configuration.nix;
in {
  darwinConfiguration = config;
  # because of reasons...
  nixosConfiguration = {};
  meta.darwin = true;
}

