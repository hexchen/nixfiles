{ config, lib, pkgs, users, profiles, ... }:

{
  imports = [
    ./hardware.nix
    users.hexchen.desktopFull
    profiles.desktop
    profiles.nopersist
  ];

  boot.initrd.systemd.enable = true;
  nixpkgs.config.allowBroken = true; # le sigh
  boot.kernelPackages = pkgs.linuxKernel.packages.linux_6_6;
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  networking.hostId = "83d638df";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  systemd.network.networks."99-ethernet-default-dhcp".dhcpV4Config.ClientIdentifier = "mac";

  users.users.hexchen.hashedPassword = "$2b$05$9/4rV72603aTH8c084nf7ucmDffkxyqk3oJLjN84bKH2UnKJEwVpO";

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    repositoryFile = "/persist/restic/system.repo";
    paths = [ "/home/hexchen" "/persist" ];
    pruneOpts = [ "--keep-hourly 24" "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 12" "--keep-yearly unlimited" ];
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
      RandomizedDelaySec = "30m";
    };
  };

  users.users.hexchen.packages = with pkgs; [
    (wrapOBS {
      plugins = with obs-studio-plugins; [ wlrobs obs-move-transition ];
    })
    steam
  ];

  environment.systemPackages = [ pkgs.vulkan-validation-layers ];
  hardware.opengl.enable = true;



  boot.plymouth.enable = true;
  boot.plymouth.theme = "sphere";
  boot.plymouth.themePackages = [ pkgs.adi1090x-plymouth-themes ];

  services.pipewire.extraConfig.pipewire-pulse."35-rtp-recv"."pulse.cmd" = [
    { cmd = "load-module"; args = "module-rtp-recv"; }
  ];

  networking.firewall.extraInputRules = ''
    ip daddr 224.0.0.56 accept
  '';

  system.stateVersion = "23.11";
}
