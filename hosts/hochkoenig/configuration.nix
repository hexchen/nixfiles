{ config, lib, pkgs, users, profiles, modules, ... }:

{
  imports = [
    ./hardware.nix
    users.hexchen.desktopFull
    profiles.desktop
    profiles.nopersist

    modules.encboot
  ];

  hexchen.encboot = {
    enable = true;
    dataset = "rpool";
    networkDrivers = [ "e1000e" ];
  };

  networking.hostId = "0a6cc0fe";

  nixpkgs.config.allowBroken = true; # le sigh
  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  systemd.network.networks."99-ethernet-default-dhcp".dhcpV4Config.ClientIdentifier = "mac";

  users.users.hexchen.hashedPassword = "$2b$05$iB76wptfWkKPQy7BlLDoUutdDnQYUbCivONl/o1magBNQ937sIrVC";

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    repositoryFile = "/persist/restic/system.repo";
    paths = [ "/home/hexchen" "/persist" "/video" ];
    pruneOpts = [ "--keep-hourly 24" "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 12" "--keep-yearly unlimited" ];
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
      RandomizedDelaySec = "30m";
    };
  };

  system.stateVersion = "23.11";
}
