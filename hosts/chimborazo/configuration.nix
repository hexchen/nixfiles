{ config, pkgs, lib, users, profiles, modules, overlays, sources, ... }:

{
  imports = [
    ./hardware.nix
    users.hexchen.desktopFull
    profiles.desktop
    profiles.nopersist
    modules.nftnat
    modules.safeboot
    sources.nixos-hardware.nixosModules.lenovo-thinkpad-t14-amd-gen1
  ];

  boot.initrd.systemd.enable = true;
  boot.initrd.systemd.emergencyAccess = true;
  boot.loader.grub.enable = false;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.device = "nodev";
  boot.supportedFilesystems = [ "nfs" ];
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
  networking.hostId = "123318d1";
  networking.wireless.interfaces = [ "wlp3s0" ];
  boot.kernelPackages = pkgs.linuxPackages_6_6;
  nixpkgs.config.allowBroken = true;

  boot.loader.safeboot.enable = true;
  boot.loader.safeboot.extraOrder = [ "001B" "0017" "0018" "0019" "001A" "001C" ];

  hardware.bluetooth.enable = true;
  powerManagement.cpuFreqGovernor = "schedutil";
  services.upower.enable = true;

  programs.steam.enable = true;
  programs.wireshark.enable = true;
  programs.wireshark.package = pkgs.wireshark;
  services.fwupd.enable = true;
  users.users.hexchen.packages = with pkgs; [
    prismlauncher
    blender
    lutris # factorio
    (wrapOBS {
      plugins = with obs-studio-plugins; [ wlrobs obs-move-transition ];
    })
  ];

  users.users.hexchen.extraGroups = [ "wireshark" ];
  users.users.hexchen.hashedPassword =
    "$6$GHhUjotgJ/D$j77ekxK9ep9Ggxo9K3ROCaoDrMeeyrKUrCbsZIW.cRJAYMGCCDRRt4cqIUgzyjoq4d5gM0lDXdYseg5YkYM2Y.";
  users.users.root.hashedPassword =
    "$6$K9rb9VDrrt$3NcwGwSeZ560z1wQ.0vj6vB2aOpuUnJQ89TWf6nOHck.9Nluc6OEO4dLj8uRXSx3mNCHNMcwXYz2vR9uW7mpQ.";

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    repositoryFile = "/persist/restic/system.repo";
    paths = [ "/home" "/persist" ];
    pruneOpts = [ "--keep-hourly 24" "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 12" "--keep-yearly unlimited" ];
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
      RandomizedDelaySec = "30m";
    };
  };

  services.udev.extraRules = ''
    SUBSYSTEM=="input", GROUP="input", MODE="0666"
    SUBSYSTEM=="usb", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="006?", MODE:="666", GROUP="plugdev"
    KERNEL=="hidraw", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="006?", MODE:="666", GROUP="plugdev"
    SUBSYSTEM=="usb", ATTRS{idVendor}=="ffff", ATTRS{idProduct}=="1f4?", MODE:="666", GROUP="plugdev"
    KERNEL=="hidraw", ATTRS{idVendor}=="ffff", ATTRS{idProduct}=="1f4?", MODE:="666", GROUP="plugdev"
  '';

  services.lldpd.enable = true;

  services.openvpn.servers.voc.config = ''
    ; general
    client
    dev tun
    topology subnet

    ; connection
    remote 185.106.84.49
    port 1194
    proto udp
    keepalive 10 30
    resolv-retry infinite
    link-mtu 1400
    mssfix 0

    ; crypto
    cipher AES-128-CBC

    ; certificates
    key /persist/etc/openvpn/hexchen.key    # TODO: change me
    ca /persist/etc/openvpn/ca.crt
    cert /persist/etc/openvpn/hexchen.crt   # TODO: change me
    tls-auth /persist/etc/openvpn/ta.key 1

    ; auth
    tls-cipher "DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA"
    tls-client
    auth SHA512
    remote-cert-tls server

    ; logging
    verb 0
  '';

  home-manager.users.hexchen.services.kanshi.profiles.home-desk.outputs = [
    {
      criteria = "eDP-1";
      mode = "1920x1080@60.045Hz";
      position = "2880,2160";
    }
    {
      criteria = "Goldstar Company Ltd LG HDR 4K 0x00008200";
      mode = "3840x2160@60Hz";
      position = "0,0";
    }
    {
      criteria = "Goldstar Company Ltd LG HDR 4K 0x0000B172";
      mode = "3840x2160@60Hz";
      position = "3840,0";
    }
  ];

  services.pipewire.extraConfig.pipewire-pulse."35-rtp-send"."pulse.cmd" = [
    { cmd = "load-module"; args = "module-null-sink sink_name=rtp-sink"; }
    { cmd = "load-module"; args = "module-rtp-send source=rtp-sink.monitor"; }
  ];

  networking.firewall.extraInputRules = ''
    ip daddr 224.0.0.56 udp dport 9875 accept
  '';

  system.stateVersion = "20.09";
}
