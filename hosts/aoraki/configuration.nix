{ config, pkgs, lib, profiles, modules, users, ... }:

{
  imports = [
    profiles.server
    profiles.nopersist
    ./hardware.nix

    ../../services/attic.nix
    ../../services/atuin.nix
    ../../services/auth
    ../../services/codimd.nix
    ../../services/dendrite.nix
    ../../services/mail.nix
    ../../services/pleroma
    ../../services/paperless.nix
    ../../services/railwitch.nix
    ../../services/restic.nix
    ../../services/soju.nix
    ../../services/terraria.nix
    ../../services/torrents.nix

    modules.nftnat
  ];

  boot.initrd.systemd.enable = true;
  boot.kernelParams = [ "video=HDMI-A-1:1920x1080@60:D" ];
  boot.initrd.kernelModules = [ "r8169" "e1000e" ];
  boot.kernelModules = [ "nct6683" ];

  boot.initrd.network = {
    enable = true;
    ssh = {
      enable = true;
      port = 2222;
      authorizedKeys = with lib;
        concatLists (mapAttrsToList (name: user:
          if elem "wheel" user.extraGroups then
            user.openssh.authorizedKeys.keys
          else
            [ ]) config.users.users);
      hostKeys = [ /persist/ssh/encboot_host ];
    };
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."aoraki.amt" = {
    A = [ "192.168.100.48" ];
  };
  hexchen.dns.zones."chaoswit.ch".subdomains."aoraki" = {
    A = [ "91.198.192.208" ];
    AAAA = [ "2001:67c:b54:1::5" ];
  };

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.supportedFilesystems = [ "zfs" ];

  networking.hostName = "aoraki";
  networking.hostId = "f19dd7d0";
  environment.etc.machine-id.text = "6521a1386f77f592382cdca34e374bc1";

  users.users.hexchen.hashedPassword = "$2b$05$QezNEELHaEoOsHR/cryA4.etVeKTs/4qpgsehDWmZA6dhqsOw6lHu";

  networking.nat.externalInterface = "vlan6";
  networking.nat.internalInterfaces = [ "ve-+" ];
  hexchen.nftables.nat.enable = true;
  networking.nameservers = [ "1.1.1.1" "1.0.0.1" ];
  networking.vlans.vlan6 = { id = 6; interface = "eno1"; };
  networking.interfaces.eno1.ipv4.addresses = [{ address = "192.168.100.48"; prefixLength = 24; }];
  networking.interfaces.vlan6.ipv4.addresses = [{ address = "91.198.192.208"; prefixLength = 27; }];
  networking.interfaces.vlan6.ipv4.routes = [{ address = "0.0.0.0"; prefixLength = 0; via = "91.198.192.193"; }];
  networking.interfaces.vlan6.ipv6.addresses = [{ address = "2001:67c:b54:1::5"; prefixLength = 64; }];
  networking.interfaces.vlan6.ipv6.routes = [{ address = "::"; prefixLength = 0; via = "2001:67c:b54:1::1"; }];
  systemd.network.networks."40-eno1".networkConfig.IPv6AcceptRA = false;
  systemd.network.networks."40-vlan6".networkConfig.IPv6AcceptRA = false;

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    environmentFile = "/persist/restic/system.s3creds";
    repository = "b2:tardis-aoraki:tardis";
    paths = [ "/home" "/persist" "/data" ];
    exclude = [
      "/data/torrents"
    ];
    pruneOpts = [ "--keep-hourly 24" "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 3" ];
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
      RandomizedDelaySec = "30m";
    };
  };

  services.nginx.proxyTimeout = "300s";

  networking.firewall.interfaces."tailscale0".allowedTCPPorts =
    [ 445 ];
  networking.firewall.interfaces."tailscale0".allowedUDPPorts =
    [ 445 ];

  services.samba = {
    enable = true;
    openFirewall = false;
    settings = let
      appleCommon = {
         "vfs objects" = "fruit streams_xattr";
         "fruit:metadata" = "stream";
         "fruit:model" = "MacSamba";
         "fruit:posix_rename" = "yes";
         "fruit:zero_file_id" = "yes";
         "fruit:veto_appledouble" = "no";
         "fruit:wipe_intentionally_left_blank_rfork" = "yes";
         "fruit:delete_empty_adfiles" = "yes";
      };
    in {
      global = {
        "guest account" = "nobody";
        "map to guest" = "bad user";
        "smb min protocol" = "SMB3_02";
        "smb encrypt" = "required";
      };
      tardis = {
        path = "/data/backups/tardis";
        browseable = "yes";
        "guest ok" = "no";
        "writable" = "yes";
        "valid users" = "hexchen";
        "fruit:time machine" = "yes";
      } // appleCommon;
      store = {
        path = "/data/store";
        browseable = "yes";
        "guest ok" = "no";
        "writable" = "yes";
        "valid users" = "hexchen";
      } // appleCommon;
    };
  };

  system.stateVersion = "23.11";
}
