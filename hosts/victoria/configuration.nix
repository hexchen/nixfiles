{ config, lib, pkgs, profiles, users, ... }:

{
  imports = [
    ./hardware.nix
    profiles.server profiles.nopersist

    users.hexchen.base
    ../../services/victoriametrics
    ../../services/grafana
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostId = "99ecefd0";

  hexchen.dns.zones."chaoswit.ch".subdomains = {
    victoria.AAAA = [ "2a01:4f8:c2c:188::1" ];
    victoria.A = [ "78.46.200.166" ];
  };

  networking.interfaces.ens3.useDHCP = true;
  networking.interfaces.ens3.ipv6.addresses = [{
    address = "2a01:4f8:c2c:188::1";
    prefixLength = 64;
  }];
  networking.interfaces.ens3.ipv6.routes = [{
    address = "::";
    prefixLength = 0;
    via = "fe80::1";
  }];

  services.resolved.enable = lib.mkForce false;
  services.kresd = {
    enable = true;
    instances = 1;
    extraConfig = ''
      policy.add(policy.all(policy.FLAGS({'NO_CACHE'})))
    '';
  };

  system.autoUpgrade.allowReboot = true;

  system.stateVersion = "23.11";
}
