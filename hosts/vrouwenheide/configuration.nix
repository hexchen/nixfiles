{ config, pkgs, lib, users, profiles, modules, overlays, sources, ... }:

{
  imports = [
    ./hardware.nix
    users.hexchen.desktopFull
    profiles.desktop
    profiles.nopersist
  ];

  boot.initrd.systemd.enable = true;
  boot.initrd.systemd.emergencyAccess = true;
  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.device = "nodev";
  boot.supportedFilesystems = [ "nfs" ];
  boot.initrd.kernelModules = [ "vfio-pci" ];
  nixpkgs.config.allowBroken = true; # le sigh
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
  networking.hostId = "39da75d4";
  environment.etc.machine-id.text = "a857e463df11d758690b9fd1b73dc693";
  boot.extraModprobeConfig = ''
    options iwlwifi disable_11ax=true
  '';

  hardware.bluetooth.enable = true;
  hardware.pulseaudio = {
    package = pkgs.pulseaudioFull;
    zeroconf.discovery.enable = true;
    tcp.enable = true;
  };
  services.upower.enable = true;
  services.hardware.bolt.enable = true;

  programs.wireshark.enable = true;
  programs.wireshark.package = pkgs.wireshark;
  services.fwupd.enable = true;

  users.users.hexchen.extraGroups = [ "wireshark" ];
  users.users.hexchen.hashedPassword =
    "$2b$05$PtdITsk4Kd.LLk2mVAHz.e.DQ8ZWUanwiRee1BB/CiL0N7kGM08/6";

  home-manager.users.hexchen.wayland.windowManager.sway.config.output.DSI-1.res = "1200x1920";
  home-manager.users.hexchen.wayland.windowManager.sway.config.output.DSI-1.scale = "1.1";
  home-manager.users.hexchen.wayland.windowManager.sway.config.output.DSI-1.transform = "90";
  home-manager.users.hexchen.wayland.windowManager.sway.config.input."1:1:AT_Translated_Set_2_keyboard".xkb_layout = "us";

  services.logind.lidSwitch = "suspend";
  services.logind.powerKey = "suspend";

  boot.kernelParams = [ "fbcon=rotate:1" ];

  services.lldpd.enable = true;

  services.resolved.enable = true;
  networking.dhcpcd.enable = false;
  systemd.network.networks."wlp" = {
    matchConfig.name = "wlp*";
    dhcpV4Config.UseDNS = false;
    dhcpV6Config.UseDNS = false;
    ipv6AcceptRAConfig.UseDNS = false;
  };

  boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];

  environment.systemPackages = with pkgs; [ restic rclone ];

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    repositoryFile = "/persist/restic/system.repo";
    paths = [ "/home/hexchen" "/persist" ];
    pruneOpts = [ "--keep-hourly 24" "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 12" "--keep-yearly unlimited" ];
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
      RandomizedDelaySec = "30m";
    };
  };

  hardware.firmware = let
    intelfirmware = (pkgs.runCommandNoCC "n100-firmware" {} ''
      mkdir -p $out/lib/firmware/intel/
      cp ${pkgs.linux-firmware}/lib/firmware/intel/ibt-1040-4150.ddc $out/lib/firmware/intel/ibt-1040-1050.ddc
      cp ${pkgs.linux-firmware}/lib/firmware/intel/ibt-1040-4150.sfi $out/lib/firmware/intel/ibt-1040-1050.sfi
      cp ${pkgs.linux-firmware}/lib/firmware/intel/ibt-1040-4150.ddc $out/lib/firmware/intel/ibt-0040-1050.ddc
      cp ${pkgs.linux-firmware}/lib/firmware/intel/ibt-1040-4150.sfi $out/lib/firmware/intel/ibt-0040-1050.sfi
    ''); #// { meta.priority = 8; };
  in lib.mkBefore [ intelfirmware ];

  system.stateVersion = "23.11";
}
