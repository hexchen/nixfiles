{ lib, modules, overlays, ... }:

{
  imports = [ modules.dns ];
  nixpkgs.overlays = [ overlays.dns ];

  hexchen.dns.zones."chaoswit.ch".subdomains.melibokus.A = [ "192.168.40.3" ];

  # dummy stuff
  fileSystems."/".fsType = "tmpfs";
  boot.loader.grub.enable = false;
}
