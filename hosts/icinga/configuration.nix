{ config, lib, pkgs, profiles, users, ... }:

{
  imports = [
    ./hardware.nix
    profiles.server profiles.nopersist

    users.hexchen.base
    ../../services/icinga2
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostId = "ab14ae05";

  hexchen.dns.zones."chaoswit.ch".subdomains = {
    icinga.AAAA = [ "2a01:4f8:c0c:174d::1" ];
    icinga.A = [ "88.198.184.6" ];
  };

  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.enp1s0.ipv6.addresses = [{
    address = "2a01:4f8:c0c:174d::1";
    prefixLength = 64;
  }];
  networking.interfaces.enp1s0.ipv6.routes = [{
    address = "::";
    prefixLength = 0;
    via = "fe80::1";
  }];

  services.resolved.enable = lib.mkForce false;
  services.kresd = {
    enable = true;
    instances = 1;
    extraConfig = ''
      policy.add(policy.all(policy.FLAGS({'NO_CACHE'})))
    '';
  };

  system.autoUpgrade.allowReboot = true;

  system.stateVersion = "22.11";
}
