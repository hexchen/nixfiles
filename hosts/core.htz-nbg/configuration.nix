{ config, lib, pkgs, users, profiles, ... }:

{
  imports = [
    ./hardware.nix
    users.hexchen.base
    profiles.server profiles.nopersist

    # ../../services/upstream.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostId = "3eee82df";
  networking.hostName = lib.mkForce "core";
  networking.domain = "htz-nbg.chaoswit.ch";

  networking.useDHCP = true;
  networking.interfaces.ens3.useDHCP = true;
  networking.interfaces.ens3.ipv6.addresses = [{
    address = "2a01:4f8:1c1c:5e1e::1";
    prefixLength = 64;
  }];
  networking.interfaces.ens3.ipv6.routes = [{
    address = "::";
    prefixLength = 0;
    via = "fe80::1";
  }];

  hexchen.dns.zones."chaoswit.ch".subdomains."htz-nbg".subdomains = {
    core.A = [ "128.140.6.190" ];
    core.AAAA = [ "2a01:4f8:1c1c:5e1e::1" ];
  };

  services.resolved.enable = lib.mkForce false;
  environment.etc."resolv.conf".text = lib.mkForce ''
    # managed directly by nixos
    options edns0
    nameserver 1.1.1.1
    nameserver 1.0.0.1
  '';

  system.autoUpgrade.allowReboot = true;
  system.autoUpgrade.flake = ''gitlab:hexchen/nixfiles#"core.htz-nbg"'';

  system.stateVersion = "23.05";
}
