{ lib, modules, overlays, ... }:

{
  imports = [ modules.dns ];
  nixpkgs.overlays = [ overlays.dns ];
  hexchen.dns.enable = true;
  hexchen.dns.xfrIPs = [ "216.218.130.2" "2001:470:100::2" "216.218.133.2" "2001:470:600::2" ];
  hexchen.dns.primary = false;

  # dummy stuff
  fileSystems."/".fsType = "tmpfs";
  boot.loader.grub.enable = false;

  services.knot.enable = lib.mkForce false;
}
