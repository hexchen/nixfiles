{ config, pkgs, lib, users, profiles, modules, overlays, sources, ... }:

{
  imports = [
    ./hardware.nix
    users.hexchen.desktopFull
    profiles.desktop
    profiles.nopersist
    profiles.work
    modules.nftnat

    sources.nixos-hardware.nixosModules.lenovo-thinkpad-t480s
  ];

  boot.initrd.systemd.enable = true;
  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.device = "nodev";
  boot.supportedFilesystems = [ "nfs" ];
  boot.initrd.kernelModules = [ "vfio-pci" ];
  nixpkgs.config.allowBroken = true; # le sigh
  boot.kernelPackages = pkgs.linuxPackages_6_6;
  boot.zfs.allowHibernation = true;
  boot.zfs.forceImportRoot = false;
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
  networking.hostId = "f034bb82";
  #environment.etc.machine-id.text = "655113f7423e4268b02a1b93bcd845d9";
  # networking.wireless.interfaces = [ "wlp61s0" ];

  boot.plymouth.enable = true;
  boot.plymouth.theme = "sphere";
  boot.plymouth.themePackages = [ pkgs.adi1090x-plymouth-themes ];

  hardware.bluetooth.enable = true;
  hardware.pulseaudio = {
    package = pkgs.pulseaudioFull;
    zeroconf.discovery.enable = true;
    tcp.enable = true;
  };
  services.upower.enable = true;
  services.hardware.bolt.enable = true;

  programs.wireshark.enable = true;
  programs.wireshark.package = pkgs.wireshark;
  services.fwupd.enable = true;
  users.users.hexchen.packages = with pkgs; [
    (wrapOBS {
      plugins = with obs-studio-plugins; [ wlrobs obs-move-transition ];
    })
    steam
  ];

  users.users.hexchen.extraGroups = [ "wireshark" ];
  users.users.hexchen.hashedPassword =
    "$6$1mq2DXuf5iEIoJsc$TtbMmwJrjZjLazawXBI9lzLpKLEOLIB.Sm2l3kMplAphT.w1y.XJ4B.9oGuP5MLktzYdG.j8xQ2GFAazuk..O0";

  home-manager.users.hexchen.wayland.windowManager.sway.config.output.eDP-1.res = "2560x1440";
  home-manager.users.hexchen.wayland.windowManager.sway.config.output.eDP-1.scale = "1.2";

  services.logind.lidSwitch = "suspend";

  networking.bridges.lxcbr0.interfaces = [ ];
  networking.interfaces.lxcbr0.ipv4.addresses = [{
    address = "10.53.118.1";
    prefixLength = 24;
  }];

  networking.nftables.extraConfig = ''
    table ip nat {
      chain prerouting {
        type nat hook prerouting priority -100
      }
      chain postrouting {
        type nat hook postrouting priority 100
        iifname lxcbr0 masquerade
      }
    }
  '';
  networking.nftables.forwardPolicy = "drop";
  networking.nftables.extraForward = ''
    iifname lxcbr0 accept
    oifname lxcbr0 accept
  '';
  boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = true;

  virtualisation.lxc.enable = true;
  virtualisation.lxc.systemConfig = ''
    lxc.bdev.zfs.root = rpool/lxc
    lxc.lxcpath = /persist/lxc
  '';
  virtualisation.lxc.usernetConfig = ''
    hexchen veth lxcbr0 10
  '';
  environment.etc."lxc/share".source = "${pkgs.lxc}/share/lxc";

  # boot.kernelParams = [ "systemd.unified_cgroup_hierarchy=0" "amd_pstate=active" "amdgpu.dpm=1" ];

  services.lldpd.enable = true;

  services.resolved.enable = true;
  networking.dhcpcd.enable = false;
  systemd.network.networks."wlp" = {
    matchConfig.name = "wlp*";
    dhcpV4Config.UseDNS = false;
    dhcpV6Config.UseDNS = false;
    ipv6AcceptRAConfig.UseDNS = false;
  };

  boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];

  environment.systemPackages = with pkgs; [ restic rclone ];

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    repositoryFile = "/persist/restic/system.repo";
    paths = [ "/home" "/persist" ];
    pruneOpts = [ "--keep-hourly 24" "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 12" "--keep-yearly unlimited" ];
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
      RandomizedDelaySec = "30m";
    };
  };

  virtualisation.docker.enable = true;

  system.stateVersion = "23.11";
}
