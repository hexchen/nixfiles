{ config, lib, pkgs, profiles, ... }:

{
  imports = [
    profiles.server
    profiles.nopersist
    ./hardware.nix
  ];

  boot.initrd.systemd.enable = true;
  boot.initrd.systemd.emergencyAccess = true;
  boot.initrd.kernelModules = [ "igb" ];
  boot.initrd.network = {
    enable = true;
    ssh = {
      enable = true;
      port = 2222;
      authorizedKeys = with lib;
        concatLists (mapAttrsToList (name: user:
          if elem "wheel" user.extraGroups then
            user.openssh.authorizedKeys.keys
          else
            [ ]) config.users.users);
      hostKeys = [ /persist/ssh/encboot_host ];
    };
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."signalkuppe" = {
    A = [ "116.202.168.17" ];
    AAAA = [ "2a01:4f8:241:703::1" ];
  };

  boot.loader.grub.enable = true;
  boot.loader.grub.devices = [ "/dev/sda" "/dev/sdb" "/dev/sdc" "/dev/sdd" ];
  boot.supportedFilesystems = [ "zfs" ];

  networking.hostName = "signalkuppe";
  networking.hostId = "048d181b";
  environment.etc.machine-id.text = "5cb19bb30fc8c58ddb4f9372f5ab9336";

  networking.nameservers = [ "1.1.1.1" "1.0.0.1" ];
  networking.interfaces.eno1.ipv4.addresses = [{ address = "116.202.168.17"; prefixLength = 26; }];
  networking.interfaces.eno1.ipv4.routes = [{ address = "0.0.0.0"; prefixLength = 0; via = "116.202.168.1"; }];
  networking.interfaces.eno1.ipv6.addresses = [{ address = "2a01:4f8:241:703::1"; prefixLength = 64; }];
  networking.interfaces.eno1.ipv6.routes = [{ address = "::"; prefixLength = 0; via = "fe80::1"; }];

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    environmentFile = "/persist/restic/system.s3creds";
    paths = [
      "/home"
      "/persist"
      "/data"
    ];
    exclude = [
      "/data/torrents"
    ];
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
      RandomizedDelaySec = "60m";
    };
    pruneOpts = [ "--keep-hourly 24" "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 3" ];
    repository = "b2:tardis-signalkuppe:tardis";
  };

  networking.firewall.interfaces.tailscale0.allowedTCPPorts = [ 111 445 2049 4659 ];
  services.nfs.server = {
    enable = true;
    mountdPort = 4659;
    exports = ''
      /data/torrents -crossmnt 100.64.0.0/10(rw) fd7a:115c:a1e0::/48(rw)
    '';
  };
  services.samba = {
    enable = true;
    openFirewall = false;
    settings = let
      appleCommon = {
         "vfs objects" = "fruit streams_xattr";
         "fruit:metadata" = "stream";
         "fruit:model" = "MacSamba";
         "fruit:posix_rename" = "yes";
         "fruit:zero_file_id" = "yes";
         "fruit:veto_appledouble" = "no";
         "fruit:wipe_intentionally_left_blank_rfork" = "yes";
         "fruit:delete_empty_adfiles" = "yes";
      };
    in {
      global = {
        "guest account" = "nobody";
        "map to guest" = "bad user";
        "smb min protocol" = "SMB3_02";
        "smb encrypt" = "required";
      };
      torrents = {
        path = "/data/torrents";
        browseable = "yes";
        "guest ok" = "no";
        "writable" = "yes";
        "valid users" = "hexchen";
      } // appleCommon;
    };
  };

  system.stateVersion = "24.11";
}
