{ config, lib, pkgs, hostname, hosts, ... }:

with lib;

let
  upHosts = {
    osmium = {
      publicKey = "kih/GnR4Bov/DM/7Rd21wK+PFQRUNH6sywVuNKkUAkk=";
      host = "5.199.141.154";
      magicNumber = 8;
    };
    ketchup = {
      publicKey = "ygGp0FG7qjFsikd4Dv/68olRFKIQDdt0xrpcoac8YE8=";
      host = "195.39.221.190";
      magicNumber = 4;
    };
    tralphium = {
      publicKey = "BG2oXzv2qZnQTr+PjUoEMA+vG24g617L3+5TjqHycHk=";
      host = "185.133.208.146";
      magicNumber = 5;
    };
    wob = {
      publicKey = "1BQ79VZ6UCU0TXIT5P4oLrtxqgjI19aA36ITvJQxL1s=";
      host = "45.14.233.180";
      magicNumber = 3;
    };
  };

  mcfg = hosts.${hostname}.meta.wireguard;
in {
  networking.wireguard.interfaces = mapAttrs' (hname: hconf:
    let
      iname = "wgupst_${substring 0 8 hname}";
      magicPort = 50000 + hconf.magicNumber
        + mcfg.magicNumber;
    in nameValuePair iname {
      allowedIPsAsRoutes = false;
      privateKeyFile = config.network.wireguard.keyPath;
      ips = [
        "fe80::1:${toString mcfg.magicNumber}/64"
        "10.22.${
          toString
          (50 + mcfg.magicNumber + hconf.magicNumber)
        }.1/24"
      ];
      listenPort = magicPort;
      peers = [{
        publicKey = hconf.publicKey;
        allowedIPs = [ "0.0.0.0/0" "::/0" ];
        endpoint = "${hconf.host}:${toString magicPort}";
      }];
      postSetup = ''
        wg set ${iname} fwmark 51820
      '';
    }) upHosts;

  networking.firewall.allowedUDPPorts = mapAttrsToList
    (_: hconf: 50000 + hconf.magicNumber + mcfg.magicNumber)
    upHosts;

  systemd.network.wait-online.ignoredInterfaces = mapAttrsToList (hname: hconf: "wgupst_${substring 0 8 hname}") upHosts;

  services.bird2.config = ''
    ${concatStringsSep "\n" (mapAttrsToList (hname: hconf: ''
      protocol bgp upstream_${hname} {
        local as 65489;
        ipv4 { import filter { preference = 100; accept; }; export all; };
        ipv6 { import filter { preference = 100; accept; }; export all; };
        direct;
        source address fe80::1:${toString mcfg.magicNumber};
        neighbor fe80::2:${toString hconf.magicNumber}%wgupst_${
          substring 0 8 hname
        } as 207921;
      }
    '') upHosts)}
  '';

  # TODO: add monitoring here
}
