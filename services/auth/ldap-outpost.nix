{ config, lib, pkgs, sources, ... }:

{
  imports = [ sources.authentik-nix.nixosModules.default ];

  environment.etc."authentik/config.yml" = lib.mkDefault { text = ""; };
  sops.secrets.authentik-ldap = {};
  services.authentik-ldap = {
    enable = true;
    environmentFile = "${config.sops.secrets.authentik-ldap.path}";
  };
}
