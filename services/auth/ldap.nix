{ config, lib, pkgs, ... }:

let
  baseDN = "dc=chaoswit,dc=ch";
in {
  hexchen.dns.zones."chaoswit.ch".subdomains."ldap".CNAME =
    [ "aoraki.chaoswit.ch." ];

  security.acme.certs."ldap.chaoswit.ch" = {
    listenHTTP = ":8080";
    group = "openldap";
    reloadServices = [ "openldap" ];
  };

  services.openldap = {
    enable = true;
    urlList = [ "ldaps:///" ];
    package = pkgs.openldap.overrideAttrs (old: {
      extraContribModules = old.extraContribModules ++ [];
      configureFlags = old.configureFlags ++ [
        "--enable-memberof"
        "--enable-ppolicy"
      ];
    });
    settings = {
      attrs = {
        olcLogLevel = "config";
        olcDisallows = "bind_anon";
        olcTLSCACertificateFile = "${config.security.acme.certs."ldap.chaoswit.ch".directory}/fullchain.pem";
        olcTLSCertificateFile = "${config.security.acme.certs."ldap.chaoswit.ch".directory}/cert.pem";
        olcTLSCertificateKeyFile = "${config.security.acme.certs."ldap.chaoswit.ch".directory}/key.pem";
        olcPasswordCryptSaltFormat = "$6$%.12s";
      };
      children = {
        "cn=module{0}".attrs = {
          objectClass = "olcModuleList";
          olcModuleLoad = [
            "{0}pw-sha2.so"
            "{1}argon2.so"
            "{2}memberof.so"
            "{3}ppolicy.so"
          ];
        };
        "cn=schema".includes = [
          "${config.services.openldap.package}/etc/schema/core.ldif"
          "${config.services.openldap.package}/etc/schema/cosine.ldif"
          "${config.services.openldap.package}/etc/schema/nis.ldif"
          "${config.services.openldap.package}/etc/schema/inetorgperson.ldif"
        ];
        "olcDatabase={-1}frontend".attrs = {
          objectClass = [ "olcDatabaseConfig" "olcFrontendConfig" ];
          olcDatabase = "{-1}frontend";
          olcAccess = [ "{0}to * by dn.exact=uidNumber=0+gidNumber=0,cn=peercred,cn=external,cn=auth manage stop by * none stop" ];
          olcPasswordHash = "{CRYPT}";
        };
        "olcDatabase={0}config".attrs = {
          objectClass = "olcDatabaseConfig";
          olcDatabase = "{0}config";
          olcAccess = [ "{0}to * by * none break" ];
        };
        "olcDatabase={1}mdb" = {
          attrs = {
            objectClass = [ "olcDatabaseConfig" "olcMdbConfig" ];
            olcDatabase = "{1}mdb";
            olcDbDirectory = "/var/lib/openldap/ldap";
            olcDbIndex = [
              "cn pres,eq"
              "dc pres,eq"
              "member pres,eq"
              "memberOf pres,eq"
              "memberUid eq"
              "objectClass eq"
              "uid pres,eq"
            ];
            olcSuffix = baseDN;
            olcRootDN = "uid=root,${baseDN}";
            olcRootPW = "{ARGON2}$argon2id$v=19$m=65536,t=2,p=1$4IwCPKI2LctYJXIhbjynJw$2VJnR+IcFPV7YdXDW08I/2agmcm/zTC9jnVW84nY91I";
            olcAccess = [
              # allow users to authenticate
              ''{0}to dn.one="ou=users,${baseDN}" attrs=userPassword by anonymous auth by * break''
              # admin access
              ''{1}to * by group/groupOfNames/member.exact="ou=sec-ldap-admin,ou=groups,${baseDN}" manage by * break''
              # services
              ''{2}to dn.children="ou=services,${baseDN}" attrs=userPassword by anonymous auth by * break''
              ''{3}to dn.sub="ou=users,${baseDN}" by dn.children="ou=services,${baseDN}" read by * break''
              ''{4}to dn.sub="ou=groups,${baseDN}" by dn.children="ou=services,${baseDN}" read by * break''
            ];
          };
          children = {
            "olcOverlay={0}memberof".attrs = {
              objectClass = [ "olcOverlayConfig" "olcMemberOfConfig" ];
              olcOverlay = "{0}memberof";
              olcMemberOfDangling = "ignore";
              olcMemberOfRefInt = "TRUE";
              olcMemberOfGroupOC = "groupOfNames";
              olcMemberOfMemberAD = "member";
              olcMemberOfMemberOfAD = "memberOf";
            };
            "olcOverlay={1}ppolicy".attrs = {
              objectClass = [ "olcOverlayConfig" "olcPPolicyConfig" ];
              olcOverlay = "{1}ppolicy";
              olcPPolicyHashCleartext = "TRUE";
              olcPPolicyUseLockout = "FALSE";
              olcPPolicyForwardUpdates = "FALSE";
              olcPPolicyDisableWrite = "FALSE";
              olcPPolicySendNetscapeControls = "FALSE";
            };
          };
        };
      };
    };
  };
}
