{ config, lib, pkgs, sources, ... }:

{
  imports = [ sources.authentik-nix.nixosModules.default ];

  hexchen.dns.zones."chaoswit.ch".subdomains."auth".CNAME =
    [ "aoraki.chaoswit.ch." ];

  hexchen.dns.zones."cuties.network" = {
    inherit (config.hexchen.dns.zones."chaoswit.ch".subdomains."aoraki") A AAAA;
  };

  services.nginx.virtualHosts."cuties.network" = {
    forceSSL = true;
    enableACME = true;
    locations = let
      webfinger = {
        subject = "acct:$1@cuties.network";
        links = [ {
          rel = "http://openid.net/specs/connect/1.0/issuer";
          href = "https://auth.chaoswit.ch/application/o/tailscale/";
        } ];
      };
    in {
      "/.well-known/webfinger".extraConfig = "rewrite ^/.well-known/webfinger /webfinger/$arg_resource last;";
      "~ ^/webfinger/acct%3A(\\S*)%40cuties.network$" = {
        return = ''200 '${builtins.toJSON webfinger}' '';
        extraConfig = "internal;";
      };
    };
  };

  services.authentik = {
    enable = true;
    environmentFile = "/persist/var/lib/authentik/env";
    nginx = {
      enable = true;
      enableACME = true;
      host = "auth.chaoswit.ch";
    };
    settings = {
      disable_startup_analytics = true;
      avatars = "attributes.avatar,gravatar,initials";
      paths.media = "/var/lib/authentik/media";
      log_level = "warning";
    };
  };
  hexchen.bindmounts."/var/lib/private/authentik" = "/persist/var/lib/authentik";
  systemd.services.authentik.wants = [ "network-online.target" ];

  services.nginx.virtualHosts."auth.chaoswit.ch".locations."/outpost.goauthentik.io" = {
    recommendedProxySettings = false;
    extraConfig = ''
      proxy_pass https://localhost:9443/outpost.goauthentik.io;
      proxy_set_header        Host $host;
      proxy_set_header        X-Real-IP $remote_addr;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header        X-Forwarded-Proto $scheme;
    '';
  };

  monitoring.vmagent.extraExporters.authentik.port = 9300;
}
