{ config, lib, pkgs, ... }:

{
  imports = [
  # ./ldap.nix
    ./authentik.nix
  ];

  networking.firewall.allowedTCPPorts = [ 443 ];
}
