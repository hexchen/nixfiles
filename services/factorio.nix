{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."arbeitssicherheit.lol".subdomains."fabrik".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "factorio-headless"
  ];

  services.factorio = {
    enable = true;
    game-name = "Blahaj Factory Simulator";
    description = "Bikeshedding Solutions Coworking Factory Planning";
    admins = [ "hexchen" ];
    loadLatestSave = true;
    openFirewall = true;
  };
}
