{ config, lib, pkgs, hosts, ... }:

{
  services.prometheus.exporters = {
    domain.enable = true;
    blackbox = {
      enable = true;
      configFile = pkgs.writeText "blackbox.yml" (builtins.toJSON {
        modules.icmp4 = {
          prober = "icmp";
          icmp.preferred_ip_protocol = "ip4";
          icmp.ip_protocol_fallback = false;
        };
        modules.icmp6 = {
          prober = "icmp";
          icmp.preferred_ip_protocol = "ip6";
          icmp.ip_protocol_fallback = false;
        };
        modules.https_certs = {
          prober = "http";
          http.fail_if_not_ssl = true;
        };
      });
    };
  };

  services.vmagent.prometheusConfig.scrape_configs = let
    monHosts =
      lib.filterAttrs (_:v: v.nixosConfiguration.config.monitoring.enable or false) hosts;
    monTargets =
      lib.mapAttrsToList (_: v: v.nixosConfiguration.config.monitoring.target) monHosts;
    nginxVhostTargets = n: v: [ (if v.serverName == null then n else v.serverName) ] ++ v.serverAliases;
    nginxTargets =
      lib.flatten (lib.mapAttrsToList
        (_: v: lib.mapAttrsToList nginxVhostTargets v.nixosConfiguration.config.services.nginx.virtualHosts)
        monHosts);
    mkBlackbox = module: targets: {
      job_name = "blackbox_${module}";
      metrics_path = "/probe";
      params.module = [ module ];
      honor_labels = true;
      static_configs = [{
        targets = targets;
      }];
      relabel_configs = [
        {
          source_labels = [ "__address__" ];
          target_label = "__param_target";
        }
        {
          source_labels = [ "__address__" ];
          target_label = "instance";
        }
        {
          target_label = "module";
          replacement = module;
        }
        {
          target_label = "query_instance";
          replacement = config.networking.fqdn;
        }
        {
          target_label = "__address__";
          replacement = "127.0.0.1:${toString config.services.prometheus.exporters.blackbox.port}";
        }
      ];
    };
  in [
    (mkBlackbox "icmp4" monTargets)
    (mkBlackbox "icmp6" monTargets)
    (mkBlackbox "https_certs" nginxTargets)
    {
      job_name = "domain";
      metrics_path = "/probe";
      honor_labels = true;
      static_configs = [{
        targets = (lib.attrNames hosts.ns1.nixosConfiguration.config.hexchen.dns.allZones) ++ [
          "hxchn.de"
        ];
      }];
      relabel_configs = [
        {
          source_labels = [ "__address__" ];
          target_label = "__param_target";
        }
        {
          source_labels = [ "__address__" ];
          target_label = "instance";
        }
        {
          target_label = "query_instance";
          replacement = config.networking.fqdn;
        }
        {
          target_label = "__address__";
          replacement = "127.0.0.1:${toString config.services.prometheus.exporters.domain.port}";
        }
      ];
    }
  ];
}
