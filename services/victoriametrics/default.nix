{ config, lib, pkgs, ... }:

{
  imports = [
    ./vmalert.nix
    ./alertmanager.nix
    ./monitoring.nix
    ./loki.nix
  ];

  hexchen.dns.zones."chaoswit.ch".subdomains."metrics".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  services.nginx.appendHttpConfig = ''
    geo $auth_bypass {
      127.0.0.1/32 "off";
      ::1/128 "off";
      2a01:4f8:c2c:188::/64 "off";
      default "secured";
    }
  '';
  services.nginx.virtualHosts."metrics.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/".proxyPass = "http://${config.services.victoriametrics.listenAddress}";
    extraConfig = ''
      auth_basic $auth_bypass;
      auth_basic_user_file ${pkgs.writeText "metrics-htpasswd" ''
        vmagent:$2y$05$F.Hd7Op07L/LNsb03clFJuWyKjJ2e14k6RPciQUtRLpjGh0FPKffq
      ''};
    '';
  };

  services.victoriametrics = {
    enable = true;
    retentionPeriod = "1";
    listenAddress = "[::1]:8428";
    extraOptions = [
      "-enableTCP6"
      "-vmalert.proxyURL=http://${config.services.vmalert.settings.httpListenAddr}"
      "-retentionPeriod=3"
    ];
  };


  monitoring.vmagent.extraExporters.victoriametrics.port = 8428;
}
