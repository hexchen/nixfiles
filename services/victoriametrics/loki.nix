{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."chaoswit.ch".subdomains."loki".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  services.nginx.virtualHosts."loki.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/".proxyPass = "http://localhost:${toString config.services.loki.configuration.server.http_listen_port}";
    locations."/".basicAuthFile = pkgs.writeText "loki-read" ''
      loki-read:$2b$05$p4VicxoUCtr8OZyecxPlk.W0TBxG2ceWj3j9.Ib30bds4XLLQ.KlC
    '';
    locations."/loki/api/v1/push".proxyPass = config.services.nginx.virtualHosts."loki.chaoswit.ch".locations."/".proxyPass;
    locations."/loki/api/v1/push".basicAuthFile = pkgs.writeText "loki-write" ''
      loki-write:$2y$05$F.Hd7Op07L/LNsb03clFJuWyKjJ2e14k6RPciQUtRLpjGh0FPKffq
    '';
  };

  services.loki = let
    cfg = config.services.loki;
  in {
    enable = true;
    configuration = {
      auth_enabled = false;
      server.http_listen_port = 3100;
      server.grpc_listen_port = 9096;
      common = {
        path_prefix = "${cfg.dataDir}";
        storage.filesystem.chunks_directory = "${cfg.dataDir}/chunks";
        storage.filesystem.rules_directory = "${cfg.dataDir}/rules";
        replication_factor = 1;
        ring.instance_addr = "127.0.0.1";
        ring.kvstore.store = "inmemory";
      };
      schema_config.configs = [
        {
          from = "2020-10-24";
          store = "boltdb-shipper";
          object_store = "filesystem";
          schema = "v11";
          index.prefix = "index_";
          index.period = "24h";
        }
        {
          from = "2024-05-16";
          store = "tsdb";
          object_store = "filesystem";
          schema = "v13";
          index.prefix = "index_";
          index.period = "24h";
        }
      ];
      compactor = {
        working_directory = "${cfg.dataDir}/compactor";
        compaction_interval = "10m";
        delete_request_store = "filesystem";
        retention_enabled = true;
      };
      limits_config = {
        retention_period = "2200h";
        max_query_series = 5000;
        ingestion_rate_mb = 64;
        allow_structured_metadata = false;
      };
      ruler = {
        storage.type = "local";
        storage.local.directory = "${cfg.dataDir}/rules";
        evaluation_interval = "1m";
        alertmanager_url = let pa = config.services.prometheus.alertmanager; in "http://${pa.listenAddress}:${toString pa.port}";
        enable_alertmanager_v2 = true;
        ring.kvstore.store = "inmemory";
      };
    };
  };

  systemd.services.loki.preStart = let
    alerting_rules = {
      groups = [
        {
          name = "knot";
          rules = [
            {
              alert = "KSKRolloverInProgress";
              for = "1m";
              labels.severity = "info";
              labels.alertgroup = "knot";
              labels.instance = "{{ $labels.node }}";
              annotations.summary = "KSK Rollover in Progress";
              annotations.description = "A KSK Rollover for a zone has been initiated, but the registry has not picked up the change yet.";
              expr = ''
                sum by (zone,node) (rate({unit="knot.service"} |= "KSK submission check: negative" | json | line_format "{{.MESSAGE}}" | pattern "<level>: [<zone>] <message>" [1h])) > 0
              '';
            }
          ];
        }
      ];
    };
    rules_file = pkgs.writeText "loki_rules.yaml" (builtins.toJSON alerting_rules);
  in ''
    mkdir -p "${config.services.loki.dataDir}/rules/fake"
    rm -f "${config.services.loki.dataDir}/rules/fake/nixos_rules.yaml"
    cp ${rules_file} "${config.services.loki.dataDir}/rules/fake/nixos_rules.yaml"
  '';
}
