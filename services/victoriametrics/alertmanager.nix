{ config, lib, pkgs, ... }:

{
  imports = [
    ./karma.nix
  ];
  hexchen.dns.zones."chaoswit.ch" = {
    subdomains."alertmanager" = {
      CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
    };
    subdomains.${config.networking.hostName} = {
      TXT = [ (pkgs.dns.combinators.spf.strict [ "+a" ]) ];
    };
  };

  services.authentik-proxy.domains = [ "alertmanager.chaoswit.ch" ];
  services.nginx.virtualHosts."alertmanager.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/".proxyPass = let pa = config.services.prometheus.alertmanager; in "http://${pa.listenAddress}:${toString pa.port}";
  };

  sops.secrets.alertmanager_env = {
    sopsFile = ./alertmanager.sops.yaml;
  };

  services.prometheus.alertmanager = {
    enable = true;
    environmentFile = config.sops.secrets.alertmanager_env.path;
    listenAddress = "[::1]";
    extraFlags = [
      ''--cluster.listen-address ""''
      "--web.external-url=https://alertmanager.chaoswit.ch"
    ];
    configuration = {
      global.smtp_smarthost = "localhost:25";
      global.smtp_from = "alertmanager@${config.networking.fqdn}";
      global.smtp_require_tls = false;

      time_intervals = [
        {
          name = "24x7";
          time_intervals = [{
            times = [{
              start_time = "00:00";
              end_time = "24:00";
            }];
          }];
        }
        {
          name = "daytime";
          time_intervals = [{
            times = [{
              start_time = "10:00";
              end_time = "24:00";
            }];
          }];
        }
      ];

      route = {
        receiver = "monitoring-inbox";
        repeat_interval = "4h";
        group_by = [ "alertname" "alertgroup" ];

        routes = [
          {
            # desktops are always muted
            mute_time_intervals = [ "24x7" ];
            matchers = [ ''instance_type="desktop"'' ];
          }
          {
            # don't send emails for infos
            mute_time_intervals = [ "24x7" ];
            matchers = [ ''severity="info"'' ];
            continue = true;
          }
          {
            # only infrequently repeat warnings unless something new has been added
            active_time_intervals = [ "daytime" ];
            repeat_interval = "24h";
            group_wait = "5m";
            group_interval = "5m";
            matchers = [ ''severity="warning"'' ];
            continue = true;
          }
        ];
      };

      inhibit_rules = [
        {
          # suppress warnings and info if criticals exist
          source_matchers = [ ''severity="critical"'' ];
          target_matchers = [ ''severity=~"warning|info"'' ];
          equal = [ "alertname" ];
        }
        {
          # suppress info if warning exists
          source_matchers = [ ''severity="warning"'' ];
          target_matchers = [ ''severity="info"'' ];
          equal = [ "alertname" ];
        }
        {
          # suppress stale data alerts when node is down
          source_matchers = [ ''alertgroup="blackbox_icmp"'' ''alertname="PingTimeout"'' ];
          target_matchers = [ ''alertgroup="vmagent"'' ''alertname="StaleData"'' ];
          equal = [ "instance" ];
        }
      ];

      receivers = [
        {
          name = "monitoring-inbox";
          email_configs = [ { to = "monitoring@chaoswit.ch"; } ];
          pushover_configs = [
            {
              user_key = "$PO_USER";
              token = "$PO_APP";
              priority = ''{{ if eq .Status "firing" }}1{{ else }}-1{{ end }}'';
            }
          ];
        }
      ];
    };
  };

  services.postfix.enable = true;
  services.postfix.hostname = config.networking.fqdn;
  services.postfix.origin = config.networking.fqdn;

  services.kthxbye.enable = true;

  monitoring.vmagent.extraExporters.kthxbye.port = config.services.kthxbye.port;
  monitoring.vmagent.extraExporters.alertmanager.port = config.services.prometheus.alertmanager.port;
}
