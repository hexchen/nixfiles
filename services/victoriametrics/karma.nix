{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."chaoswit.ch".subdomains."karma".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  services.authentik-proxy.domains = [ "karma.chaoswit.ch" ];
  services.nginx.virtualHosts."karma.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/".proxyPass = let k = config.services.karma.settings.listen; in "http://${k.address}:${toString k.port}";
  };
  # tailscale-only backup in case oidc breaks down
  services.nginx.virtualHosts."${config.networking.hostName}.mammoth-tailor.ts.net" = {
    listenAddresses = [ "100.102.90.80" ];
    locations."/".proxyPass = let k = config.services.karma.settings.listen; in "http://${k.address}:${toString k.port}";
    locations."/".extraConfig = "proxy_set_header X-authentik-username tailscale;";
  };

  monitoring.vmagent.extraExporters.karma.port = config.services.karma.settings.listen.port;

  services.karma = {
    enable = true;
    settings = {
      listen.port = 8081;
      alertmanager = {
        interval = "1m";
        servers = [{
          name = "alertmanager";
          uri = let pa = config.services.prometheus.alertmanager; in "http://${pa.listenAddress}:${toString pa.port}";
          proxy = true;
          external_uri = "https://alertmanager.chaoswit.ch";
        }];
      };
      authentication.header = {
        name = "X-authentik-username";
        value_re = "^(.+)$";
      };
      alertAcknowledgement = {
        enabled = true;
        duration = "15m";
        author = "karma";
        comment = "ACK! This alert was acknowledged using karma on %NOW%";
      };
      filters.default = [
        "@state=active"
      ];
      history.enabled = false;
      labels = {
        order = [
          "alertgroup" "alertname" "instance" "severity" "job" "instance_type"
        ];
        color = {
          static = [
            "instance" "job"
          ];
          unique = [
            "instance_type"
          ];
          custom = {
            severity = [
              { value = "info"; color = "#87c4e0"; }
              { value = "warning"; color = "#ffae42"; }
              { value = "critical"; color = "#ff220c"; }
              { value_re = ".*"; color = "#736598"; }
            ];
          };
        };
      };
    };
  };
}
