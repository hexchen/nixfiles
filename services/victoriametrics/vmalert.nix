{ config, lib, pkgs, ... }:

{
  monitoring.vmagent.extraExporters.vmalert.port = 8880;

  services.vmalert = {
    enable = true;
    settings.httpListenAddr = "[::1]:8880";
    settings.enableTCP6 = true;
    settings."external.url" = "https://metrics.chaoswit.ch";
    settings."datasource.url" = "http://${config.services.victoriametrics.listenAddress}";
    settings."remoteWrite.url" = "http://${config.services.victoriametrics.listenAddress}";
    settings."remoteRead.url" = "http://${config.services.victoriametrics.listenAddress}";
    settings."notifier.url" = let pa = config.services.prometheus.alertmanager; in [ "http://${pa.listenAddress}:${toString pa.port}" ];

    rules = {
      groups = [
        {
          name = "vmalert";
          rules = [
            {
              alert = "ReloadFailed";
              labels.severity = "critical";
              annotations.summary = "vmalert config reload failed";
              expr = ''
                vmalert_config_last_reload_successful == 0
              '';
            }
          ];
        }
        {
          name = "vmagent";
          rules = [
            {
              alert = "StaleData";
              labels.severity = "warning";
              annotations.summary = "Scrape data stale";
              annotations.description = "No updates have been received from job in at least 5m";
              expr = ''
                time() - timestamp(up[24h]) > 300
              '';
            }
            {
              alert = "FailedScrape";
              labels.severity = "warning";
              annotations.summary = "Scrape failed";
              annotations.description = "The job was not successfully scraped";
              for = "2m";
              expr = ''
                up == 0
              '';
            }
          ];
        }
        {
          name = "systemd";
          rules = [
            {
              alert = "UnitFailed";
              labels.severity = "warning";
              expr = ''
                node_systemd_unit_state{state="failed"} > 0
              '';
            }
          ];
        }
        {
          name = "node";
          rules = [
            {
              alert = "NodeCpuLoadHigh";
              labels.severity = "warning";
              annotations.summary = "Node cpu load high";
              annotations.description = "Node cpu load more than 70% within 5 minutes";
              annotations.value = "{{ $value }}";
              for = "5m";
              expr = ''
                100 - avg by(instance)
                  (rate(node_cpu_seconds_total{mode="idle"}[5m]))*100
                  > 70
              '';
            }
            {
              alert = "NodeCpuIowaitHigh";
              labels.severity = "warning";
              annotations.summary = "Node cpu iowait high";
              annotations.description = "Node cpu iowait more than 5% within 5 minutes";
              annotations.value = "{{ $value }}";
              for = "5m";
              expr = ''
                avg by(instance)
                  (rate(node_cpu_seconds_total{mode="iowait"}[5m]) * 100 > 5)
              '';
            }
            {
              alert = "NodeFilesystemSpaceFillingUp";
              labels.severity = "warning";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem is expected to fill up within a week.";
              annotations.value = "{{ $value }}";
              for = "12h";
              expr = ''
                (node_filesystem_avail_bytes{fstype!="",mountpoint!=""} / node_filesystem_size_bytes{fstype!="",mountpoint!=""} * 100 < 15
                 and predict_linear(node_filesystem_avail_bytes{fstype!="",mountpoint!=""}[6h], 7*24*60*60) < 0
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeFilesystemSpaceFillingUp";
              labels.severity = "warning";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem is expected to fill up within 24h.";
              annotations.value = "{{ $value }}";
              for = "1h";
              expr = ''
                (node_filesystem_avail_bytes{fstype!="",mountpoint!=""} / node_filesystem_size_bytes{fstype!="",mountpoint!=""} * 100 < 15
                 and predict_linear(node_filesystem_avail_bytes{fstype!="",mountpoint!=""}[6h], 24*60*60) < 0
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeFilesystemSpaceFillingUp";
              labels.severity = "critical";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem is expected to fill up within 2h.";
              annotations.value = "{{ $value }}";
              for = "15m";
              expr = ''
                (node_filesystem_avail_bytes{fstype!="",mountpoint!=""} / node_filesystem_size_bytes{fstype!="",mountpoint!=""} * 100 < 15
                 and predict_linear(node_filesystem_avail_bytes{fstype!="",mountpoint!=""}[6h], 4*60*60) < 0
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeFilesystemAlmostFull";
              labels.severity = "info";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem has less than 5% space left.";
              annotations.value = "{{ $value }}";
              for = "15m";
              expr = ''
                (node_filesystem_avail_bytes{fstype!="",mountpoint!=""} / node_filesystem_size_bytes{fstype!="",mountpoint!=""} * 100 < 5
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeFilesystemAlmostFull";
              labels.severity = "warning";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem has less than 3% space left.";
              annotations.value = "{{ $value }}";
              for = "15m";
              expr = ''
                (node_filesystem_avail_bytes{fstype!="",mountpoint!=""} / node_filesystem_size_bytes{fstype!="",mountpoint!=""} * 100 < 3
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeFilesystemAlmostFull";
              labels.severity = "critical";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem has less than 1% space left.";
              annotations.value = "{{ $value }}";
              for = "15m";
              expr = ''
                (node_filesystem_avail_bytes{fstype!="",mountpoint!=""} / node_filesystem_size_bytes{fstype!="",mountpoint!=""} * 100 < 1
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeFilesystemIONodesFillingUp";
              labels.severity = "warning";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem is expected to fill up within a week.";
              annotations.value = "{{ $value }}";
              for = "12h";
              expr = ''
                (node_filesystem_files_free{fstype!="",mountpoint!=""} / node_filesystem_files{fstype!="",mountpoint!=""} * 100 < 15
                 and predict_linear(node_filesystem_files_free{fstype!="",mountpoint!=""}[6h], 7*24*60*60) < 0
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeFilesystemIONodesFillingUp";
              labels.severity = "warning";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem is expected to fill up within 24h.";
              annotations.value = "{{ $value }}";
              for = "1h";
              expr = ''
                (node_filesystem_files_free{fstype!="",mountpoint!=""} / node_filesystem_files{fstype!="",mountpoint!=""} * 100 < 15
                 and predict_linear(node_filesystem_files_free{fstype!="",mountpoint!=""}[6h], 24*60*60) < 0
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeFilesystemIONodesFillingUp";
              labels.severity = "critical";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem is expected to fill up within 2h.";
              annotations.value = "{{ $value }}";
              for = "15m";
              expr = ''
                (node_filesystem_files_free{fstype!="",mountpoint!=""} / node_filesystem_files{fstype!="",mountpoint!=""} * 100 < 15
                 and predict_linear(node_filesystem_files_free{fstype!="",mountpoint!=""}[6h], 4*60*60) < 0
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeIONodesAlmostFull";
              labels.severity = "info";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem has less than 5% space left.";
              annotations.value = "{{ $value }}";
              for = "15m";
              expr = ''
                (node_filesystem_files_free{fstype!="",mountpoint!=""} / node_filesystem_files{fstype!="",mountpoint!=""} * 100 < 5
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeIONodesAlmostFull";
              labels.severity = "warning";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem has less than 3% space left.";
              annotations.value = "{{ $value }}";
              for = "15m";
              expr = ''
                (node_filesystem_files_free{fstype!="",mountpoint!=""} / node_filesystem_files{fstype!="",mountpoint!=""} * 100 < 3
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
            {
              alert = "NodeIONodesAlmostFull";
              labels.severity = "critical";
              annotations.summary = "Filesystem filling up";
              annotations.description = "Filesystem has less than 1% space left.";
              annotations.value = "{{ $value }}";
              for = "15m";
              expr = ''
                (node_filesystem_files_free{fstype!="",mountpoint!=""} / node_filesystem_files{fstype!="",mountpoint!=""} * 100 < 1
                 and node_filesystem_readonly{fstype!="",mountpoint!=""} == 0)
              '';
            }
          ];
        }
        {
          name = "blackbox_icmp";
          rules = [
            {
              alert = "PingTimeout";
              labels.severity = "critical";
              annotations.summary = "Node can not be pinged";
              for = "2m";
              expr = ''
                probe_success{job=~"blackbox_icmp.+"} == 0
              '';
            }
          ];
        }
        {
          name = "blackbox_tls";
          rules = [
            {
              alert = "CertExpiry";
              labels.severity = "warning";
              annotations.summary = "TLS certificate will expire soon";
              annotations.description = "certificate expires in less than 14 days";
              annotations.value = "{{ $value }}d";
              expr = ''
                round(((probe_ssl_earliest_cert_expiry - time()) / 86400), 0.1) < 14
              '';
            }
            {
              alert = "CertExpiry";
              labels.severity = "critical";
              annotations.summary = "TLS certificate will expire soon";
              annotations.description = "certificate expires in less than 3 days";
              annotations.value = "{{ $value }}d";
              expr = ''
                round(((probe_ssl_earliest_cert_expiry - time()) / 86400), 0.1) < 3
              '';
            }
          ];
        }
        {
          name = "domain";
          rules = [
            {
              alert = "DomainExpiryQueryFail";
              labels.severity = "info";
              annotations.summary = "Domain expiry couldn't be queried";
              annotations.description = "Domain expiry returned -1, this probably means that the query failed.";
              expr = ''
                domain_expiry_days == -1
              '';
            }
            {
              alert = "DomainExpiry";
              labels.severity = "warning";
              annotations.summary = "Domain will expire soon";
              annotations.description = "Domain will expire in less than 30 days";
              annotations.value = "{{ $value }}d";
              expr = ''
                (domain_expiry_days != -1) < 30
              '';
            }
            {
              alert = "DomainExpiry";
              labels.severity = "critical";
              annotations.summary = "Domain will expire soon";
              annotations.description = "Domain will expire in less than 7 days";
              annotations.value = "{{ $value }}d";
              expr = ''
                (domain_expiry_days != -1) < 7
              '';
            }
          ];
        }
        {
          name = "dendrite";
          rules = [
            {
              alert = "FederationBackoffs";
              labels.severity = "warning";
              annotations.summary = "Federation sender is backing off";
              annotations.description = "Outgoing federation was not possible with multiple federation destinations";
              expr = ''
                dendrite_federationapi_destination_queues_backing_off > 10
              '';
            }
            {
              alert = "FederationBackoffs";
              labels.severity = "critical";
              annotations.summary = "Federation sender is backing off";
              annotations.description = "Outgoing federation was not possible with multiple federation destinations";
              expr = ''
                dendrite_federationapi_destination_queues_backing_off > 30
              '';
            }
          ];
        }
      ];
    };
  };
}
