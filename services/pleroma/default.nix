{ config, lib, pkgs, sources, evalConfig, ... }:

{

  containers.pleroma = {
    privateNetwork = true;
    hostAddress = "192.168.249.1";
    localAddress = "192.168.249.9";
    autoStart = true;
    bindMounts = {
      "/persist" = {
        hostPath = "/persist/containers/pleroma";
        isReadOnly = false;
      };
    };
    path = let
      hconfig = config;
    in (evalConfig {
      hosts = { };
      groups = { };
    } "x86_64-linux" ({ config, lib, pkgs, profiles, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";
      system.stateVersion = "23.05";

      imports = [ profiles.nopersist ];

      services.prometheus.exporters.node.enable = true;
      services.prometheus.exporters.node.enabledCollectors = [ "systemd" ];
      services.prometheus.exporters.systemd.enable = true;
      services.prometheus.exporters.systemd.extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;

      services.kresd.enable = true;

      services.akkoma = {
        enable = true;
        nginx = null;
        config = let
          e = (pkgs.formats.elixirConf { }).lib;
        in {
          ":pleroma".":instance" = {
            description = "hexchen's akkoma";
            email = "akkoma@chaoswit.ch";
            notify_email = null;
            limit = 5000;
            name = "Akkoma";
            registrations_open = false;
            upload_dir = "/persist/var/lib/pleroma/uploads";
          };
          ":pleroma"."Pleroma.Uploaders.Local".uploads = "/persist/var/lib/pleroma/uploads";
          ":pleroma"."Pleroma.Repo" = {
            adapter = e.mkRaw "Ecto.Adapters.Postgres";
            socket_dir = "/run/postgresql";
            username = config.services.akkoma.user;
            database = "pleroma";
          };
          ":pleroma"."Pleroma.Web.Endpoint" = {
            http.ip = "0.0.0.0";
            http.port = 4000;
            url.host = "pleroma.chaoswit.ch";
            url.port = 443;
            url.scheme = "https";
          };
          ":pleroma".":mrf_simple" = {
            reject = [
              (e.mkTuple [ "frennet.link" "no reason given" ])
              (e.mkTuple [ "hunk.city" "no reason given" ])
            ];
          };
          ":web_push_encryption".":vapid_details".subject = "mailto:";
          ":pleroma".":database".rum_enabled = false;
        };
        frontends = {
          primary = {
            package = pkgs.akkoma-frontends.akkoma-fe;
            name = "akkoma-fe";
            ref = "stable";
          };
          # mastodon = {
          #   package = pkgs.callPackage ./mastofe.nix {};
          #   name = "masto-fe";
          #   ref = "akkoma";
          # };
          admin = {
            package = pkgs.akkoma-frontends.admin-fe;
            name = "admin-fe";
            ref = "stable";
          };
        };
      };

      services.postgresql = {
        enable = true;
        ensureDatabases = [ "pleroma" ];
        ensureUsers = [{
          name = "akkoma";
        }];
      };

      systemd.services.postgresql.postStart =
        lib.mkAfter ''$PSQL -tAc 'ALTER DATABASE "pleroma" OWNER TO "akkoma";';'';
    })).config.system.build.toplevel;
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."pleroma".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  services.nginx.recommendedProxySettings = true;
  services.nginx.virtualHosts."pleroma.chaoswit.ch" = {
    enableACME = true;
    forceSSL = true;
    locations = {
      "/" = {
        proxyPass = "http://192.168.249.9:4000/";
        proxyWebsockets = true;
      };
    };
    extraConfig = "client_max_body_size 100m;";
  };

  monitoring.services."PLEROMA HEALTHCHECK" = {
    check_command = "check_http_wget";
    "vars.http_wget_url" = "https://pleroma.chaoswit.ch/api/v1/instance";
    "vars.notification.sms" = false;
    "vars.notification.mail" = true;
  };
}
