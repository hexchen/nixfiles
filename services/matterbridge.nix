{ config, lib, pkgs, ... }:

{
  services.matterbridge.enable = true;
  services.matterbridge.configPath = "/persist/matterbridge/matterbridge.toml";
}
