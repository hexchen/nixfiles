{ config, lib, pkgs, evalConfig, ... }:

{
  hexchen.dns.zones."chaoswit.ch".subdomains."atuin".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];


  containers.atuin = {
    privateNetwork = true;
    hostAddress = "192.168.249.1";
    localAddress = "192.168.249.10";
    autoStart = true;
    bindMounts = {
      "/persist" = {
        hostPath = "/persist/containers/atuin";
        isReadOnly = false;
      };
    };
    path = let
      hconfig = config;
    in (evalConfig {
      hosts = { };
      groups = { };
    } "x86_64-linux" ({ config, lib, pkgs, profiles, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";
      system.stateVersion = "24.05";

      imports = [ profiles.nopersist ];

      services.prometheus.exporters.node.enable = true;
      services.prometheus.exporters.node.enabledCollectors = [ "systemd" ];
      services.prometheus.exporters.systemd.enable = true;
      services.prometheus.exporters.systemd.extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;

      services.atuin = {
        enable = true;
        host = "0.0.0.0";
        maxHistoryLength = 1024 * 32;
        openRegistration = false;
      };
    })).config.system.build.toplevel;
  };

  services.nginx.virtualHosts."atuin.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations = {
      "/".proxyPass = "http://192.168.249.10:8888";
    };
  };
}
