{ config, lib, pkgs, ... }:

let port = 18685;
in {
  fileSystems."/data/backups/restic" =
    { device = "tank/backups/restic";
      fsType = "zfs";
    };

  hexchen.dns.zones."chaoswit.ch".subdomains.restic.CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];
  networking.firewall.allowedTCPPorts = [ port ];
  services.restic = {
    server = {
      enable = true;
      dataDir = "/data/backups/restic";
      listenAddress = "${toString port}";
      privateRepos = true;
      prometheus = true;
    };
  };
}
