{ config, lib, pkgs, profiles, modules, evalConfig, ... }:

{
  system.fsPackages = [ pkgs.nfs-utils ];
  systemd.mounts = [
    {
      after = [ "tailscale.service" ];
      what = "signalkuppe.mammoth-tailor.ts.net:/data/torrents";
      where = "/data/torrents";
      type = "nfs";
      wantedBy = [ "multi-user.target" ];
    }
    {
      what = "rpool/safe/containers/torrents";
      where = "/persist/containers/torrents";
      type = "zfs";
      wantedBy = [ "multi-user.target" ];
    }
    {
      what = "tank/torrents";
      where = "/data/torrents/downloads";
      type = "zfs";
      wantedBy = [ "multi-user.target" ];
    }
  ];

  systemd.services."container@torrents".after = [ "data-torrents.mount" "persist-containers-torrents.mount" ];
  # extremely annoyed hexchen noises
  systemd.network.networks."50-ve-torrents" = {
    name = "ve-torrents";
    address = [ "192.168.249.1/32" ];
    routes = [
      { routeConfig.Destination = "192.168.249.2/32"; }
    ];
  };
  containers.torrents = {
    privateNetwork = true;
    autoStart = true;
    hostAddress6 = "fd00::42:1";
    localAddress6 = "fd00::42:2";
    bindMounts = {
      "/data" = {
        hostPath = "/data/torrents";
        isReadOnly = false;
      };
      "/persist" = {
        hostPath = "/persist/containers/torrents";
        isReadOnly = false;
      };
    };
    path = let
      hconfig = config;
    in (evalConfig {
      hosts = { };
      groups = { };
    } "x86_64-linux" ({ config, lib, pkgs, profiles, modules, overlays, sources, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";
      system.stateVersion = hconfig.system.stateVersion;

      imports = [ profiles.nopersist ];
      nixpkgs.overlays = [ overlays.hexpkgs ];

      nixpkgs.config.permittedInsecurePackages = [
        "dotnet-sdk-6.0.428"
        "aspnetcore-runtime-6.0.36"
      ];

      services.prometheus.exporters.node.enable = true;
      services.prometheus.exporters.node.enabledCollectors = [ "systemd" ];
      services.prometheus.exporters.systemd.enable = true;
      services.prometheus.exporters.systemd.extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;
      networking.nameservers = [ "1.1.1.1" "1.0.0.1" ];
      networking.interfaces.eth0.ipv4.addresses = [
        { address = "192.168.249.2"; prefixLength = 32; }
      ];
      networking.interfaces.eth0.ipv4.routes = [
        { address = "192.168.249.1"; prefixLength = 32; }
        { address = "149.88.24.129"; prefixLength = 32; via = "192.168.249.1"; }
      ];
      # this is so i don't accidentally get DMCAs lol
      networking.wireguard.enable = true;
      networking.wireguard.interfaces.proton = {
        allowedIPsAsRoutes = true;
        generatePrivateKeyFile = true;
        privateKeyFile = "/persist/wireguard/proton-key";
        ips = [ "10.2.0.2/32" ];
        peers = [{
          allowedIPs = [ "0.0.0.0/0" ];
          publicKey = "XVhgEmVfTwllba68JLfzHVCw2Jr5RQsRwHB3JrcbRHE=";
          endpoint = "149.88.24.129:51820";
        }];
      };
      systemd.timers.natpmp = {
        after = ["network.target" "wireguard-proton.service"];
        requires = ["wireguard-proton.service"];
        timerConfig = {
          OnBootSec = "45s";
          OnUnitActiveSec = "45s";
          AccuracySec = "1s";
          RandomizedDelaySec = 0;
          FixedRandomDelay = false;
        };
        wantedBy = ["timers.target"];
      };
      systemd.services.natpmp = {
        after = ["network.target"];
        serviceConfig.Type = "oneshot";
        script = ''
          sleep 1
          ${pkgs.libnatpmp}/bin/natpmpc -a 1 0 udp 60 -g 10.2.0.1
          ${pkgs.libnatpmp}/bin/natpmpc -a 1 0 tcp 60 -g 10.2.0.1 | grep -oP 'Mapped public port \K\d+' > /run/natpmp_port
        '';
      };

      networking.hosts."fd00::42:1" = [ "${hconfig.networking.hostName}.local" ];
      networking.hosts."fd00::42:2" = [ "torrents.local" ];

      services.rtorrent = {
        enable = true;
        package = pkgs.rtorrent_0_10;
        dataDir = "/persist/rtorrent";
        dataPermissions = "0755";
        downloadDir = "/data/downloads";
        group = "users";
        configText = ''
          dht.mode.set = auto
          protocol.pex.set = yes
          trackers.use_udp.set = yes

          method.insert = get_public_ip_address, simple|private, "execute.capture=${pkgs.bash}/bin/bash,-c,(cat,\"eval echo -n \$(\",${pkgs.dig}/bin/dig,\" -4 TXT +short o-o.myaddr.l.google.com @ns1.google.com)\")"
          schedule2 = ip_tick, 0, 1800, "network.local_address.set=(get_public_ip_address)"

          method.insert = get_mapped_port, simple|private, "execute.capture=cat,/run/natpmp_port"
          schedule2 = port_tick, 60, 300, "network.port_range.set=(cat,(get_mapped_port),\"-\",(get_mapped_port))"

          system.umask.set = 0002
        '';
      };
      systemd.services.rtorrent = let
        cfg = config.services.rtorrent;
        rtorrentConfigFile = pkgs.writeText "rtorrent.rc" cfg.configText;
        launchScript = pkgs.writeShellScript "rtorrent-start" ''
          NATPMP_PORT=$(cat /run/natpmp_port)
          ${cfg.package}/bin/rtorrent -n -o system.daemon.set=true -o import=${rtorrentConfigFile} -o network.port_range.set=$NATPMP_PORT-$NATPMP_PORT -o dht.port.set=$NATPMP_PORT
        '';
      in {
        after = [ "natpmp.service" ];
        wants = [ "natpmp.service" ];
        serviceConfig = {
          ExecStart = lib.mkForce "${launchScript}";
          KillSignal= "SIGINT";
        };
      };

      services.nginx = {
        enable = true;
        group = "users";
        virtualHosts."torrents.local".locations."/rtorrent/scgi".extraConfig = ''
          include ${pkgs.nginx}/conf/scgi_params;
          scgi_pass unix:${config.services.rtorrent.rpcSocket};
        '';
      };

      services.flood = {
        enable = true;
        host = "::";
        extraArgs = [
          "--auth=none"
          "--rtsocket=${config.services.rtorrent.rpcSocket}"
        ];
      };
      systemd.services.flood.serviceConfig.SupplementaryGroups = [ "users" ];

      services.sonarr.enable = true;
      services.radarr.enable = true;
      services.prowlarr.enable = true;

      services.autobrr = {
        enable = true;
        secretFile = "/persist/var/lib/autobrr/sessionSecret";
        settings = {
          host = "[::]";
          port = 7474;
          metricsHost = "[::]";
          metricsPort = 9074;
          checkForUpdates = false;
          metricsEnabled = true;
          oidc_enabled = true;
          oidc_issuer = "https://auth.chaoswit.ch/application/o/autobrr/";
          oidc_client_id = "mRaLZc1xunofw7awOw9QCOUp0nHZT182ZUFsvnSb";
          oidc_redirect_url = "https://autobrr.chaoswit.ch/api/auth/oidc/callback";
          disable_built_in_login = true;
          logLevel = "INFO";
        };
      };

      # jesus christ the module for prowlarr is absolute garbage.
      systemd.services.prowlarr = {
        serviceConfig.ExecStart = lib.mkForce "${pkgs.prowlarr}/bin/Prowlarr -nobrowser -data=/persist/prowlarr";
        serviceConfig.StateDirectory = lib.mkForce null;
        serviceConfig.User = "sonarr";
        serviceConfig.Group = "sonarr";
        serviceConfig.DynamicUser = lib.mkForce false;
      };

      systemd.services.autobrr = let
        cfg = config.services.autobrr;
        configFormat = pkgs.formats.toml { };
        configTemplate = configFormat.generate "autobrr.toml" cfg.settings;
      in {
        serviceConfig.LoadCredential = [ "sessionSecret:${cfg.secretFile}" "oauthSecret:/persist/var/lib/autobrr/oauthSecret" ];
        serviceConfig.ExecStartPre = lib.mkForce (pkgs.writeShellScript "autobrr-patcher" ''
          ${lib.getExe pkgs.dasel} put -f '${configTemplate}' -v "$(${config.systemd.package}/bin/systemd-creds cat sessionSecret)" -o /var/lib/autobrr/config.toml "sessionSecret"
          ${lib.getExe pkgs.dasel} put -f /var/lib/autobrr/config.toml -v "$(${config.systemd.package}/bin/systemd-creds cat oauthSecret)" "oidc_client_secret"
        '');
      };

      hexchen.bindmounts."/var/lib/private/unpackerr" = "/persist/var/lib/unpackerr";
      systemd.services.unpackerr = {
        wantedBy = [ "multi-user.target" ];

        serviceConfig = {
          Type = "simple";
          DynamicUser = true;
          StateDirectory = "unpackerr";
          SupplementaryGroups = [ "users" ];
          ReadWritePaths = "/data/downloads";
          ExecStart = "${pkgs.unpackerr}/bin/unpackerr --config /persist/var/lib/unpackerr/unpackerr.conf";
        };
      };

      users.users.sonarr.extraGroups = [ "users" ];
      users.users.radarr.extraGroups = [ "users" ];

      services.resolved.enable = lib.mkForce false;
      environment.etc."resolv.conf".text = lib.mkForce ''
        # managed directly by nixos
        options edns0
        nameserver 1.1.1.1
        nameserver 1.0.0.1
      '';
    })).config.system.build.toplevel;
  };

  monitoring.vmagent.extraExporters.autobrr = { host = "torrents.local"; port = 9074; };
  monitoring.vmagent.extraExporters.unpackerr = { host = "torrents.local"; port = 5656; };

  services.prometheus.exporters.exportarr-sonarr = {
    enable = true;
    url = "http://torrents.local:8989/sonarr/";
    environment = {
      API_KEY = "168b309d9df84b789ae43370740f1710";
      ENABLE_ADDITIONAL_METRICS = "true";
    };
  };
  services.prometheus.exporters.exportarr-radarr = {
    enable = true;
    url = "http://torrents.local:7878/radarr/";
    environment = {
      API_KEY = "56e793a5bbdc4a3787feeb3193c74932";
      ENABLE_ADDITIONAL_METRICS = "true";
    };
    port = config.services.prometheus.exporters.exportarr-sonarr.port + 1;
  };
  services.prometheus.exporters.exportarr-prowlarr = {
    enable = true;
    url = "http://torrents.local:9696/prowlarr/";
    environment = {
      API_KEY = "217929f8a559469d93de112b02510468";
      ENABLE_ADDITIONAL_METRICS = "true";
    };
    port = config.services.prometheus.exporters.exportarr-sonarr.port + 2;
  };

  monitoring.vmagent.extraExporters.rtorrent.port = 9135;
  systemd.services.rtorrent-exporter = {
    serviceConfig = let
      exporter_config = pkgs.writeText "rtorrent_exporter.yaml" (builtins.toJSON {
        rtorrent.addr = "http://torrents.local/rtorrent/scgi";
        rtorrent.trackers.tracker-name-substitutions = [{
          convert-to = "iptorrents.com";
          matchers = [
            "bgp\.technology$"
            "empirehost\.me$"
            "stackoverflow\.tech$"
          ];
        }];
      });
    in {
      ExecStart = "${pkgs.rtorrent-exporter}/bin/rtorrent-exporter --config ${exporter_config}";
      DynamicUser = true;
    };
    wantedBy = [ "multi-user.target" ];
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."torrents".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];
  services.nginx.recommendedProxySettings = true;
  # sigh
  networking.hosts."fd00::42:2" = [ "torrents.local" ];
  services.authentik-proxy.domains = [ "torrents.chaoswit.ch" "flood.chaoswit.ch" ];
  services.nginx.virtualHosts."torrents.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    root = "/data/torrents";
    extraConfig = "autoindex on;";
    locations = let
      auth_proxy = ''
        auth_request     /outpost.goauthentik.io/auth/nginx;
        error_page       401 = @goauthentik_proxy_signin;
        auth_request_set $auth_cookie $upstream_http_set_cookie;
        add_header       Set-Cookie $auth_cookie;
      '' ;
    in {
      "= /".return = "404";
      "/sonarr".proxyPass =   "http://torrents.local:8989";
      "/sonarr".recommendedProxySettings = false;
      "/sonarr".extraConfig = ''
        proxy_set_header Authorization "Basic YWRtaW46YWRtaW4=";
        ${auth_proxy}
      '';
      "/radarr".proxyPass = "http://torrents.local:7878";
      "/radarr".recommendedProxySettings = false;
      "/radarr".extraConfig = ''
        proxy_set_header Authorization "Basic YWRtaW46YWRtaW4=";
        ${auth_proxy}
      '';
      "/prowlarr".proxyPass = "http://torrents.local:9696";
      "/prowlarr".recommendedProxySettings = false;
      "/prowlarr".extraConfig = ''
        proxy_set_header Authorization "Basic YWRtaW46YWRtaW4=";
        ${auth_proxy}
      '';
    };
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."jellyfin".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];
  services.jellyfin.enable = true;
  services.nginx.virtualHosts."jellyfin.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations = {
      "/".proxyPass = "http://127.0.0.1:8096/";
      "/socket" = {
        proxyPass = "http://127.0.0.1:8096/";
        extraConfig = ''
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "upgrade";
        '';
      };
    };
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."flood".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];
  services.nginx.virtualHosts."flood.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    root = "${pkgs.flood}/lib/node_modules/flood/dist/assets/";
    locations = {
      "/".tryFiles = "$uri /index.html";
      "/api".proxyPass = "http://torrents.local:3000";
    };
  };
  hexchen.dns.zones."chaoswit.ch".subdomains."autobrr".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];
  services.nginx.virtualHosts."autobrr.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations = {
      "/".proxyPass = "http://torrents.local:7474";
    };
  };

  home-manager.users.hexchen = {
    programs.beets = {
      enable = true;
      settings = {
        directory = "/data/torrents/music/beets";
        library = "/data/torrents/music/beets.db";
        plugins = lib.concatStringsSep " "
          [ "duplicates" "chroma" "fish" "fetchart" "embedart" ];
        "import" = {
          write = true;
          copy = true;
          move = false;
          incremental = true;
          languages = lib.concatStringsSep " " [ "en" "de" ];
          duplicate_verbose_prompt = true;
          bell = true;
        };
      };
    };
  };
}
