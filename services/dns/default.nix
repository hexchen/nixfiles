{ config, lib, pkgs, sources, ... }:

{
  services.resolved.enable = lib.mkForce false;
  networking.resolvconf.useLocalResolver = false;
  hexchen.dns = {
    enable = true;
    dnssec.enable = true;
    symlinkZones = true;
    allZones = with pkgs.dns.combinators;
      let
        common = {
          SOA = {
            nameServer = "ns1.hxchn.de.";
            adminEmail = "admin@hxchn.de";
            serial = 2023032601;
            ttl = 3600;
          };
          CAA = map (x: x // { ttl = 3600; }) (letsEncrypt "acme@lilwit.ch");
        } // (lib.mapAttrs (name: value: map (x: x // { ttl = 3600; }) value)
          (delegateTo [
            "ns1.chaoswit.ch."
            "ns1.he.net."
            "ns2.he.net."
            "ns3.he.net."
            "ns4.he.net."
            "ns5.he.net."
          ]));
      in {
        "colon.at" = common;
        "chaoswit.ch" = common;
        "lilwit.ch" = common;
        "copyonwit.ch" = common;
        "h7.pm" = common;
        "hacker.gay" = common;
        "nixwit.ch" = common;
        "cuties.network" = common;
        "transwit.ch" = common;
        "railwit.ch" = common;
        "arbeitssicherheit.lol" = common;
        "itsicherheit.lol" = common;
        "iso27001.lol" = common;
        "c3resterampe.de" = common;
      };
    zones."railwit.ch".subdomains."_atproto".TXT = [ "did=did:plc:htryfy54ng6drc3ebfkjuzyh" ];
    zones."iso27001.lol".A = [ "10.1.3.3" ];
    extraAcls = [ "acme_tsig" ];
    extraAclDefs.acme_tsig = {
      key = "acme_tsig";
      action = "update";
      update-type = "TXT";
    };
  };

  sops.secrets.acme_tsig_key = {
    owner = config.users.users.knot.name;
    sopsFile = ./tsig.sops.yaml;
  };

  services.knot.keyFiles = [ "/run/secrets/acme_tsig_key" ];
  monitoring.services."DNS SERVER ns1.chaoswit.ch" = {
    check_command = "dns";
    "vars.dns_lookup" = "ns1.chaoswit.ch";
    "vars.dns_server" = "ns1.chaoswit.ch";
    "vars.dns_expected_answers" = "135.181.24.73";
    "vars.dns_authoritative" = true;
    "vars.notification.sms" = true;
    "vars.notification.mail" = true;
  };
}
