{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."chaoswit.ch".subdomains."syncplay".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  services.syncplay = {
    enable = true;
    passwordFile = "/persist/etc/syncplay.pass";
    user = "syncplay";
    group = "syncplay";
    salt = "c657e2dee1955146c261ebd7d6c64020c2c75937de8261134ba64c2e615a5bc8";
    certDir = "/var/lib/syncplay/acme";
  };

  systemd.services.syncplay-bindfs = {
    description = "mount bindfs for syncplay";
    after = [ "local-fs.target" ];
    wantedBy = [ "local-fs.target" "syncplay.service" ];
    preStart = "mkdir -p /var/lib/syncplay/acme";
    script =
      "${pkgs.bindfs}/bin/bindfs -u syncplay -g syncplay -r -p 0400,u+D '/var/lib/acme/syncplay.chaoswit.ch' '/var/lib/syncplay/acme'";
    serviceConfig.Type = "forking";
  };
  users = {
    users.syncplay = {
      isSystemUser = true;
      group = "syncplay";
    };
    groups.syncplay = { };
  };

  services.nginx.virtualHosts."syncplay.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations = {
      "/".return = "404";
    };
  };
}
