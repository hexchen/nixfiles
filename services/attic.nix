{ config, lib, pkgs, evalConfig, ... }:

{
  containers.attic = {
    privateNetwork = true;
    hostAddress = "192.168.249.1";
    localAddress = "192.168.249.11";
    autoStart = true;
    bindMounts = {
      "/data" = {
        hostPath = "/data/attic";
        isReadOnly = false;
      };
      "/persist" = {
        hostPath = "/persist/containers/attic";
        isReadOnly = false;
      };
    };
    path = let
      hconfig = config;
    in (evalConfig {
      hosts = { };
      groups = { };
    } "x86_64-linux" ({ config, lib, pkgs, profiles, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";
      system.stateVersion = "24.05";

      imports = [ profiles.nopersist ];

      services.prometheus.exporters.node.enable = true;
      services.prometheus.exporters.node.enabledCollectors = [ "systemd" ];
      services.prometheus.exporters.systemd.enable = true;
      services.prometheus.exporters.systemd.extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;

      services.kresd.enable = true;

      services.atticd = {
        enable = true;
        environmentFile = "/persist/attic.env";
        settings = {
          listen = "[::]:8080";
          allowed-hosts = [ "attic.chaoswit.ch" ];
          api-endpoint = "https://attic.chaoswit.ch/";
          require-proof-of-possession = true;

          database.url = "postgresql:///atticd?host=/run/postgresql";

          storage = {
            type = "local";
            path = "/data";
          };

          chunking = {
            nar-size-threshold = 65536;  # chunk files that are 64 KiB or larger
            min-size = 16384;            # 16 KiB
            avg-size = 65536;            # 64 KiB
            max-size = 262144;           # 256 KiB
          };

          compression.type = "zstd";

          garbage-collection.interval = "12 hours";
          garbage-collection.default-retention-period = "3 months";
        };
      };

      services.postgresql = {
        enable = true;
        ensureDatabases = [ "atticd" ];
        ensureUsers = [{
          name = "atticd";
          ensureDBOwnership = true;
        }];
      };
    })).config.system.build.toplevel;
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."attic".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  services.nginx.virtualHosts."attic.chaoswit.ch" = {
    enableACME = true;
    forceSSL = true;
    locations."/".proxyPass = "http://${config.containers.attic.localAddress}:8080";
    extraConfig = "client_max_body_size 10g;";
  };
}
