{ config, lib, pkgs, sources, ... }:

let
  defaultDns = with pkgs.dns.combinators; {
    MX = [
      (mx.mx 10 "${config.mailserver.fqdn}.")
      (mx.mx 20 "ararat.chaoswit.ch.")
      (mx.mx 30 "mail.chaoswit.ch.")
    ];
    TXT = [ (spf.strict [ "+mx" ]) ];
    subdomains."_dmarc".TXT = [ "v=DMARC1;p=quarantine;sp=reject;adkim=s;aspf=s;pct=100;ruf=mailto:postmaster@hxchn.de" ];
  };
in {
  imports = [ sources.nixos-mailserver.nixosModule ];

  hexchen.dns.zones."chaoswit.ch" = lib.recursiveUpdate defaultDns {
    subdomains.mail.CNAME = [ "${config.mailserver.fqdn}." ];
    subdomains."mail._domainkey".TXT = [
      "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQWya63yD/3yMGxjrPF2MVxQZIMVKeTaZR9sh9S29aSb4vPUF0R09Xvm3jDHd4eXd3agxhYBMSt/yjHNy8m79x9um2txhYtS1AxVzTUJRdZR+UMd/JCKwqn+t0XM+PrZJGEbMoc7mE9qwGVq7WN0OZiCw5b1wBhEvei40CpS2wbwIDAQAB"
    ];
  };
  hexchen.dns.zones."lilwit.ch" = lib.recursiveUpdate defaultDns {
    subdomains."mail._domainkey".TXT = [
      "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJwgkGDkv/o+MdhZmkZbBcXccjm92lDalQvbKzTSDTG5PJjCXmn5QU3gerxV0z7kOaKbGETk+JEyh7VirX/ED1p1puL9Ovy2Wd4V4KPEgBPuUg87Q1PU2+3L0GKeCtwnsb13jaikKZ9EbVj+N5jnE3F+WJ8f9sVprgvSa9Wm8QnwIDAQAB"
    ];
  };
  hexchen.dns.zones."copyonwit.ch" = lib.recursiveUpdate defaultDns {
    subdomains."mail._domainkey".TXT = [
      "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDGjcWxYP2kMhQmxKpxSNAU2iDlYQlBayL9Vtrh3FIXQBQL425d+yDjsWskey6i1J7r/UAJuHXD5FOmMU6Ee3GXgsvKCaqOfqozv2D6LhSRBTjOqJZ5EaFqpHyNKATMPc+KQdV0isxN5j/lfgIihhV1ySrcItoGwB7Kvh5kUU1htwIDAQAB"
    ];
  };
  hexchen.dns.zones."nixwit.ch" = lib.recursiveUpdate defaultDns {
    subdomains."mail._domainkey".TXT = [
      "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDS2evAUK/YUvqP+v1YoKdQwcAws0bJjjSExk0C8UGCFJLc1kbNuaCsjQ3hPU7M9NRfve3xDD9ky64fA/fkw5kFCAoiq4zayVPUU8G4KcbNQDQbdYo5pwmj+LOI6WDTFOBSk/rJLqG42e/p+rq6oQPe5SdOQ32kW0OyfSCXanryhwIDAQAB"
    ];
  };
  hexchen.dns.zones."h7.pm" = lib.recursiveUpdate defaultDns {
    subdomains."mail._domainkey".TXT = [
      "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCl1Og24Yuj0qeSOi+F3H2fkcHUNAI65VnPNJSTBEdSjWUZSgWm7Md5ZsuO1acdJNos+VIA25oTtPWgaLls8FgzSiI2yakvt+VYGkyr39ihFZ4bwUuGvwmCm9OZ2oZSFp9m8/g+oU7bqoXem9XCL+M9nm4S9bZi9JyCcJfy+f7+JwIDAQAB"
    ];
  };

  services.dovecot2.sieve.extensions = [ "fileinto" ];
  mailserver = {
    mailDirectory = "/persist/mail";
    enable = true;
    fqdn = "${config.networking.hostName}.${config.networking.domain}";
    domains = [ "hxchn.de" "lilwit.ch" "copyonwit.ch" "chaoswit.ch" "nixwit.ch" "g4.pm" "h7.pm" ];
    certificateDomains = [ "mail.chaoswit.ch" ];
    loginAccounts."hexchen@lilwit.ch" = {
      hashedPassword =
        "$6$LSplQKTGbCa.$dAjTykAUKheeZzB0L7lhPQagnZ7GodJRtt4dBbj4a.f/Cide0cXrmd.WDeFmyFkDYW9vEn/oWk/7ERpTr.dHV/";
      aliases = [ "@hxchn.de" "@lilwit.ch" "@copyonwit.ch" "@chaoswit.ch" "@nixwit.ch" ];
    };

    loginAccounts."mon@h7.pm".hashedPassword = "$6$b5AMzC55mRW3H8/r$K1ueIrmD45wrYsWiU.YXlCC2XixL4irUiPgB12Vqz0RePDlJPBdb03vpbzgqU3Q4sVoW8WhWVVpzUjLb9bX/F/";
    loginAccounts."scanner@h7.pm".hashedPassword = "$6$MpWrUKnn5KA6g1ld$rDkiwJFClCvWp7GGKuKtMKoLeC8jStl8E7wkQw/QvZx0HrllxpM6HEBZ/nnMO2TyRumxGTXOS/ZeXLepVgc7T0";
    loginAccounts."paperless@h7.pm".hashedPassword = "$6$E3TUqaInrk.41fgX$cXIkLfpCaAGDLWR/1SZB.Z0VbVZiE/eEVkErCLsQ1U0PJqbRKY6ibhCPyjvlJi1hBAowFjTnAbjOH9EhHuLtz/";

    certificateScheme = "acme-nginx";

    enablePop3 = false;
    enablePop3Ssl = false;
    enableImap = false;
    enableImapSsl = true;
    enableManageSieve = true;

    virusScanning = false;
    localDnsResolver = false;

    policydSPFExtraConfig = ''
      HELO_Whitelist = mng.c3voc.de,knopi.disroot.org,post.haecksen.org,conway.haecksen.org
      skip_addresses = 127.0.0.0/8,::ffff:127.0.0.0/104,::1,185.106.84.49,2001:67c:20a0:e::179,178.21.23.139,157.90.91.40,2a01:4f8:252:3c96::1,157.90.91.40
    '';
  };

  services.postfix.config.message_size_limit = lib.mkForce "104857600";

  services.rspamd.extraConfig = ''
    actions {
      reject = null; # Disable rejects, default is 15
      add_header = 4; # Add header when reaching this score
      greylist = 10; # Apply greylisting when reaching this score
    }
  '';

  monitoring.services = {
    "DOVECOT IMAPS" = {
      check_command = "imap";
      "vars.imap_port" = 993;
      "vars.imap_ssl" = true;
      "vars.notification.sms" = false;
      "vars.notification.mail" = true;
    };
    "POSTFIX SMTP" = {
      check_command = "smtp";
      "vars.smtp_helo_fqdn" = "icinga.chaoswit.ch";
      "vars.notification.sms" = true;
    };
    "POSTFIX SPAM RBL v4" = {
      check_command = "spam_blocklist";
      "vars.ip" = "91.198.192.208";
      "vars.notification.sms" = false;
      "vars.notification.mail" = true;
    };
    "POSTFIX SPAM RBL v6" = {
      check_command = "spam_blocklist";
      "vars.ip" = "2001:67c:b54:1::5";
      "vars.notification.sms" = false;
      "vars.notification.mail" = true;
    };
  };
}
