{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."cuties.network".subdomains."headscale".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  services.headscale = {
    enable = true;
    settings = {
      server_url = "https://headscale.cuties.network";
      listen_addr = "127.0.0.1:8323";

      oidc = {
        only_start_if_oidc_is_available = true;
        issuer = "https://idm.cuties.network/oauth2/openid/headscale";
        client_id = "headscale";
        client_secret_path = "/run/secrets/headscale_oidc_secret";
        strip_email_domain = true;
      };

      dns_config.magic_dns = true;
      dns_config.domains = [ "ts.cuties.network" ];
      dns_config.base_domain = "ts.cuties.network";
    };
  };

  users.users.headscale.extraGroups = [ config.users.groups.keys.name ];
  sops.secrets.headscale_oidc_secret = {
    owner = config.users.users.headscale.name;
    sopsFile = ./headscale.sops.yaml;
  };

  services.nginx.virtualHosts."headscale.cuties.network" = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      proxyPass = "http://localhost:8323";
      proxyWebsockets = true;
    };
  };
}
