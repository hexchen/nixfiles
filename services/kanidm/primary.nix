{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."cuties.network" = {
    subdomains."idm".CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
  };

  services.kanidm = {
    enableServer = true;
    serverSettings = {
      bindaddress = "[::1]:9443";
      ldapbindaddress = "[::1]:636";
      trust_x_forward_for = true;
      db_fs_type = "zfs";
      tls_chain = "/var/lib/kanidm/acme/fullchain.pem";
      tls_key = "/var/lib/kanidm/acme/key.pem";
      domain = "idm.cuties.network";
      origin = "https://idm.cuties.network";
      online_backup.path = "/var/lib/kanidm/backups/";
      online_backup.schedule = "03 */6 * * *";
    };
  };

  services.nginx.virtualHosts."idm.cuties.network" = {
    enableACME = true;
    forceSSL = true;
    locations."/".proxyPass = "https://[::1]:9443";
  };

  systemd.services.kanidm-bindfs = {
    description = "mount bindfs for kanidm";
    after = [ "local-fs.target" ];
    wantedBy = [ "local-fs.target" "kanidm.service" ];
    preStart = "mkdir -p /var/lib/kanidm/acme";
    script =
      "${pkgs.bindfs}/bin/bindfs -u kanidm -g kanidm -r -p 0400,u+D '/var/lib/acme/idm.cuties.network' '/var/lib/kanidm/acme'";
    serviceConfig.Type = "forking";
  };

  monitoring.services."KANIDM STATUS" = {
    check_command = "check_http_wget";
    "vars.http_wget_url" = "https://idm.cuties.network/";
    "vars.http_wget_contains" = "Kanidm idm.cuties.network";
    "vars.notification.sms" = true;
    "vars.notification.mail" = true;
  };
}
