lib: hostName: host:

with lib;
let
  stringify = value:
    if builtins.typeOf value == "bool" then
      if value then "true" else "false"
    else if builtins.typeOf value == "list" then
      "[${concatMapStringsSep ", " stringify value}]"
    else if builtins.typeOf value == "string" && hasSuffix "_interval" value then
      value
    else
      ''"${toString value}"'';
  stringifyAttrs = attrs:
    concatStringsSep "\n"
    (mapAttrsToList (name: value: "${name} = ${stringify value}") attrs);
  serviceConfig = sname: service: ''
    object Service "${sname}" {
      import "generic-service"
      host_name = "${hostName}"
      ${stringifyAttrs service}
    }
  '';
in ''
   object Host "${hostName}" {
     import "generic-host"
     ${stringifyAttrs host.settings}
   }

   ${concatStringsSep "\n" (mapAttrsToList serviceConfig (host.services or {}))}
''
