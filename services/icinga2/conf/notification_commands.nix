pkgs:

let
  notificationWrapper = pkgs.writeScript "notification_wrapper.py" ''
    #!${pkgs.python3.withPackages (py: [ py.requests ])}/bin/python3
    ${builtins.readFile ./notification_wrapper.py}
  '';
in ''
object NotificationCommand "send-host-notification" {
    import "plugin-notification-command"

    command = [ "${notificationWrapper}" ]

    arguments = {
        "--mail" = {
            set_if = {{
                host_vars = host.vars
                    if (host_vars.notification.mail == true) {
                        return true
                    } else {
                        return false
                    }
                }}
            value = "$user.email$"
        }
        "--sms" = {
            set_if = {{
                host_vars = host.vars
                if (host_vars.notification.sms == true) {
                    return true
                } else {
                    return false
                }
            }}
            value = "$user.vars.mobile$"
        }
        "--host_name" = "$host.display_name$"
        "--state" = "$host.state$"
        "--output" = "$host.output$"
        "--notification-type" = "$notification.type$"
    }
}

object NotificationCommand "send-service-notification" {
    import "plugin-notification-command"

    command = [ "${notificationWrapper}" ]

    arguments = {
        "--mail" = {
            set_if = {{
                svc_vars = service.vars
                if (svc_vars.notification.mail == true) {
                    return true
                } else {
                    return false
                }
            }}
            value = "$user.email$"
        }
        "--sms" = {
            set_if = {{
                svc_vars = service.vars
                if (svc_vars.notification.sms == true) {
                    return true
                } else {
                    return false
                }
            }}
            value = "$user.vars.mobile$"
        }
        "--host_name" = "$host.display_name$"
        "--service_name" = "$service.display_name$"
        "--state" = "$service.state$"
        "--output" = "$service.output$"
        "--notification-type" = "$notification.type$"
    }
}
''
