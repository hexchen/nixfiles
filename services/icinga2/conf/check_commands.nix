{ icinga-checks, monitoring-plugins }:

''
object CheckCommand "sshmon" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${icinga-checks.sshmon}/bin/sshmon" ]

    arguments = {
        "-c" = {
            value = "$sshmon_command$"
        }
        "-h" = {
            value = "$address$"
        }
        "-p" = {
            set_if = bool("$sshmon_port$")
            value = "$sshmon_port$"
        }
        "-t" = {
            set_if = bool("$sshmon_timeout$")
            value = "$sshmon_timeout$"
        }
    }
}

object CheckCommand "sshmon_other_host" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${icinga-checks.sshmon}/bin/sshmon" ]

    arguments = {
        "-c" = {
            value = "$sshmon_command$"
        }
        "-h" = {
            value = "$run_check_on$"
        }
        "-p" = {
            set_if = bool("$sshmon_port$")
            value = "$sshmon_port$"
        }
        "-t" = {
            set_if = bool("$sshmon_timeout$")
            value = "$sshmon_timeout$"
        }
    }
}

object CheckCommand "dummy_hostalive" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "true" ]
}

object CheckCommand "check_http_wget" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${icinga-checks.httpwget}" ]

    arguments = {
        "--check-string" = {
            set_if = bool("$http_wget_contains$")
            value = "$http_wget_contains$"
        }
        "--no-follow-redirects" = {
            set_if = "$http_wget_nofollow$"
        }
        "--no-verify-ssl" = {
            set_if = "$http_wget_noverify$"
        }
        "--useragent" = {
            set_if = bool("$http_wget_useragent$")
            value = "$http_wget_useragent$"
        }
        "--username" = {
            set_if = bool("$http_wget_username$")
            value = "$http_wget_username$"
        }
        "--password" = {
            set_if = bool("$http_wget_password$")
            value = "$http_wget_password$"
        }
        "--url" = {
            value = "$http_wget_url$"
        }
    }
}

object CheckCommand "https_cert" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${monitoring-plugins}/bin/check_http" ]

    arguments = {
        "--ssl" = {}
        "--sni" = {}
        "-w" = "4"
        "-c" = "8"
        "-C" = "30"
        "-p" = "$port$"
        "-H" = "$hostname$"
    }

    vars.port = "443"
}

object CheckCommand "check_certificate_at" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${icinga-checks.certat}/bin/certat", "$domain$", "$port$" ]
    vars.port = "443"
}

object CheckCommand "check_imap" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${monitoring-plugins}/bin/check_imap" ]

    arguments = {
        "-S" = {
            set_if = "$imap_ssl$"
        }
        "-p" = {
            value = "$imap_port$"
        }
        "-H" = {
            value = "$address$"
        }
    }
}

object CheckCommand "check_ping_ip" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${monitoring-plugins}/bin/check_ping", "-H", "$ip$", "-w", "100,5%", "-c", "500,10%" ]
}

object CheckCommand "check_pop3" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${monitoring-plugins}/bin/check_pop" ]

    arguments = {
        "-S" = {
            set_if = "$pop3_ssl$"
        }
        "-p" = {
            value = "$pop3_port$"
        }
        "-H" = {
            value = "$address$"
        }
    }
}

object CheckCommand "prometheus" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${monitoring-plugins}/bin/check_prometheus" ]
    arguments = {
        "-u" = "$url$"
        "-U" = "$username$"
        "-p" = "$password$"
        "-a" = "$alertname$"
    }
}

object CheckCommand "check_smtp" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${monitoring-plugins}/bin/check_smtp" ]

    arguments = {
        "-H" = {
            value = "$address$"
        }
    }
}

object CheckCommand "spam_blocklist" {
    import "plugin-check-command"
    import "ipv4-or-ipv6"

    command = [ "${icinga-checks.spamblocklist}", "$ip$" ]
    timeout = 15
}
''
