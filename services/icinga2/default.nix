{ config, lib, pkgs, hosts, ... }:

let
  icinga = pkgs.icinga2.override { withPostgresql = true; };
  constantsconf = ''
    const PluginDir = "${pkgs.monitoring-plugins}/bin"
    const ManubulonPluginDir = "bin"
    const PluginContribDir = "bin"
    const NodeName = "${config.networking.fqdn}"
    const ZoneName = NodeName
  '';
  dependencies = ''
    apply Dependency "disable-service-checks-on-host-down" to Service {
      disable_checks = true
      ignore_soft_states = true
      assign where true
    }
  '';
  ido-pgsql = ''
    library "db_ido_pgsql"

    object IdoPgsqlConnection "ido-pgsql" {
      user = "icinga",
      password = "icinga",
      host = "localhost",
      database = "icinga"
    }
  '';
  users = ''
    object UserGroup "on-call_sms" {
        display_name = "On-Call Support (with SMS)"
    }
  '';
  icingaconf = ''
    include "constants.conf"
    include "salt.conf"

    include "zones.conf"
    include <itl>
    include <plugins>
    include <plugins-contrib>

    include "features-enabled/*.conf"
    include_recursive "conf.d"

    object IcingaApplication "app" { }
  '';

  hostconfig = import ./conf/host.nix lib;

  checks = import ./checks pkgs;

  check_commands = import ./conf/check_commands.nix {
    inherit (pkgs) monitoring-plugins icinga-checks;
  };

  icingaHosts = with lib; filterAttrs (h: c: c.enable)
    (mapAttrs (name: host: host.nixosConfiguration.config.monitoring or { enable = false; }) hosts);
in {
  monitoring.enable = true;
  environment.systemPackages = [ icinga ];

  # ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIObcCtyJ+I8QcvhS3JIHlzPgQS7q9yaLEjRl+A0BZ/HH sshmon
  sops.secrets."sshmon.priv" = {
    owner = config.users.users.icinga2.name;
    mode = "0400";
    sopsFile = ./icinga2.sops.yaml;
  };
  sops.secrets."apiusers.conf" = {
    restartUnits = [ "icinga2.service" ];
    owner = config.users.users.icinga2.name;
    sopsFile = ./icinga2.sops.yaml;
  };
  sops.secrets."userdetails.conf" = {
    restartUnits = [ "icinga2.service" ];
    owner = config.users.users.icinga2.name;
    sopsFile = ./icinga2.sops.yaml;
  };
  sops.secrets."salt.conf" = {
    restartUnits = [ "icinga2.service" ];
    owner = config.users.users.icinga2.name;
    sopsFile = ./icinga2.sops.yaml;
  };
  sops.secrets."icinga_notifications.env" = {
    restartUnits = [ "icinga2.service" ];
    owner = config.users.users.root.name;
    sopsFile = ./icinga2.sops.yaml;
  };

  hexchen.bindmounts."/var/lib/icinga2" = "/persist/var/lib/icinga2";
  users.users.icinga2.isSystemUser = true;
  users.users.icinga2.home = "/var/lib/icinga2";
  users.users.icinga2.group = "icinga2";
  users.groups.icinga2 = {};

  environment.etc = {
    "sshmon.priv".source = "/run/secrets/sshmon.priv";
    "icinga2/icinga2.conf".text = icingaconf;
    "icinga2/constants.conf".text = constantsconf;
    "icinga2/salt.conf".source = "/run/secrets/salt.conf";
    "icinga2/zones.conf".source = "${icinga}/etc/icinga2/zones.conf";
    "icinga2/features-enabled/api.conf".source = "${icinga}/etc/icinga2/features-available/api.conf";
    "icinga2/features-enabled/checker.conf".source = "${icinga}/etc/icinga2/features-available/checker.conf";
    "icinga2/features-enabled/ido-pgsql.conf".text = ido-pgsql;
    "icinga2/features-enabled/mainlog.conf".source = "${icinga}/etc/icinga2/features-available/mainlog.conf";
    "icinga2/features-enabled/notification.conf".source = "${icinga}/etc/icinga2/features-available/notification.conf";
    "icinga2/conf.d/apiusers.conf".source = "/run/secrets/apiusers.conf";
    "icinga2/conf.d/dependencies.conf".text = dependencies;
    "icinga2/conf.d/check_commands.conf".text = check_commands;
    "icinga2/conf.d/notifications.conf".source = ./conf/notifications.conf;
    "icinga2/conf.d/notification_commands.conf".text = import ./conf/notification_commands.nix pkgs;
    "icinga2/conf.d/templates.conf".source = ./conf/templates.conf;
    "icinga2/conf.d/timeperiods.conf".source = ./conf/timeperiods.conf;
    "icinga2/conf.d/users.conf".text = users;
    "icinga2/conf.d/userdetails.conf".source = "/run/secrets/userdetails.conf";

    "icingaweb2/modules/monitoring/commandtransports.ini".source = "/run/secrets/web-commandtransports.ini";
  } // lib.mapAttrs' (k: v: { name = "icinga2/conf.d/hosts/${k}.conf" ; value.text = hostconfig k v; }) icingaHosts;

  systemd.services.icinga2 = {
    enable = true;
    restartTriggers = with lib; mapAttrsToList (_: item: item.source) (filterAttrs (_: item: (hasPrefix "icinga" item.target) && (isDerivation item.source)) config.environment.etc);
    script = "${icinga}/bin/icinga2 daemon -c /etc/icinga2/icinga2.conf";
    serviceConfig.EnvironmentFile = "/run/secrets/icinga_notifications.env";
    serviceConfig.User = "icinga2";
    serviceConfig.Group = "icinga2";
    wantedBy = [ "multi-user.target" ];
  };

  systemd.tmpfiles.rules = [
    "d /run/icinga2 0755 icinga2 icinga2"
    "z /run/icinga2 0755 icinga2 icinga2"
    "d /var/cache/icinga2 0755 icinga2 icinga2"
    "z /var/cache/icinga2 0755 icinga2 icinga2"
    "d /var/log/icinga2 0755 icinga2 icinga2"
    "z /var/log/icinga2 0755 icinga2 icinga2"
  ];

  services.postgresql = {
    enable = true;
    initialScript = pkgs.writeText "icinga-initial.sql" ''
      CREATE ROLE icinga WITH LOGIN PASSWORD 'icinga';
      CREATE DATABASE icinga WITH OWNER 'icinga' ENCODING 'UTF-8';

      CREATE ROLE icingaweb WITH LOGIN PASSWORD 'icingaweb';
      CREATE DATABASE icingaweb WITH OWNER 'icingaweb' ENCODING 'UTF-8';

      \c icinga
      SET ROLE icinga;
      \i ${icinga}/share/icinga2-ido-pgsql/schema/pgsql.sql

      \c icingaweb
      SET ROLE icingaweb;
      \i ${pkgs.icingaweb2}/etc/schema/pgsql.schema.sql
      INSERT INTO icingaweb_user (name, active, password_hash) VALUES ('hexchen', 1, '$2y$10$AvZKZGBOw3q5AU6x/9/4KO3y/Iwduz8gpbgfx3CGJVNeJfHrR8nDW');
    '';
  };

  services.nginx.virtualHosts."icinga.chaoswit.ch" = {
    enableACME = true;
    forceSSL = true;
    locations."~ ^/index.php(.*)$".extraConfig = ''
      auth_request     /outpost.goauthentik.io/auth/nginx;
      error_page       401 = @goauthentik_proxy_signin;
      auth_request_set $auth_cookie $upstream_http_set_cookie;
      add_header       Set-Cookie $auth_cookie;
      auth_request_set $authentik_username $upstream_http_x_authentik_username;
      fastcgi_param REMOTE_USER $authentik_username;
    '';

    locations."/outpost.goauthentik.io" = {
      recommendedProxySettings = false;
      extraConfig = ''
        proxy_pass              https://auth.chaoswit.ch/outpost.goauthentik.io;
        # ensure the host of this vserver matches your external URL you've configured
        # in authentik
        proxy_set_header        Host auth.chaoswit.ch;
        proxy_set_header        X-Forwarded-Host $host;
        proxy_set_header        X-Original-URL $scheme://$http_host$request_uri;
        add_header              Set-Cookie $auth_cookie;
        auth_request_set        $auth_cookie $upstream_http_set_cookie;
        proxy_pass_request_body off;
        proxy_ssl_server_name   on;
        proxy_set_header        Content-Length "";
      '';
    };

    locations."@goauthentik_proxy_signin" = {
      recommendedProxySettings = false;
      extraConfig = ''
        internal;
        add_header Set-Cookie $auth_cookie;
        return 302 /outpost.goauthentik.io/start?rd=$request_uri;
      '';
    };
  };
  services.icingaweb2 = {
    enable = true;
    virtualHost = "icinga.chaoswit.ch";
    authentications.autologin.backend = "external";
    authentications.icingaweb = {
      backend ="db";
      resource = "icingaweb-psql";
    };
    generalConfig.global.config_resource = "icingaweb-psql";
    groupBackends.icingaweb = {
      backend ="db";
      resource = "icingaweb-psql";
    };
    modules.monitoring.enable = true;
    modules.monitoring.backends.icinga2.resource = "icinga2";
    # stop it from generating a file
    modules.monitoring.mutableTransports = true;
    resources.icingaweb-psql = {
      type = "db";
      db = "pgsql";
      host = "localhost";
      username = "icingaweb";
      password = "icingaweb";
      dbname = "icingaweb";
    };
    resources.icinga2 = {
      type = "db";
      db = "pgsql";
      host = "localhost";
      username = "icinga";
      password = "icinga";
      dbname = "icinga";
    };
    roles.Administrators = {
      permissions = "*";
      users = "hexchen";
      groups = "icinga_admin";
    };
  };
  services.phpfpm.pools.icingaweb2.phpPackage = lib.mkForce (pkgs.php81.withExtensions ({ enabled, all }: [ all.imagick ] ++ enabled));

  services.postfix.enable = true;
  services.postfix.hostname = config.networking.fqdn;
  services.postfix.origin = config.networking.fqdn;

  hexchen.dns.zones."chaoswit.ch".subdomains.${config.networking.hostName} = {
    TXT = [ (pkgs.dns.combinators.spf.strict [ "+a" ]) ];
  };

  sops.secrets."web-commandtransports.ini" = {
    owner = config.users.users.icingaweb2.name;
    sopsFile = ./icinga2.sops.yaml;
  };

  # les checks
  monitoring.services."ICINGA HEALTH".check_command = "icinga";
  monitoring.services."ICINGA IDO HEALTH" = {
    check_command = "ido";
    "vars.ido_type" = "IdoPgsqlConnection";
    "vars.ido_name" = "ido-pgsql";
  };
}
