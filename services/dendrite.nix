{ config, lib, pkgs, modules, evalConfig, ... }:

{
  containers.dendrite = {
    privateNetwork = true;
    hostAddress = "192.168.249.1";
    localAddress = "192.168.249.8";
    autoStart = true;
    bindMounts = {
      "/persist" = {
        hostPath = "/persist/containers/dendrite";
        isReadOnly = false;
      };
    };
    path = let
      hconfig = config;
    in (evalConfig {
      hosts = { };
      groups = { };
    } "x86_64-linux" ({ config, lib, pkgs, profiles, overlays, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";
      system.stateVersion = "23.05";

      imports = [ profiles.nopersist ];
      nixpkgs.overlays = [ overlays.hexpkgs ];

      services.prometheus.exporters.node.enable = true;
      services.prometheus.exporters.node.enabledCollectors = [ "systemd" ];
      services.prometheus.exporters.systemd.enable = true;
      services.prometheus.exporters.systemd.extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;

      fileSystems."/var/lib/dendrite" = {
        device = "/persist/var/lib/dendrite";
        options = [ "bind" ];
      };

      services.dendrite = {
        enable = true;
        settings = {
          global = {
            server_name = "colon.at";
            private_key = "/var/lib/dendrite/matrix_key.pem";
            kafka.addresses = [ ];
            presence.enable_inbound = false;
            presence.enable_outbound = false;
            metrics.enabled = true;
            cache = {
              max_size_estimated = "1gb";
              max_age = "1h";
            };
          };
          client_api.registration_disabled = true;
          media_api.dynamic_thumbnails = true;
          media_api.max_file_size_bytes = 1024*1024*512;
          sync_api.search.enabled = true;
          logging = [{
            type = "std";
            level = "warn";
          }];
          mscs.mscs = [
            "msc2836"
          ];
          federation_api.key_perspectives = [{
            server_name = "matrix.org";
            keys = [
              {
                key_id = "ed25519:auto";
                public_key = "Noi6WqcDj0QmPxCNQqgezwTlBKrfqehY1u2FyWP9uYw";
              }
              {
                key_id = "ed25519:a_RXGa";
                public_key = "l8Hft5qXKn1vfHrg3p4+W8gELQVo8N13JkluMfmn2sQ";
              }
            ];
          }];

          # databases
          app_service_api.database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_appservice?sslmode=disable";
          federation_api.database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_federationapi?sslmode=disable";
          media_api.database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_mediaapi?sslmode=disable";
          mscs.database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_mscs?sslmode=disable";
          room_server.database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_roomserver?sslmode=disable";
          sync_api.database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_syncapi?sslmode=disable";
          key_server.database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_keyserver?sslmode=disable";
          user_api.account_database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_userapi?sslmode=disable";
          user_api.device_database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_userapi?sslmode=disable";
          relay_api.database.connection_string = "postgresql://dendrite:dendrite@localhost/dendrite_relayapi?sslmode=disable";
        };
      };

      services.matrix-sliding-sync = {
        enable = true;
        environmentFile = "/persist/etc/syncv3.env";
        settings = {
          SYNCV3_BINDADDR = "0.0.0.0:8009";
          SYNCV3_SERVER = "https://aoraki.colon.at";
          SYNCV3_LOG_LEVEL = "warn";
          SYNCV3_PROM = "0.0.0.0:8010";
        };
      };

      services.postgresql = {
        enable = true;
        settings.max_connections = 10000;
        ensureDatabases = [
          "dendrite_appservice"
          "dendrite_federationapi"
          "dendrite_mediaapi"
          "dendrite_mscs"
          "dendrite_roomserver"
          "dendrite_syncapi"
          "dendrite_keyserver"
          "dendrite_userapi"
          "dendrite_relayapi"
        ];
        ensureUsers = [{
          name = "dendrite";
        }];
      };
      systemd.services.postgresql.postStart = (lib.mkAfter (lib.concatMapStringsSep "\n"
        (x: ''$PSQL -tAc 'ALTER DATABASE "${x}" OWNER TO "dendrite";';'')
        [
          "dendrite_appservice"
          "dendrite_federationapi"
          "dendrite_mediaapi"
          "dendrite_mscs"
          "dendrite_roomserver"
          "dendrite_syncapi"
          "dendrite_keyserver"
          "dendrite_userapi"
          "dendrite_relayapi"
        ]
      ));

      users.users.dendrite.isSystemUser = true;
      users.users.dendrite.group = "dendrite";
      users.groups.dendrite = {};
      systemd.services.dendrite.serviceConfig.User = "dendrite";
      systemd.services.dendrite.serviceConfig.Group = "dendrite";
      systemd.services.dendrite.serviceConfig.DynamicUser = lib.mkForce false;
      systemd.services.dendrite.serviceConfig.StateDirectory = lib.mkForce null;
      systemd.services.dendrite.serviceConfig.LoadCredential = lib.mkForce null;

      # deep sigh
      services.kresd.enable = true;
    })).config.system.build.toplevel;
  };

  monitoring.vmagent.extraExporters.dendrite = {
    host = "dendrite";
    port = 8008;
    labels.container = "dendrite";
  };
  monitoring.vmagent.extraExporters.sliding-sync = {
    host = "dendrite";
    port = 8010;
    labels.container = "dendrite";
  };

  hexchen.dns.zones."colon.at" = {
    inherit (config.hexchen.dns.zones."chaoswit.ch".subdomains."aoraki") A AAAA;
  };
  hexchen.dns.zones."colon.at".subdomains."${config.networking.hostName}".CNAME =
    [ "aoraki.chaoswit.ch." ];

  services.nginx.virtualHosts = {
    "colon.at" = {
      forceSSL = true;
      enableACME = true;
      locations = let
        wellKnown.server = {
          "m.server" = "${config.networking.hostName}.colon.at:443";
        };
        wellKnown.client = {
          "m.homeserver"."base_url" = "https://${config.networking.hostName}.colon.at";
          "m.identity_server"."base_url" = "https://vector.im";
          "org.matrix.msc3575.proxy"."url" = "https://${config.networking.hostName}.colon.at";
        };
      in {
        "/.well-known/matrix/server" = {
          extraConfig = ''
            add_header Content-Type application/json;
          '';
          return = ''200 '${builtins.toJSON wellKnown.server}' '';
        };
        "/.well-known/matrix/client" = {
          extraConfig = ''
            add_header Content-Type application/json;
            add_header Access-Control-Allow-Origin *;
          '';
          return = ''200 '${builtins.toJSON wellKnown.client}' '';
        };
      };
    };
    "${config.networking.hostName}.colon.at" = {
      forceSSL = true;
      enableACME = true;
      locations = {
        "~ ^/(client/|_matrix/client/unstable/org.matrix.msc3575/sync)" = { proxyPass = "http://${config.containers.dendrite.localAddress}:8009"; };
        "/_matrix" = { proxyPass = "http://${config.containers.dendrite.localAddress}:8008"; };
        "/".return = "404";
      };
    };
  };
}
