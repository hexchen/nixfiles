{ config, lib, pkgs, ... }:

{
  monitoring.vmagent.extraExporters.grafana.port = 3000;

  hexchen.dns.zones."chaoswit.ch".subdomains."grafana".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  services.nginx.virtualHosts."grafana.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/".proxyPass = let gcs = config.services.grafana.settings.server; in "http://[${gcs.http_addr}]:${toString gcs.http_port}";
  };

  services.grafana = {
    enable = true;
    settings = {
      analytics = {
        feedback_links_enabled = false;
        reporting_enabled = false;
      };
      "auth.generic_oauth" = {
        name = "authentik";
        enabled = "true";
        client_id = "E5ake75qhXz1oXNUDfR0qhJGw5u8camWN3V89Wnu";
        client_secret = "$__file{/persist/var/lib/grafana/client_secret}";
        login_attribute_path = "preferred_username";
        scopes = "openid email profile entitlements";
        auth_url = "https://auth.chaoswit.ch/application/o/authorize/";
        token_url = "https://auth.chaoswit.ch/application/o/token/";
        api_url = "https://auth.chaoswit.ch/application/o/userinfo/";
        allow_assign_grafana_admin = "true";
        role_attribute_path = "contains(entitlements, 'sysadmin') && 'GrafanaAdmin' || contains(entitlements, 'admin') && 'Admin' || contains(entitlements, 'editor') && 'Editor' || 'Viewer'";
        auto_login = "true";
      };
      server = {
        # Listening Address
        http_addr = "::1";
        # and Port
        http_port = 3000;
        # Grafana needs to know on which domain and URL it's running
        domain = "grafana.chaoswit.ch";
        root_url = "https://${config.services.grafana.settings.server.domain}/";
        router_logging = false;
      };
      security.disable_initial_admin_creation = true;
    };
    provision.datasources.settings = {
      apiVersion = 1;
      datasources = [
        {
          name = "VictoriaMetrics";
          type = "prometheus";
          url = "http://${config.services.victoriametrics.listenAddress}";
          isDefault = true;
        }
        {
          name = "AlertManager";
          type = "alertmanager";
          url = let pa = config.services.prometheus.alertmanager; in "http://${pa.listenAddress}:${toString pa.port}";
          jsonData.implementation = "prometheus";
        }
        {
          name = "Loki";
          type = "loki";
          url = "http://localhost:${toString config.services.loki.configuration.server.http_listen_port}";
        }
      ];
    };
  };
}
