{ config, lib, pkgs, evalConfig, ... }:

{
  hexchen.dns.zones."chaoswit.ch".subdomains."pad".CNAME =
    [ "aoraki.chaoswit.ch." ];

  containers.codimd = {
    privateNetwork = true;
    hostAddress = "192.168.249.1";
    localAddress = "192.168.249.3";
    autoStart = true;
    bindMounts = {
      "/persist" = {
        hostPath = "/persist/containers/codimd";
        isReadOnly = false;
      };
    };
    path = let
      hconfig = config;
    in (evalConfig {
      hosts = { };
      groups = { };
    } "x86_64-linux" ({ config, lib, pkgs, profiles, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";
      system.stateVersion = "21.05";

      imports = [ profiles.nopersist ];

      services.prometheus.exporters.node.enable = true;
      services.prometheus.exporters.node.enabledCollectors = [ "systemd" ];
      services.prometheus.exporters.systemd.enable = true;
      services.prometheus.exporters.systemd.extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;

      services.hedgedoc = {
        enable = true;
        environmentFile = "/persist/var/lib/hedgedoc/env";
        settings = {
          allowAnonymous = false;
          allowAnonymousEdits = true;
          allowFreeURL = true;
          allowGravatar = false;
          allowOrigin = [ "localhost" "pad.chaoswit.ch" ];
          dbURL = "postgres://codimd:codimd@localhost:5432/codimd";
          defaultPermission = "private";
          domain = "pad.chaoswit.ch";
          host = "0.0.0.0";
          protocolUseSSL = true;
          hsts.preload = false;
          allowEmailRegister = false;
          oauth2 = {
            clientID = "BEP3uhhp5mPi3Nk5vx3FuHSfogX8rn5cee0y7IVe";
            authorizationURL = "https://auth.chaoswit.ch/application/o/authorize/";
            tokenURL = "https://auth.chaoswit.ch/application/o/token/";
            userProfileURL = "https://auth.chaoswit.ch/application/o/userinfo/";
            userProfileUsernameAttr = "preferred_username";
            userProfileDisplayNameAttr = "name";
            userProfileEmailAttr = "email";
            scope = "openid email profile";
          };
        };
      };

      systemd.services.hedgedoc.serviceConfig.StateDirectory = lib.mkForce "/persist/var/lib/hedgedoc";

      services.postgresql = {
        enable = true;
        package = pkgs.postgresql_15;
        ensureDatabases = [ "codimd" ];
        ensureUsers = [{
          name = "codimd";
          ensureDBOwnership = true;
        }];
      };
      # deep sigh
      services.kresd.enable = true;
    })).config.system.build.toplevel;
  };

  services.nginx.virtualHosts."pad.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      proxyPass = "http://${config.containers.codimd.localAddress}:3000";
      extraConfig = ''
        proxy_pass_request_headers on;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;
        proxy_buffering off;
      '';
    };
    locations."/status".return = 404;
    locations."/metrics".return = 404;
  };

  monitoring.vmagent.extraExporters.hedgedoc = {
    host = "codimd";
    port = 3000;
    labels.container = "codimd";
  };
}
