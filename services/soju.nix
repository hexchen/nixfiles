{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."chaoswit.ch".subdomains."soju".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];
  services.nginx.virtualHosts."soju.chaoswit.ch" = {
    enableACME = true;
    locations."/".return = "200";
  };

  networking.firewall.allowedTCPPorts = [ 6697 ];

  systemd.services.soju-bindfs = {
    description = "mount bindfs for soju";
    after = [ "local-fs.target" ];
    wantedBy = [ "local-fs.target" "soju.service" ];
    preStart = "mkdir -p /var/lib/soju/acme";
    script =
      "${pkgs.bindfs}/bin/bindfs -u soju -g soju -r -p 0400,u+D '/var/lib/acme/soju.chaoswit.ch' '/var/lib/soju/acme'";
    serviceConfig.Type = "forking";
  };

  users = {
    users.soju = {
      isSystemUser = true;
      group = "soju";
    };
    groups.soju = { };
  };

  systemd.services.soju.serviceConfig = {
    DynamicUser = lib.mkForce false;
    User = "soju";
    Group = "soju";
    StateDirectory = lib.mkForce null;
  };

  services.soju = {
    enable = true;
    hostName = "soju.chaoswit.ch";
    tlsCertificate = "/var/lib/soju/acme/fullchain.pem";
    tlsCertificateKey = "/var/lib/soju/acme/key.pem";
  };

  monitoring.services."SOJU TLS" = {
    check_command = "check_certificate_at";
    "vars.domain" = "soju.chaoswit.ch";
    "vars.port" = "6697";
    "vars.notification.sms" = true;
    "vars.notification.mail" = true;
  };
}
