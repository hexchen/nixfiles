{ config, lib, pkgs, evalConfig, ... }:

{
  hexchen.dns.zones."cuties.network".subdomains."netbox".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  containers.netbox = {
    privateNetwork = true;
    hostAddress = "192.168.100.1";
    localAddress = "192.168.100.7";
    autoStart = true;
    bindMounts = {
      "/persist" = {
        hostPath = "/persist/container/netbox";
        isReadOnly = false;
      };
    };
    path = let
      hconfig = config;
    in (evalConfig {
      hosts = { };
      groups = { };
    } "x86_64-linux" ({ config, lib, pkgs, profiles, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";
      system.stateVersion = hconfig.system.stateVersion;

      imports = [ profiles.nopersist ];

      services.prometheus.exporters.node.enable = true;
      services.prometheus.exporters.node.enabledCollectors = [ "systemd" ];
      services.prometheus.exporters.systemd.enable = true;
      services.prometheus.exporters.systemd.extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;

      services.netbox = {
        enable = true;
        listenAddress = "[::]";
        secretKeyFile = "/persist/var/lib/netbox/secret";
        plugins = py: [ py.python-jose ];
        package = pkgs.netbox;

        extraConfig = ''
          REMOTE_AUTH_BACKEND = 'social_core.backends.open_id_connect.OpenIdConnectAuth'
          REMOTE_AUTH_DEFAULT_GROUPS = [ 'netbox_access' ]
          SOCIAL_AUTH_OIDC_OIDC_ENDPOINT = 'https://idm.cuties.network/oauth2/openid/netbox'
          SOCIAL_AUTH_OIDC_KEY = 'netbox'
          SOCIAL_AUTH_OIDC_SECRET = 'yDXQ9xXXetyqfK4cTSyV7EGkQxZB3bUBy3Cz4wc8G4RkLSHr'
        '';
      };
    })).config.system.build.toplevel;
  };

  services.nginx.virtualHosts."netbox.cuties.network" = {
    forceSSL = true;
    enableACME = true;
    locations."/".proxyPass = "http://${config.containers.netbox.localAddress}:8001";
    locations."/static".root = "/persist/container/netbox/var/lib/netbox";
  };

  monitoring.services."NETBOX STATUS" = {
    check_command = "check_http_wget";
    "vars.http_wget_url" = "https://netbox.cuties.network/api/status/";
    "vars.notification.sms" = false;
    "vars.notification.mail" = true;
  };
}
