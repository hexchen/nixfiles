{ config, lib, pkgs, evalConfig, ... }:

{
  hexchen.dns.zones."chaoswit.ch".subdomains."paperless".CNAME =
    [ "aoraki.chaoswit.ch." ];

  containers.paperless = {
    privateNetwork = true;
    hostAddress = "192.168.249.1";
    localAddress = "192.168.249.6";
    autoStart = true;
    bindMounts = {
      "/persist" = {
        hostPath = "/persist/containers/paperless";
        isReadOnly = false;
      };
    };
    path = let
      hconfig = config;
    in (evalConfig {
      hosts = { };
      groups = { };
    } "x86_64-linux" ({ config, lib, pkgs, profiles, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";
      system.stateVersion = hconfig.system.stateVersion;

      imports = [ profiles.nopersist ];

      services.prometheus.exporters.node.enable = true;
      services.prometheus.exporters.node.enabledCollectors = [ "systemd" ];
      services.prometheus.exporters.systemd.enable = true;
      services.prometheus.exporters.systemd.extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;

      services.kresd.enable = true;

      services.paperless = let
        package = pkgs.paperless-ngx.overrideAttrs (old: {
          disabledTestPaths = [ "src/documents/tests/test_management_consumer.py" ];
          propagatedBuildInputs = old.propagatedBuildInputs ++ [ pkgs.paperless-ngx.python.pkgs.nltk ];
        });
        nltkDatasets = {
          # Raw data is licensed in most parts as BSD-3 clause with one CC-BY-SA 3 dataset
          # BSD-3: https://github.com/snowballstem/snowball-data/commit/0fe3da19bb818461548f4780815ab323fea606bb
          # CC-BY-SA dataset addition: https://github.com/snowballstem/snowball-data/commit/35999218ecdd19c82fc87f791179d00657007814
          "stemmers/snowball_data" = pkgs.fetchzip { # Roughly 35M in closure size
            pname = "snowball_data";
            version = "2015-02-10";
            url = "https://raw.githubusercontent.com/nltk/nltk_data/ebd75b7b05637d6b70828a15fc458fd9e76a70d0/packages/stemmers/snowball_data.zip";
            hash = "sha256-CjMftTNwAQksVzIom0M7nlAroD5w8Fk64blBNSgZX5k=";
            meta.license = with lib.licenses; [ bsd3 cc-by-nc-sa-30 ];
          };
          # License unclear. Some parts seem to (in part?) originate from https://pypi.org/project/stop-words/
          "corpora/stopwords" = pkgs.fetchzip { # Roughly 85K in final closure size
            pname = "stopwords";
            version = "2016-08-20";
            url = "https://raw.githubusercontent.com/nltk/nltk_data/7d5849ea69b31fe13f03903756835fa2f867a187/packages/corpora/stopwords.zip";
            hash = "sha256-zQf9RnK0yWI1bIEjNEm2hmGubHGeDQN0supIBQGww6c= ";
          };
          # Unable to spot a license for this
          "tokenizers/punkt" = pkgs.fetchzip { # Roughly 35M in closure size
            pname = "punkt";
            version = "2022-07-14";
            url = "https://raw.githubusercontent.com/nltk/nltk_data/1d3c34b4cfd6059986bf4bc604e5929335ab92ff/packages/tokenizers/punkt.zip";
            hash = "sha256-SKZu26K17qMUg7iCFZey0GTECUZ+sTTrF/pqeEgJCos=";
          };
        };
      in {
        inherit package;
        enable = true;
        settings = {
          PAPERLESS_OCR_LANGUAGE = "deu+eng";
          PAPERLESS_URL = "https://paperless.chaoswit.ch";
          PAPERLESS_CONVERT_BINARY = "${pkgs.imagemagick}/bin/convert";
          PAPERLESS_GS_BINARY = "${pkgs.ghostscript}/bin/gs";
          PAPERLESS_OPTIPNG_BINARY = "${pkgs.optipng}/bin/optipng";
          PAPERLESS_OCR_MODE = "skip";
          PAPERLESS_OCR_USER_ARGS = builtins.toJSON {
            invalidate_digital_signatures = true;
          };
          PAPERLESS_NLTK_DIR = pkgs.linkFarm "nltk_data" nltkDatasets;
          PAPERLESS_CONSUMER_ENABLE_BARCODES = true;
          PAPERLESS_CONSUMER_ENABLE_ASN_BARCODE = true;
          PAPERLESS_CONSUMER_BARCODE_SCANNER = "ZXING";
          PAPERLESS_CONSUMER_BARCODE_DPI = 600;
          PAPERLESS_CONSUMER_BARCODE_UPSCALE = 2;

          PAPERLESS_ENABLE_HTTP_REMOTE_USER = true;
          PAPERLESS_HTTP_REMOTE_USER_HEADER_NAME = "HTTP_X_AUTHENTIK_USERNAME";
        };
        address = "192.168.249.6";
      };


      # services.openssh = {
      #   enable = true;
      #   ports = [ 22 ];
      #   passwordAuthentication = false;
      #   kbdInteractiveAuthentication = false;
      #   permitRootLogin = "no";
      #   gatewayPorts = "no";
      #   # *sigh*
      #   # scanners, amirite?
      #   kexAlgorithms = [
      #     "diffie-hellman-group-exchange-sha1"
      #     "sntrup761x25519-sha512@openssh.com"
      #     "curve25519-sha256"
      #     "curve25519-sha256@libssh.org"
      #     "diffie-hellman-group-exchange-sha256"
      #   ];
      #   macs = [
      #     "hmac-sha1"
      #     "hmac-sha2-512-etm@openssh.com"
      #     "hmac-sha2-256-etm@openssh.com"
      #     "umac-128-etm@openssh.com"
      #     "hmac-sha2-512"
      #     "hmac-sha2-256"
      #     "umac-128@openssh.com"
      #   ];
      #   ciphers = [
      #     "aes128-ctr"
      #     "chacha20-poly1305@openssh.com"
      #     "aes256-gcm@openssh.com"
      #     "aes128-gcm@openssh.com"
      #     "aes256-ctr"
      #     "aes192-ctr"
      #   ];
      #   hostKeys = [
      #     {
      #       bits = 2048;
      #       path = "/persist/ssh/ssh_host_rsa_key_unsafe";
      #       type = "ssh-rsa";
      #     }
      #     {
      #       bits = 1024;
      #       path = "/persist/ssh/ssh_host_dss_key_unsafe";
      #       type = "ssh-dss";
      #     }
      #   ];
      #   sftpServerExecutable = "internal-sftp";
      #   extraConfig = ''
      #     HostKeyAlgorithms ^ssh-rsa
      #     X11Forwarding no
      #     AllowAgentForwarding no
      #     AllowTcpForwarding no
      #     PermitTunnel no
      #     ChrootDirectory %h
      #     ForceCommand internal-sftp
      #     PrintMotd no
      #   '';
      # };
      #
      # users.users.pdfsftp = {
      #   isNormalUser = true;
      #   openssh.authorizedKeys.keys = [
      #     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBk7bRNR+uL9QsiMjcG2F61Sc89pEhcdLn309kBaYE8L+nEbIipT6TbB3LldmhSTa65021eox1unf/y0AIaybhzloHn2x6KgmnP8emetCmLp9BGkrIzI/NAB0FZv0BeqV5KxqDl/Ge38+Ijnt4xynP8thdiKf/FYBgi4ZIsCiTaP48kLXZO0h+PjdcOZXdiFTn03yDpX2GAsmvrRKLlo1ifWQ+r5UJM+X1MIYCWESJrp35ALca86sGYoePVLq9uEtoPMLAwccR5LPwSMvjPw259oup3TiJ+nk4ow1ff7tZoXNLLhrDrhtSpoHeEyrt+c8WvKIYD+5BHhHA05El/UwT root@BR5CF3705DEEBC"
      #   ];
      #   home = "/import";
      # };

      systemd.services.paperless-web.serviceConfig.EnvironmentFile = "/persist/var/lib/paperless/env";
      systemd.services.paperless-scheduler.serviceConfig.EnvironmentFile = "/persist/var/lib/paperless/env";
      systemd.services.paperless-consumer.serviceConfig.EnvironmentFile = "/persist/var/lib/paperless/env";
    })).config.system.build.toplevel;
  };

  # hexchen.nftables.nat.forwardPorts = [{
  #   ports = [ 54161 ];
  #   destination = "${config.containers.paperless.localAddress}:22";
  #   proto = "tcp";
  # }];

  services.authentik-proxy.domains = [ "paperless.chaoswit.ch" ];
  services.nginx.virtualHosts."paperless.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/".proxyPass = "http://${config.containers.paperless.localAddress}:28981";
    locations."/".proxyWebsockets = true;
  };

  monitoring.services."PAPERLESS STATUS" = {
    check_command = "check_http_wget";
    "vars.http_wget_url" = "https://paperless.chaoswit.ch/accounts/login/";
    "vars.http_wget_contains" = "Paperless-ngx sign in";
    "vars.notification.sms" = false;
    "vars.notification.mail" = true;
  };
}
