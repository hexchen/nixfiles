{ config, lib, pkgs, modules, profiles, evalConfig, users, ... }:

{
  hexchen.nftables.nat.forwardPorts = [{
    ports = [ 25565 1024 ];
    destination = "192.168.101.2";
    proto = "tcp";
  }];
  networking.firewall.allowedTCPPorts = [ 25565 1024 ];

  networking.bridges.lxcbr0.interfaces = [ ];
  networking.interfaces.lxcbr0.ipv4.addresses = [{
    address = "192.168.101.1";
    prefixLength = 24;
  }];
  networking.nat.internalInterfaces = [ "lxcbr0" ];

  virtualisation.lxc.enable = true;
  virtualisation.lxc.systemConfig = ''
    lxc.bdev.zfs.root = rpool/lxc
    lxc.lxcpath = /persist/lxc
  '';

  monitoring.sshmonServices = {
    "HCF CONTAINER" = {
      check_command = "${pkgs.monitoring-plugins}/bin/check_ping -H 192.168.101.2 -w 3000,80% -c 5000,100%";
      settings."vars.notification.sms" = false;
      settings."vars.notification.mail" = true;
    };
  };
}
