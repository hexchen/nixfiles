{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."railwit.ch" = {
    inherit (config.hexchen.dns.zones."chaoswit.ch".subdomains."${config.networking.hostName}")
      A AAAA;
  };

  services.nginx.virtualHosts."railwit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/".return = "307 https://umap.openstreetmap.de/en/map/hexchens-railmap_39898";
  };
}
