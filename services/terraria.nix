{ config, lib, pkgs, ... }:

{
  hexchen.dns.zones."arbeitssicherheit.lol".subdomains."terraria".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];
  hexchen.dns.zones."h7.pm".subdomains."t".CNAME =
    [ "${config.networking.hostName}.chaoswit.ch." ];

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "terraria-server"
  ];

  services.terraria = {
    enable = true;
    openFirewall = true;
    dataDir = "/persist/terraria";
    messageOfTheDay = "i would never arbeitssicherheit a terraria";
    noUPnP = true;
    password = "$TERRARIA_PASSWORD";
    worldPath = "/persist/terraria/.local/share/Terraria/Worlds/World.wld";
  };

  # i hate broken modules so much.
  systemd.services.terraria.serviceConfig.ExecStart = let
    cfg = config.services.terraria;
    tmuxCmd = "${lib.getExe pkgs.tmux} -S ${lib.escapeShellArg cfg.dataDir}/terraria.sock";
    worldSizeMap = {
      small = 1;
      medium = 2;
      large = 3;
    };
    valFlag =
      name: val:
      lib.optionalString (val != null) "-${name} \"${lib.escape [ "\\" "\"" ] (toString val)}\"";
    boolFlag = name: val: lib.optionalString val "-${name}";
    flags = [
      (valFlag "port" cfg.port)
      (valFlag "maxPlayers" cfg.maxPlayers)
      (valFlag "password" cfg.password)
      (valFlag "motd" cfg.messageOfTheDay)
      # this one is borked, i think
      # (valFlag "autocreate" (builtins.getAttr cfg.autoCreatedWorldSize worldSizeMap))
      (valFlag "world" cfg.worldPath)
      (valFlag "banlist" cfg.banListPath)
      (boolFlag "secure" cfg.secure)
      (boolFlag "noupnp" cfg.noUPnP)
    ];
  in lib.mkForce "${tmuxCmd} new -d ${pkgs.terraria-server}/bin/TerrariaServer ${lib.concatStringsSep " " flags}";

  systemd.services.terraria.serviceConfig.EnvironmentFile = "/persist/terraria/env";
}
