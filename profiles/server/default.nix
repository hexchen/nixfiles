{ config, lib, pkgs, profiles, modules, ... }:

{
  imports = [ profiles.base.linux modules.encboot modules.authentik-proxy ];

  services.iperf3.enable = true;
  services.iperf3.openFirewall = true;

  security.acme.defaults.email = "acme@lilwit.ch";
  security.acme.acceptTerms = true;

  services.nginx = {
    enable = lib.mkDefault true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    virtualHosts."${config.networking.fqdn}" = {
      default = true;
      forceSSL = true;
      enableACME = true;
    };
  };
  services.authentik-proxy.enable = true;

  networking.firewall.allowedTCPPorts = [ 80 443 ];

  networking.nameservers = [ "1.1.1.1" "1.0.0.1" "2606:4700:4700::1111" "2606:4700:4700::1001" ];

  systemd.network.wait-online.ignoredInterfaces = lib.mapAttrsToList (x: _: "ve-${x}") config.containers;
  systemd.network.wait-online.anyInterface = true;

  services.vmagent.prometheusConfig.global.external_labels.instance_type = "server";
}
