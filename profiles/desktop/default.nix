{ config, lib, pkgs, profiles, ... }:

{
  imports = [ profiles.base.linux ];
  boot.plymouth.enable = lib.mkDefault true;
  boot.plymouth.theme = "sphere";
  boot.plymouth.themePackages = [ pkgs.adi1090x-plymouth-themes ];
  boot.extraModulePackages = [];
  boot.kernelModules = [];
  boot.kernelParams = [ "preempt=full" ];
  nixpkgs.config.allowUnfree = true;
  powerManagement.cpuFreqGovernor = lib.mkOverride 998 "ondemand";
  programs.light.enable = true;

  hardware.bluetooth.settings.General.Experimental = true;
  hardware.pulseaudio.enable = false;
  services.dbus.enable = true;
  services.pipewire.enable = true;
  services.pipewire.pulse.enable = true;
  services.pipewire.alsa.enable = true;
  xdg.portal = {
    enable = true;
    wlr = {
      enable = true;
      settings = {
        screencast = {
          max_fps = 30;
          chooser_type = "simple";
          chooser_cmd = "${pkgs.slurp}/bin/slurp -f %o -or";
        };
      };
    };
  };

  services.printing = {
    enable = true;
    drivers = with pkgs; [ foomatic-filters cups-kyodialog3 ];
    browsing = true;
  };
  hardware.printers.ensurePrinters = [
    {
      name = "Entropia";
      description = "Entropia";
      location = "Entropia";
      deviceUri = "socket://10.214.225.146";
      model = "Kyocera/Kyocera_FS-1320D.ppd";
      ppdOptions.PageSize = "A4";
    }
  ];
  hardware.sane = {
    enable = true;
    extraBackends = with pkgs; [ epkowa ];
  };

  networking.useDHCP = true;
  networking.useNetworkd = true;
  networking.wireless.enable = true;
  networking.nameservers = [ "1.1.1.1" "1.0.0.1" "2606:4700:4700::1111" "2606:4700:4700::1001" ];
  networking.hosts."172.18.1.110" = [ "iceportal.de" ];
  # networking.hosts."10.101.64.10" = [ "wifi.bahn.de" ];
  systemd.network.wait-online.anyInterface = true;

  hardware.graphics.enable = true;

  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;

  services.udev.packages = [ pkgs.yubikey-personalization pkgs.libu2f-host pkgs.qmk-udev-rules pkgs.epkowa pkgs.qflipper ];

  services.xserver.enable = true;
  services.xserver.displayManager.gdm.enable = true;

  boot.supportedFilesystems = [ "zfs" ];

  virtualisation.libvirtd = {
    enable = true;
    qemu.package = pkgs.qemu_kvm;
    qemu.ovmf.enable = true;
  };
  virtualisation.spiceUSBRedirection.enable = true;

  services.usbmuxd.enable = true;

  services.udev.extraRules = ''
    SUBSYSTEM=="tty", ATTRS{interface}=="Black Magic GDB Server", SYMLINK+="ttyBmpGdb"
    SUBSYSTEM=="tty", ATTRS{interface}=="Black Magic UART Port", SYMLINK+="ttyBmpTarg"
  '';

  programs.dconf.enable = true;
  services.gnome.gnome-settings-daemon.enable = true;

  programs.openvpn3.enable = true;

  services.pcscd.enable = true;

  # laptops and other desktops are fairly dynamic.
  # checking them is probably not feasible.
  # for now...
  monitoring.enable = false;

  # clone key!!!
  # ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJurLAusX04cdnKs6p1QWbvZrZLucPhtfdjPCiYYO5lj hexchen@clonekey
  sops.secrets.clonekey = {
    sopsFile = ./secrets.sops.yaml;
  };
  home-manager.users.root.programs.ssh = {
    enable = true;
    controlMaster = "auto";
    controlPersist = "10m";
    matchBlocks."*".identityFile = config.sops.secrets.clonekey.path;
  };

  # nix-community key
  # ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPhkoAm+YPpPIXrk9tSu91dZNYytadsB8gPIQU7mUQJq hexchen@dna
  sops.secrets.nix-community.sopsFile = ./secrets.sops.yaml;

  programs.ssh.knownHosts."build01.nix-community.org".publicKey =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIElIQ54qAy7Dh63rBudYKdbzJHrrbrrMXLYl7Pkmk88H";

  nix = {
    distributedBuilds = false;
    buildMachines = [
      {
        hostName = "build01.nix-community.org";
        maxJobs = 64;
        sshKey = config.sops.secrets.nix-community.path;
        sshUser = "hexchen";
        system = "x86_64-linux";
        supportedFeatures = [ "big-parallel" ];
      }
    ];
  };

  services.vmagent.prometheusConfig.global.external_labels.instance_type = "desktop";

  security.pam.loginLimits = [
    {
      domain = "@users";
      type   = "-";
      item   = "rtprio";
      value  = "95";
    }
    {
      domain = "@users";
      type   = "-";
      item   = "nice";
      value  = "-19";
    }
    {
      domain = "@users";
      type   = "-";
      item   = "memlock";
      value  = "4194304";
    }
  ];

  services.pipewire.extraConfig.pipewire."20-rt" = {
    "context.modules" = [{
      name = "libpipewire-module-rt";
      args."nice.level" = -10;
      args."rt.prio" = 85;
      flags = [ "ifexists" "nofail" ];
    }];
  };

  services.joycond.enable = true;

  home-manager.users.root.home.stateVersion = "23.05";
}
