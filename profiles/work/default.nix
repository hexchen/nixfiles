{ config, lib, pkgs, users, ... }:

{
  imports = [
    users.hsattler.base
    users.hsattler.linux
  ];
}
