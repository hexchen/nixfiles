{ config, lib, pkgs, overlays, users, modules, sources, ... }:

{
  nixpkgs.overlays = [ overlays.hexpkgs overlays.dns ];
  imports = [
    users.hexchen.base
    ./make-nixpkgs.nix
  ];

  nix.package = pkgs.lix;
  nix.settings = {
    binary-caches = [ "https://kittywitch.cachix.org" "https://hexchen.cachix.org" ];
    binary-cache-public-keys = [
      "kittywitch.cachix.org-1:KIzX/G5cuPw5WgrXad6UnrRZ8UDr7jhXzRTK/lmqyK0="
      "hexchen.cachix.org-1:296JO0xOaB/sY4YYdpgODpiIa2fGBVNi7LjAbWDeMM8="
    ];
    trusted-users = [ "root" "@wheel" ];
    experimental-features = [ "nix-command" "flakes" "repl-flake" "auto-allocate-uids" ];
  };

  nix.gc.automatic = lib.mkDefault true;
  nix.gc.options = lib.mkDefault "--delete-older-than 7d";
  environment.variables.EDITOR = "vim";
  home-manager.useGlobalPkgs = true;

  environment.systemPackages = with pkgs; [
    kitty.terminfo
    htop
    tcpdump
    nload
    iftop
    bottom
    iperf
    binutils
    dnsutils
    minicom

    ripgrep
    fd
    pv
    progress
    parallel
    file
    vim
    git
    rsync
    whois
    p7zip
    zstd
    gnupg
    pinentry-curses
  ];

  environment.etc."ssl-unbundled".source = "${pkgs.cacert.unbundled}/etc/ssl";
}
