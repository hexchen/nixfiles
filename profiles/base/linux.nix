{ config, lib, pkgs, overlays, users, modules, sources, ... }:

{
  imports = [
    ./common.nix
    modules.network.wireguard
    modules.dns
    modules.network.nftables
    modules.monitoring
    users.hexchen.linux
  ];

  # boot.kernelPackages = lib.mkOverride 999 pkgs.linuxPackages_latest;
  boot.kernelPackages = lib.mkOverride 999 pkgs.linuxPackages;
  boot.kernelParams = [ "quiet" ];
  boot.loader.efi.canTouchEfiVariables = lib.mkIf (config.boot.loader.grub.efiSupport || config.boot.loader.systemd-boot.enable) (lib.mkDefault true);

  services.zfs = lib.mkIf (config.boot.supportedFilesystems.zfs or false) {
    autoScrub.enable = true;
    autoSnapshot = {
      enable = true;
      frequent = 12;
      hourly = 24;
      daily = 3;
      weekly = 0;
      monthly = 0;
    };
  };

  systemd.network.enable = true;
  networking.useNetworkd = true;
  networking.nftables.enable = lib.mkDefault true;
  networking.domain = lib.mkDefault "chaoswit.ch";
  systemd.network.networks."80-container-ve" = {
    name = "ve-*";
    linkConfig.Unmanaged = true;
  };

  services.journald.extraConfig = ''
    SystemMaxUse=512M
    MaxRetentionSec=48h
  '';

  programs.command-not-found.enable = false;

  services.openssh = {
    enable = true;
    ports = lib.mkDefault [ 54160 ];
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      PermitRootLogin = lib.mkDefault "no";
      GatewayPorts = lib.mkDefault "yes";
      LoginGraceTime = 0;
    };
    extraConfig = "StreamLocalBindUnlink yes";
  };
  programs.mosh.enable = true;
  security.sudo.wheelNeedsPassword = lib.mkDefault false;

  # TODO: common-ize
  sops.age.sshKeyPaths = map (x: x.path) (lib.filter (x: x.type == "ed25519") config.services.openssh.hostKeys);

  powerManagement.cpuFreqGovernor = lib.mkOverride 999 "performance";

  i18n.defaultLocale = "en_IE.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "de";
  };

  programs.mtr.enable = true;

  programs.fuse.userAllowOther = true;

  monitoring.sshmon.pubkey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIObcCtyJ+I8QcvhS3JIHlzPgQS7q9yaLEjRl+A0BZ/HH icinga-sshmon";

  system.autoUpgrade = {
    enable = lib.mkDefault true;
    allowReboot = lib.mkDefault false;
    dates = "03:00";
    persistent = true;
    randomizedDelaySec = "90m";
    flake = lib.mkDefault "gitlab:hexchen/nixfiles";
    rebootWindow.lower = "03:00";
    rebootWindow.upper = "05:00";
    flags = [
      "--accept-flake-config"
    ];
  };

  environment.systemPackages = with pkgs; [
    lm_sensors
    usbutils
    pciutils
    cryptsetup
    gptfdisk

    molly-guard
  ];

  time.timeZone = "UTC";

  services.tailscale.enable = true;

  sops.defaultSopsFile = ../../hosts/${config.networking.hostName}/secrets.sops.yaml;
}
