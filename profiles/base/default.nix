{
  common = ./common.nix;
  linux = ./linux.nix;
  darwin = ./darwin.nix;
}
