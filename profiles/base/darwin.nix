{ config, lib, pkgs, overlays, users, modules, sources, ... }:

{
  imports = [
    ./common.nix
    users.hexchen.darwin
  ];

  nix.settings.trusted-users = [ "@admin" ];
  nix.settings.auto-allocate-uids = true;
  # https://github.com/NixOS/nix/issues/7273
  nix.settings.auto-optimise-store = false;
  programs.nix-index.enable = false;
  security.pam.enableSudoTouchIdAuth = true;

  environment.systemPackages = [ pkgs.emacs29-pgtk ];

  nix.distributedBuilds = true;
  nix.buildMachines = [{
    hostName = "build01.chaoswit.ch";
    sshUser = "builder";
    sshKey = "/etc/nix/builder_ed25519";
    publicHostKey = "c3NoLWVkMjU1MTkgQUFBQUMzTnphQzFsWkRJMU5URTVBQUFBSURrZE9yMzhxaUdaVXZKcGxPbG1rd1UyYVhIV2hyQjd2SkRyS0lCQ0dDVmggcm9vdEBidWlsZDAxCg==";
    system = "x86_64-linux";
    protocol = "ssh-ng";
    maxJobs = 4;
    supportedFeatures = [ "kvm" "benchmark" "big-parallel" ];
  }];
  nix.settings.builders-use-substitutes = true;

  programs.ssh.extraConfig = ''
    Host build01.chaoswit.ch
      Port 54160
  '';
}
