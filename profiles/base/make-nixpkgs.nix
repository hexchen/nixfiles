{ pkgs, lib, config, sources, ... }:

let
  nixpkgs = lib.cleanSource pkgs.path;

  version = config.system.nixos.version or config.system.darwinVersion or "unknown";
  revision = config.system.nixos.revision or config.system.darwinRevision or null;
  versionSuffix = config.system.nixos.versionSuffix or config.system.darwinVersionSuffix;

  nixSources = pkgs.runCommand "nix-${config.system.nixos.version or config.system.darwinVersion}" {
    preferLocalBuild = true;
  } ''
    mkdir -p $out
    cd ${nixpkgs.outPath}
    tar -cpf $out/nixpkgs.tar.gz .
    sha256sum $out/nixpkgs.tar.gz | cut -d " " -f 1 > $out/nixpkgs.sha256
    cp -prd ${nixpkgs.outPath} $out/nixpkgs
    chmod -R u+w $out/nixpkgs
    ${lib.optionalString (revision != null) ''
      echo -n ${revision} > $out/nixpkgs/.git-revision
    ''}
    echo -n ${versionSuffix} > $out/nixpkgs/.git-revision
    echo ${versionSuffix} | sed -e s/pre// > $out/nixpkgs/svn-revision
    date +%s > $out/last_updated
  '';
in {
  environment.etc."src".source = nixSources;
  nix.nixPath = let path = toString ../..; in lib.mkForce [ "repl=${path}/repl.nix" "nixpkgs=${sources.nixpkgs}" ];
}
