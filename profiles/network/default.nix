{ config, lib, pkgs, modules, hostname, hosts, ... }:


let
  mcfg = hosts.${hostname}.meta.wireguard;
in {
  imports = [
    modules.network.bird
    modules.network.policyrouting
    modules.network.nftables
  ];

  options.network = with lib; {
    routeDefault = mkOption {
      default = true;
      type = types.bool;
    };
    core = mkOption {
      default = mcfg.core or false;
      type = types.bool;
    };
    prefix4 = mkOption {
      default = "195.39.247";
      type = types.str;
    };
    prefix6 = mkOption {
      default = "2a0f:4ac0:1337";
      type = types.str;
    };
    prefixOffset = mkOption {
      default = 64;
      type = types.int;
    };
    prefsrc4 = mkOption {
      type = types.nullOr types.str;
      default = "${config.network.prefix4}.${
          toString
          (config.network.prefixOffset + mcfg.magicNumber)
        }";
    };
    prefsrc6 = mkOption {
      type = types.nullOr types.str;
      default = "${config.network.prefix6}::${
          toString mcfg.magicNumber
        }";
    };
    networks4 = mkOption { type = types.listOf types.str; };
    networks6 = mkOption { type = types.listOf types.str; };
  };

  config = let
    filters = {
      net4 = ''
        if (net ~ [ ${lib.concatStringsSep ", " config.network.networks4} ]) then {
          accept;
        } else {
          reject;
        }
      '';
      net6 = ''
        if (net ~ [ ${lib.concatStringsSep ", " config.network.networks6} ]) then {
          accept;
        } else {
          reject;
        }
      '';
    };
    mss = ''
      oifname wgmesh* tcp flags syn tcp option maxseg size 1301-65535 tcp option maxseg size set 1300
      iifname wgmesh* tcp flags syn tcp option maxseg size 1301-65535 tcp option maxseg size set 1300
    '';
  in {
    network.networks4 = [ "195.39.247.64/27+" ];
    network.networks6 = [ "2a0f:4ac0:1337::/48+" ];
    networking.firewall.extraCommands =
      "ip6tables -A INPUT -p 89 -i wgmesh-+ -j ACCEPT";
    networking.nftables.extraInput = ''
      meta l4proto 89 iifname wgmesh* accept
      udp dport 3784 iifname wgmesh* accept
      ${mss}
    '';
    networking.nftables.extraOutput = mss;
    networking.nftables.extraForward = mss;

    networking.nftables.extraConfig = ''
      table inet raw {
        chain prerouting {
          type filter hook prerouting priority raw; policy accept;
          iifname "wgmesh-*" meta mark set 3920
        }
      }
    '';

    networking.policyrouting = {
      enable = true;
      rules = [
        {
          FirewallMark = 3920;
          Table = 89;
          Family = "both";
          Priority = 5000;
        }
        {
          FirewallMark = 51820;
          Table = "main";
          Family = "both";
          Priority = 6000;
        }
        {
          Table = "main";
          SuppressPrefixLength = 0;
          Family = "both";
          Priority = 7000;
        }
      ] ++ (lib.optional (!config.network.core) {
        Table = 89;
        SuppressPrefixLength = 0;
        Family = "both";
        Priority = 8000;
      }) ++ (lib.optional config.network.routeDefault {
        InvertRule = true;
        FirewallMark = 51820;
        Table = 89;
        Family = "both";
        Priority = 9000;
      });
    };

    network.wireguard = {
      enable = true;
      fwmark = 51820;
    };

    network.bird = let
      mkKernel = version: ''
        ipv${toString version} {
          import all;
          export filter {
            ${
              if (config.network."prefsrc${toString version}" or null)
              != null then
                ''
                  #if !(net ~ [ 10.0.0.0/8+, 172.16.0.0/12+, 192.168.0.0/16+ ]) then {
                    krt_prefsrc = ${config.network."prefsrc${toString version}"};
                  #}
                ''
              else
                ""
            }
            if source ~ [ RTS_STATIC, RTS_PIPE ] then reject;
            if proto = "kerneli${toString version}" then reject;
            accept;
          };
        };
        learn;
        kernel table 89;
        scan time 15;
      '';
      mkIgpCore = version: {
        version = 3;
        extra = ''
          ipv${toString version} {
            import all;
            export filter {
              if source = RTS_BGP then {
                      ospf_metric1 = 100;
                      accept;
              }
              ${filters."net${toString version}"}
            };
          };
        '';
        areas."0".interfaces."wgmeshc-*" = {
            # TODO: switch to per-interface costs, detect non-routing interfaces like keepalive setting & assign high cost
            cost = 100;
            bfd = "yes";
        };
      };
      mkIgp = version: {
        version = 3;
        extra = ''
          ipv${toString version} {
            import all;
            export filter {
              ${if config.network.core
                then
                  ''
                  if (net.len = 0) then accept;
                  ${filters."net${toString version}"}
                  ''
                else
                  "${filters."net${toString version}"}"
               }
            };
          };
        '';
        areas."1"= {
          interfaces."wgmesh-*" = {
            cost = 100;
            bfd = "yes";
          };
        };
      };
    in {
      routerId = "172.23.1.${toString mcfg.magicNumber}";
      kernel4Config = mkKernel 4;
      kernel6Config = mkKernel 6;
      ospf = {
        enable = true;
        protocols = {
          igp4 = mkIgp 4;
          igp6 = mkIgp 6;
        } // (if config.network.core then {
          igpcore4 = mkIgpCore 4;
          igpcore6 = mkIgpCore 6;
        } else {});
      };
    };

    services.bird2.config = lib.mkAfter ''
      ipv4 table kernelimport4; ipv6 table kernelimport6;
      protocol kernel kerneli4 { learn; ipv4 { table kernelimport4; import all; export none; }; }
      protocol kernel kerneli6 { learn; ipv6 { table kernelimport6; import all; export none; }; }
      protocol pipe { table master4; peer table kernelimport4; import filter { ${filters.net4} }; export none; }
      protocol pipe { table master6; peer table kernelimport6; import filter { ${filters.net6} }; export none; }
      protocol bfd { interface "wgmesh-*" { interval 200 ms; multiplier 10; }; }
    '';

    users.users.hexchen.extraGroups = [ "bird2" ];

    boot.kernel.sysctl = {
      "net.ipv6.conf.all.forwarding" = true;
      "net.ipv4.conf.all.forwarding" = true;
    };
  };
}
