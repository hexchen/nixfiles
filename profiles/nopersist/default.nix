{ config, lib, pkgs, modules, ... }:

with lib;

{
  imports = [ modules.bindmount ];

  users.mutableUsers = false;

  boot.initrd = mkIf (config.fileSystems."/".fsType or "notzfs" == "zfs") {
    network.ssh.hostKeys = mkIf config.hexchen.encboot.enable
      (mkForce [ /persist/ssh/encboot_host ]);

    postDeviceCommands = mkIf (!config.boot.initrd.systemd.enable)
      (mkAfter ''
        zfs rollback -r ${config.fileSystems."/".device}@blank
      '');

    systemd = mkIf config.boot.initrd.systemd.enable {
      storePaths = [ pkgs.zfs ];
      services.rollback = {
        description = "Rollback ZFS datasets to a pristine state";
        wantedBy = [ "initrd.target" ];
        after = [ "zfs-import-${head (splitString "/" config.fileSystems."/".device)}.service" ];
        before = [ "sysroot.mount" ];
        path = [ pkgs.zfs ];
        unitConfig.DefaultDependencies = "no";
        serviceConfig.Type = "oneshot";
        script = ''
          zfs rollback -r ${config.fileSystems."/".device}@blank && echo "rollback complete"
        '';
      };
    };
  };

  services.openssh = {
    hostKeys = [
      {
        path = "/persist/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
      {
        path = "/persist/ssh/ssh_host_rsa_key";
        type = "rsa";
        bits = 4096;
      }
    ];
  };

  services.telegraf.environmentFiles =
    mkIf config.hexchen.monitoring.enable (mkForce [ "/persist/etc/telegraf/telegraf.env" ]);

  hexchen.bindmounts = listToAttrs (map (service:
    nameValuePair "/var/lib/${service}"
    (mkIf config.services."${service}".enable "/persist/var/lib/${service}")) [
      "pleroma"
      "yggdrasil"
      "jellyfin"
      "opendkim"
      "rspamd"
      "postfix"
      "soju"
      "nsd"
      "influxdb2"
      "step-ca"
      "knot"
      "headscale"
      "tailscale"
      "grafana"
      "hedgedoc"
      "murmur"
      "samba"
    ]) // (if config.services.opendkim.enable then { "/var/dkim" = "/persist/var/dkim"; } else { })
       // (if config.services.rspamd.enable then { "/var/lib/redis-rspamd" = "/persist/var/lib/redis-rspamd"; } else { })
       // (if config.services.dovecot2.enable then { "/var/lib/dovecot" = "/persist/var/lib/dovecot"; "/var/sieve" = "/persist/var/sieve"; } else { })
       // (if config.services.factorio.enable then { "/var/lib/private/factorio" = "/persist/var/lib/factorio"; } else { })
       // (if config.services.fprintd.enable then { "/var/lib/fprint" = "/persist/var/lib/fprint"; } else { })
       // (if config.hardware.bluetooth.enable then { "/var/lib/bluetooth" = "/persist/var/lib/bluetooth"; } else { })
       // (if config.services.mullvad-vpn.enable then { "/etc/mullvad-vpn" = "/persist/etc/mullvad-vpn"; } else { })
       // (if config.services.usbmuxd.enable then { "/var/lib/lockdown" = "/persist/var/lib/lockdown"; } else { })
       // (if config.services.kanidm.enableServer then { "/var/lib/kanidm" = "/persist/var/lib/kanidm"; } else { })
       // (if config.virtualisation.libvirtd.enable then { "/var/lib/libvirt" = "/persist/var/lib/libvirt"; } else { })
       // (if config.services.prometheus.alertmanager.enable then { "/var/lib/private/alertmanager" = "/persist/var/lib/alertmanager"; } else { })
       // (if config.services.victoriametrics.enable then { "/var/lib/private/victoriametrics" = "/persist/var/lib/victoriametrics"; } else { })
       // (if config.services.openldap.enable then { "/var/lib/openldap" = "/persist/var/lib/openldap"; } else { })
       // (if config.services.flood.enable then { "/var/lib/private/flood" = "/persist/var/lib/flood"; } else { })
       // (if config.services.autobrr.enable then { "/var/lib/private/autobrr" = "/persist/var/lib/autobrr"; } else { })
       // (if config.security.acme.certs != { } then { "/var/lib/acme" = "/persist/var/lib/acme"; } else { });

  environment.etc = {
    "wpa_supplicant.conf" = mkIf config.networking.wireless.enable {
      source = "/persist/etc/wpa_supplicant.conf";
    };
  };

  services.postgresql.dataDir =
    "/persist/postgresql/${config.services.postgresql.package.psqlSchema}";
  services.jackett.dataDir = "/persist/jackett";
  services.sonarr.dataDir = "/persist/sonarr";
  services.radarr.dataDir = "/persist/radarr";
  services.plex.dataDir = "/persist/var/lib/plex";
  services.grocy.dataDir = "/persist/var/lib/grocy";
  services.paperless.dataDir = "/persist/var/lib/paperless";
  services.netbox.dataDir = "/persist/var/lib/netbox";
  services.home-assistant.configDir = "/persist/var/lib/hass";
  services.mosquitto.dataDir = "/persist/var/lib/mosquitto";
  services.loki.dataDir = "/persist/var/lib/loki";
  services.zammad.dataDir = "/persist/var/lib/zammad";
  services.mysql.dataDir = "/persist/var/lib/mysql";
}
