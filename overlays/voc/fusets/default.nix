{ stdenv, fetchFromGitHub, lib
, fuse, minixml
}:

stdenv.mkDerivation rec {
  pname = "fuse-ts";
  version = "2022.11.15";

  src = fetchFromGitHub {
    owner = "a-tze";
    repo = "fuse-ts";
    rev = "aeb6f33faec7e73ba38bd21a42d5d976511e9413"; # hardcoded cause no tags
    hash = "sha256-Zmpe8flMp3DhEEMRVBCJv8YCJcmd66Lde2bfbH9iJII=";
  };

  buildInputs = [ fuse minixml ];

  installPhase = ''
    runHook preInstall

    mkdir -p $out/bin
    cp fuse-ts $out/bin/

    runHook postInstall
  '';
}
