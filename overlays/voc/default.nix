final: prev: let
  voctomix-src = prev.fetchFromGitHub {
    owner = "voc";
    repo = "voctomix";
    rev = "23b01b1860cc65ce73e2135c9f7bd9820f938e24";
    sha256 = "19p8gh45fv7a7gd9lq3va9l6d1m3p5b643wxfx18y2wp28x3xz26";
    # rev = "3156f3546890e6ae8d379df17e5cc718eee14b15";
    # hash = "sha256-TONs4ln0eBds53JGHOEgrzjI/B602mlqIT/rBFoaDSA=";
  };
  old-nixpkgs = (import (prev.fetchFromGitHub {
    owner = "nixos";
    repo = "nixpkgs";
    rev = "e8b747b3cdd1ee5d6a35f634f5ea8be2da74b782";
    hash = "sha256-FbjntPjh9rNLE/BswO9+AXgcM/WXtwHVjANbpK51rnc=";
  }).outPath {inherit (prev) system; });

  perlPackages = prev.perlPackages // rec {
    # overrides = _: rec {
      WWWCurl = prev.perlPackages.WWWCurl.overrideAttrs (pprev: {
        buildInputs = [ old-nixpkgs.curl ];
      });
      XMLRPCFast = prev.perlPackages.buildPerlPackage {
        pname = "XML-RPC-Fast";
        version = "0.8";
        src = prev.fetchurl {
          url = "mirror://cpan/authors/id/M/MO/MONS/XML-RPC-Fast-0.8.tar.gz";
          sha256 = "8913b7da28080f450116510e383b1a8b912954f449b7b8290968d3fc56d4a6c4";
        };
        buildInputs = with prev.perlPackages; [ TestNoWarnings libabs ];
        propagatedBuildInputs = with prev.perlPackages; [ HTTPMessage LWP XMLHashLX XMLLibXML ];
        meta = {
          description = "Fast and modular implementation for an XML-RPC client and server";
          license = with prev.lib.licenses; [ artistic1 gpl1Plus ];
        };
      };
      libabs = prev.perlPackages.buildPerlPackage {
        pname = "lib-abs";
        version = "0.95";
        src = prev.fetchurl {
          url = "mirror://cpan/authors/id/M/MO/MONS/lib-abs-0.95.tar.gz";
          sha256 = "340a706817d2c1af8bde47c5b4e7f506c537f75f24425409d82c4a5fe3bffc0b";
        };
        meta = {
          description = "C<lib> that makes relative path absolute to caller";
          license = with prev.lib.licenses; [ artistic1 gpl1Plus ];
        };
      };
      XMLHashLX = prev.perlPackages.buildPerlPackage {
        pname = "XML-Hash-LX";
        version = "0.07";
        src = prev.fetchurl {
          url = "mirror://cpan/authors/id/M/MO/MONS/XML-Hash-LX-0.07.tar.gz";
          sha256 = "1fad13f7dcb80897e55f16c919c185a1829ec1a328c613e06cc6330b3b6a169d";
        };
        buildInputs = with prev.perlPackages; [ libabs ];
        propagatedBuildInputs = with prev.perlPackages; [ XMLLibXML TypesSerialiser ];
        meta = {
          description = "Convert hash to xml and xml to hash using LibXML";
          license = with prev.lib.licenses; [ artistic1 gpl1Plus ];
        };
      };
    # };
  };
in {
  voctoPython = prev.python3Packages.override {
    overrides = pfinal: pprev: {
      vocto =
        prev.python3Packages.callPackage ./vocto { src = voctomix-src; };
    };
  };
  voctocore = prev.callPackage ./voctocore {
    src = voctomix-src;
    python3Packages = final.voctoPython;
  };
  voctogui = prev.callPackage ./voctogui {
    src = voctomix-src;
    python3Packages = final.voctoPython;
  };

  fuse-ts = prev.callPackage ./fusets { };
  crs-worker = final.callPackage ./crs-worker { inherit perlPackages; };
}
