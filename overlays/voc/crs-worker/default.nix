{ stdenv, lib, fetchFromGitHub, writeShellScript, linkFarm
, perl, perlPackages, ffmpeg-full, fuse-ts
}:

stdenv.mkDerivation rec {
  pname = "crs-scripts";
  version = "2022.10.2.1";

  src = fetchFromGitHub {
    owner = "crs-tools";
    repo = "crs-scripts";
    rev = "f6a47c3b872ffc12cff57ad6e62f53f90facb4c4";
    hash = "sha256-LuHoaDPqVy3XZagalAxU1ucZXTs+u/VvyaWxp+Bsex4=";
  };

  patches = [ ./fix-unmount.patch ];

  phases = [ "unpackPhase" "patchPhase" "installPhase" "fixupPhase" ];

  installPhase = ''
    runHook preInstall

    mkdir -p "$out/opt/CRS"
    mkdir -p "$out/bin"

    cp -r lib scripts $out/opt/CRS

    runHook postInstall
  '';

  postInstall = let
    deps = _: with perlPackages; [
      boolean ConfigIniFiles DateTime FileWhich IPCRun3 JSON MathRound ProcProcessTable WWWCurl XMLRPCFast XMLSimple
      IOSocketSSL LWPProtocolHttps TypesSerialiser
    ];
    nativeDeps = [
      ffmpeg-full fuse-ts
    ];
    wrapper = script: writeShellScript "${script}" ''
      export PERL5LIB="$PERL5LIB${"$"}{PERL5LIB:+:}@out@/opt/CRS/lib"
      export PATH="$PATH${"$"}{PATH:+:}${lib.makeBinPath nativeDeps}"

      ${perlPackages.perl.withPackages deps}/bin/perl @out@/opt/CRS/scripts/${script} $@
    '';
    scripts = lib.filterAttrs (name: type: type == "regular") (builtins.readDir "${src}/scripts");
    wrappers = linkFarm "crsworker-wrappers" (lib.mapAttrsToList (name: _: { inherit name; path = wrapper name;}) scripts);
  in ''
    cp --dereference -r ${wrappers}/* $out/bin/
    ${lib.concatStringsSep "\n" (lib.mapAttrsToList (name: _: "substituteInPlace $out/bin/${name} --replace @out@ $out") scripts)}
  '';

  preFixup = ''
    substituteInPlace $out/opt/CRS/lib/CRS/Fuse.pm --replace "'/usr/bin'" "'${fuse-ts}/bin'"
  '';

}
