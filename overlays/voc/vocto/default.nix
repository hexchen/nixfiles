{ buildPythonPackage, fetchFromGitHub, python, gobject-introspection, gst-python
, pygobject3, gst_all_1, src, }:

buildPythonPackage rec {
  pname = "vocto";
  version = "2.0a";
  format = "other";

  inherit src;

  propagatedBuildInputs = with gst_all_1; [
    gobject-introspection
    gst-python
    pygobject3
    gst-plugins-good
    gst-plugins-bad
    gst-plugins-ugly
    gst-plugins-base
    gst-vaapi
    gst-libav
  ];

  strictDeps = false;

  installPhase = ''
    runHook preInstall
    mkdir -p $out/lib/${python.libPrefix}/site-packages
    cp -r "vocto" $out/lib/${python.libPrefix}/site-packages
    runHook postInstall
  '';
}
