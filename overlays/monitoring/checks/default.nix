pkgs:

{
  sshmon = pkgs.writeShellApplication {
    name = "sshmon";
    text = (builtins.readFile ./sshmon.sh);
    runtimeInputs = [ pkgs.openssh ];
  };
  certat = pkgs.writeShellApplication {
    name = "certat";
    text = (builtins.readFile ./certat.sh);
    runtimeInputs = [ pkgs.openssl ];
  };
  httpwget = pkgs.writeScript "httpwget" ''
    #!${pkgs.python3.withPackages (py: [ py.requests ])}/bin/python3
    ${builtins.readFile ./httpwget.py}
  '';
  spamblocklist = pkgs.writeScript "spamblocklist" ''
    #!${pkgs.python3.withPackages (py: [ py.dnspython ])}/bin/python3
    ${builtins.readFile ./spamblocklist.py}
  '';
  check_zfs = pkgs.writeScript "check_zfs" ''
    #!${pkgs.perl}/bin/perl
    ${builtins.readFile ./check_zfs.pl}
  '';
}
