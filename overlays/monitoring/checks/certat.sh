
host=$1
port=$2

cert=$(echo | openssl s_client -connect "$host":"$port" -servername "$host" 2>/dev/null | openssl x509)
issuer_hash=$(echo "$cert" | openssl x509 -noout -issuer_hash)
not_after=$(echo "$cert" | openssl x509 -noout -dates | grep '^notAfter=')
subject=$(echo "$cert" | openssl x509 -noout -nameopt multiline -subject | sed -En 's/.*commonName += +(.*)/\1/p')
san=$(echo "$cert" | openssl x509 -noout -ext subjectAltName)

if [[ -z "$cert" || -z "$issuer_hash" || -z "$not_after" || -z "$subject" || -z "$san" ]]
then
    echo "UNKNOWN - Could not retrieve certificate! [$host:$port]"
    exit 3
fi

warn_days=60
crit_days=30

case "$issuer_hash" in
    # 4f06f81d: issuer=C = US, O = Let's Encrypt, CN = Let's Encrypt Authority X3
    # 8d33f237: issuer=C = US, O = Let's Encrypt, CN = R3
    4f06f81d|8d33f237)
        warn_days=10
        crit_days=3
        ;;
esac

# OpenSSL has a terrible output format that looks like this:
#
# X509v3 Subject Alternative Name:
#     DNS:*.smedia.tools, DNS:smedia.tools
#
# We turn all spaces into newlines, then look for lines starting with
# "DNS:", and then remove the trailing comma.
san_names=$(echo "$san" | tr ' ' '\n' | sed -En 's/^DNS:([^,]+),?$/\1/p')

printf -v all_cert_names '%s\n%s' "$subject" "$san_names"
cert_name_ok=false
while read -r
do
    # Unquoted right hand, so, yes, really do interpret it as a glob.
    # This allows names like "*.foo.de" to match "$host = bar.foo.de".
    if [[ "$host" == "$REPLY" ]]
    then
        cert_name_ok=true
        break
    fi
done <<<"$san_names"
if [[ "$cert_name_ok" == false ]]
then
    oneline=$(echo "$all_cert_names" | paste -sd' ')
    echo "CRITICAL - Certificate name does not match! [$host:$port] [$oneline]"
    exit 2
fi

if ! echo "$cert" | openssl x509 -noout -checkend 0 >/dev/null 2>&1
then
    echo "CRITICAL - Certificate has expired! [$host:$port] [$not_after]"
    exit 2
elif ! echo "$cert" | openssl x509 -noout -checkend $((86400 * crit_days)) >/dev/null 2>&1
then
    echo "CRITICAL - Certificate will expire really soon: [$host:$port] [$not_after]"
    exit 2
elif ! echo "$cert" | openssl x509 -noout -checkend $((86400 * warn_days)) >/dev/null 2>&1
then
    echo "WARNING - Certificate will expire soon: [$host:$port] [$not_after]"
    exit 1
fi

echo "OK - [$host:$port] [$not_after]"
exit 0
