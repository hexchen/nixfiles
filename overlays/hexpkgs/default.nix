final: prev: {
  binaryninja = prev.callPackage ./binaryninja { };

  # cups-kyodialog3 = prev.cups-kyodialog3.overrideAttrs(pprev: {
  #   src = fetchTarball {
  #     url = "https://www.kyoceradocumentsolutions.us/content/download-center-americas/us/drivers/drivers/Kyocera_Linux_PPD_Ver_${prev.lib.replaceChars ["."] ["_"] pprev.version}_tar_gz.download.gz";
  #     sha256 = "11znnlkfssakml7w80gxlz1k59f3nvhph91fkzzadnm9i7a8yjal";
  #   };
  # });

  monolisa = prev.callPackage ./monolisa { };

  hh3 = final.callPackage ./hh3 { };
  hh3cord = prev.discord.overrideAttrs (dprev: {
    installPhase = dprev.installPhase + "\n" + ''
      ${final.hh3}/bin/hh3 $out/opt/Discord/Discord
    '';
  });
  hh3cord-canary = prev.discord-canary.overrideAttrs (dprev: {
    installPhase = dprev.installPhase + "\n" + ''
      ${final.hh3}/bin/hh3 $out/opt/DiscordCanary/DiscordCanary
    '';
  });

  rofi = final.rofi-wayland;

  rtorrent_0_10 = (prev.rtorrent.overrideAttrs (super: rec {
    version = "0.10.0";
    src = prev.fetchFromGitHub {
      owner = "rakshasa";
      repo = "rtorrent";
      rev = "v${version}";
      hash = "sha256-G/30Enycpqg/pWC95CzT9LY99kN4tI+S8aSQhnQO+M8=";
    };
    configureFlags = [
      "--with-xmlrpc-c"
      "--with-posix-fallocate"
    ];
    buildInputs = super.buildInputs ++ [ prev.libsigcxx prev.xmlrpc_c ];
  })).override ({ libtorrent = final.libtorrent_0_14; });

  libtorrent_0_14 = prev.libtorrent.overrideAttrs (super: rec {
    version = "0.14.0";

    src = prev.fetchFromGitHub {
      owner = "rakshasa";
      repo = "libtorrent";
      rev = "v${version}";
      hash = "sha256-MDLAp7KFmVvHL+haWVYwWG8gnLkTh6g19ydRkbu9cIs=";
    };
  });

  rtorrent-exporter = prev.callPackage ./rtorrent-exporter {  };
}
