{ stdenv, requireFile, unzip }:
stdenv.mkDerivation rec {
  pname = "monolisa";
  version = "shrug";

  src = requireFile rec {
    name = "MonoLisa.zip";
    sha256 = "476db1f38907daa69c52378caec63b071ba032e5876eb8d9a7ae6ff885c77044";
    message = "Download MonoLisa from the appropriate place, idk?";
  };

  nativeBuildInputs = [ unzip ];

  dontInstall = true;

  unpackPhase = ''
    mkdir -p $out/share/fonts
    unzip -jd $out/share/fonts/truetype $src 'MonoLisa/ttf/*'
  '';
}
