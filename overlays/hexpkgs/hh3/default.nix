{ mkPnpmPackage }:

mkPnpmPackage {
  name = "hh3";
  # currently, hh3 is private. builds will fail unless you have been given access to the repo.
  src = builtins.fetchGit {
    url = "gitolite3@git.coolmathgames.tech:hh3.git";
    rev = "6e426340f8c99ad7dc7f285cc13dfa88211aeb73";
    ref = "master";
  };
}
