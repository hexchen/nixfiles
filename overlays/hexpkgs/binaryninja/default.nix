{ stdenv, alsa-lib, autoPatchelfHook, dbus, fontconfig, freetype, glib, krb5
, libglvnd, libICE, libSM, libX11, libXcomposite, libXcursor, libXdamage
, libXext, libXi, libxkbcommon, libXrandr, libXrender, libXtst, makeWrapper, nss
, ncurses, qt6Packages
, python38, requireFile, systemd, unzip, xcbutilimage, xcbutilkeysyms
, xcbutilrenderutil, xcbutilwm, xkeyboardconfig, zlib }:
stdenv.mkDerivation rec {
  pname = "binaryninja";
  version = "3.1.3469";

  src = requireFile rec {
    name = "BinaryNinja-personal.zip";
    url = "https://binary.ninja";
    sha256 = "33aa0e9c35e84322ad7ffd6fc3959535e019c72cdc8a719536aad9be9330ccbb";
  };

  buildInputs = [
    alsa-lib 
    autoPatchelfHook
    dbus
    fontconfig
    freetype
    glib
    krb5
    libglvnd
    libICE
    libSM
    libX11
    libXcomposite
    libXcursor
    libXdamage
    libXext
    libXi
    libxkbcommon
    libXrandr
    libXrender
    libXtst
    makeWrapper
    nss
    ncurses
    qt6Packages.qtbase
    qt6Packages.qtdeclarative
    python38
    stdenv.cc.cc.lib
    unzip
    xcbutilimage
    xcbutilkeysyms
    xcbutilrenderutil
    xcbutilwm
    zlib
  ];

  dontStrip = true;
  dontPatchELF = true;
  dontWrapQtApps = true;

  installPhase = ''
    mkdir -p $out/lib $out/bin $out/share
    mv $NIX_BUILD_TOP/$sourceRoot $out/lib/binary-ninja
    makeWrapper $out/lib/binary-ninja/binaryninja $out/bin/binaryninja \
        --suffix LD_LIBRARY_PATH : "${systemd}/lib" \
        --suffix LD_LIBRARY_PATH : "${python38}/lib" \
        --set QT_XKB_CONFIG_ROOT "${xkeyboardconfig}/share/X11/xkb" \
        --set QTCOMPOSE "${libX11.out}/share/X11/locale"
    # Keeping the zip file in the nix store is desirable,
    # because when the zip is missing requireFile involves manual steps.
    # Below is just a hack to keep the zip from being garbage-collected.
    ln -s "${src}" "$out/share/BinaryNinja-personal.zip"
  '';

  postFixup = ''
    mkdir -p $out/share/applications
    HOME=/build $out/lib/binary-ninja/scripts/linux-setup.sh -l -p -m
    cp /build/.local/share/applications/*.desktop $out/share/applications/
    sed -i "s|lib/binary-ninja/binaryninja|bin/binaryninja|g" $out/share/applications/*
  '';
}
