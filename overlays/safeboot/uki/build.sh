#!/usr/bin/env bash

source $stdenv/setup

set -o pipefail

stub_line=$(objdump -h "$systemd/lib/systemd/boot/efi/linuxx64.efi.stub" | tail -2 | head -1)
stub_size=0x$(echo "$stub_line" | awk '{print $3}')
stub_offs=0x$(echo "$stub_line" | awk '{print $4}')
osrel_offs=$((stub_size + stub_offs))
cmdline_offs=$((osrel_offs + $(stat -c%s "$osrel")))
linux_offs=$((cmdline_offs + $(stat -c%s "$cmdline")))
initrd_offs=$((linux_offs + $(stat -c%s "$kernel")))

objcopy \
  --add-section .osrel="$osrel" --change-section-vma .osrel=$(printf 0x%x $osrel_offs) \
  --add-section .cmdline="$cmdline" --change-section-vma .cmdline=$(printf 0x%x $cmdline_offs) \
  --add-section .linux="$kernel" --change-section-vma .linux=$(printf 0x%x $linux_offs) \
  --add-section .initrd="$initrd" --change-section-vma .initrd=$(printf 0x%x $initrd_offs) \
  "$systemd/lib/systemd/boot/efi/linuxx64.efi.stub" "$out"
