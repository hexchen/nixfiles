{ stdenvNoCC, systemd, binutils, writeText
, kernel
, initrd
, cmdline
, osrel
}:

stdenvNoCC.mkDerivation {
  inherit systemd kernel initrd osrel;
  name = "unified.efi";
  builder = ./build.sh;

  cmdline = writeText "uki-cmdline" cmdline;

  nativeBuildInputs = [ binutils ];
}
