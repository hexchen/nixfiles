final: prev: {
  unified-kernel-image = args: prev.callPackage ./uki args;
  sbsigntool = prev.sbsigntool.overrideAttrs (pprev: {
      patches = pprev.patches ++ [ ./sbsigntool/leak_engine.patch ./sbsigntool/hash_only.patch ];
  });
}
