{ config, lib, pkgs, hostname, hosts, ... }:

with lib;

let
  cfg = config.network.wireguard;
  mcfg = hosts.${hostname}.meta.wireguard;
  hcfg = _: h: h.meta.wireguard;
  netHostsSelf =
    mapAttrs hcfg (filterAttrs (_: x: x.meta.wireguard or { } != { }) hosts);
  netHosts = filterAttrs (_: x: x.pubkey != mcfg.pubkey) netHostsSelf;

  connectableHosts = (filterAttrs (_: x:
    # hosts are connectable if either me or them have a public IP
    !(
      x.publicAddress4 or null == null &&
      x.publicAddress6 or null == null &&
      mcfg.publicAddress4 or null == null &&
      mcfg.publicAddress6 or null == null
    ) ) netHosts);
  iname = hname: hconf:
    if (hconf.core or false && mcfg.core or false) then
      "wgmeshc-${substring 0 7 hname}"
    else
      "wgmesh-${substring 0 8 hname}";
  magicSum = hconf: hconf.magicNumber + mcfg.magicNumber;
in {
  options.network.wireguard = {
    enable = mkEnableOption "semi-automatic wireguard mesh";
    prefix = mkOption {
      type = types.str;
      default = "172.23";
    };
    keyPath = mkOption {
      type = types.str;
      default = "/etc/wireguard/mesh";
    };
    fwmark = mkOption {
      type = types.nullOr types.ints.u16;
      default = null;
    };
    mtu = mkOption {
      type = types.ints.u16;
      default = 1500;
    };
  };

  config = mkIf cfg.enable {
    systemd.network.enable = true;
    systemd.network.netdevs = mapAttrs' (hname: hconf:
      let magicPort = 51820 + (magicSum hconf);
      in nameValuePair (iname hname hconf) {
        netdevConfig = {
          Name = iname hname hconf;
          Kind = "wireguard";
          # what. why the fuck do i need to manually toString this
          MTUBytes = toString cfg.mtu;
        };
        wireguardConfig = {
          FirewallMark = cfg.fwmark;
          ListenPort = magicPort;
          PrivateKeyFile = cfg.keyPath;
          RouteTable = "off";
        };
        wireguardPeers = [{
          wireguardPeerConfig = {
            PublicKey = hconf.pubkey;
            AllowedIPs = [ "0.0.0.0/0" "::/0" ];
            Endpoint = with hconf;
              mkIf (hconf.publicAddress4 or null != null
                || hconf.publicAddress6 or null != null)
              (if (hconf.publicAddress4 or null != null) then
                "${publicAddress4}:${toString magicPort}"
              else
                "[${publicAddress6}]:${toString magicPort}");
            PersistentKeepalive = mkIf (hconf.publicAddress4 or null == null
              && hconf.publicAddress6 or null == null) 25;
          };
        }];
      }) connectableHosts;

    systemd.network.networks = mapAttrs' (hname: hconf:
      nameValuePair (iname hname hconf) {
        matchConfig.Name = iname hname hconf;
        networkConfig.Address = [
          "${cfg.prefix}.${toString (magicSum hconf)}.${toString mcfg.magicNumber}/24"
          "fe80::${toString mcfg.magicNumber}/64"
        ];
        routes = [{
          routeConfig.Destination = "${cfg.prefix}.${toString (magicSum hconf)}.${toString hconf.magicNumber}/32";
        }];
      }) connectableHosts;

    # networking.wireguard.interfaces = mapAttrs' (hname: hconf:
    #   let
    #     magicPort = 51820 + hconf.magicNumber + mcfg.magicNumber;
    #     magicSum = hconf.magicNumber + mcfg.magicNumber;
    #     iname = if (hconf.core or false && mcfg.core or false) then "wgmeshc-${substring 0 7 hname}" else "wgmesh-${substring 0 8 hname}";
    #   in nameValuePair iname {
    #     allowedIPsAsRoutes = false;
    #     privateKeyFile = cfg.keyPath;
    #     ips = [
    #       "${cfg.prefix}.${toString magicSum}.${toString mcfg.magicNumber}/24"
    #       "fe80::${toString mcfg.magicNumber}/64"
    #     ];
    #     listenPort = magicPort;
    #     peers = [{
    #       publicKey = hconf.pubkey;
    #       allowedIPs = [ "0.0.0.0/0" "::/0" ];
    #       endpoint = with hconf;
    #         mkIf (hconf.publicAddress4 or null != null || hconf.publicAddress6 or null != null)
    #         (if (hconf.publicAddress4 or null != null) then
    #           "${publicAddress4}:${toString magicPort}"
    #         else
    #           "[${publicAddress6}]:${toString magicPort}");
    #       persistentKeepalive =
    #         mkIf (hconf.publicAddress4 or null == null && hconf.publicAddress6 or null == null) 25;
    #     }];
    #     postSetup = ''
    #       ip route add ${cfg.prefix}.${
    #         toString hconf.magicNumber
    #       }/32 dev ${iname}
    #       ${optionalString (cfg.fwmark != null)
    #       "wg set ${iname} fwmark ${toString cfg.fwmark}"}
    #       ip link set ${iname} mtu ${toString cfg.mtu}
    #     '';
    #   }) (filterAttrs (_: x:
    #     !(x.publicAddress4 or null == null && x.publicAddress6 or null == null
    #       && mcfg.publicAddress4 or null == null && mcfg.publicAddress6 or null == null))
    #     netHosts);

    networking.firewall.allowedUDPPorts =
      mapAttrsToList (_: hconf: 51820 + (magicSum hconf)) netHosts;

    systemd.network.wait-online.ignoredInterfaces = mapAttrsToList (hname: hconf: iname hname hconf) netHosts;
  };
}
