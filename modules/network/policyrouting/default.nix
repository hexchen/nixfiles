{ config, lib, ... }:

with lib;

let
  cfg = config.networking.policyrouting;

in {
  options = {
    networking.policyrouting = {
      enable = mkEnableOption "Declarative Policy-Routing";
      rules = mkOption {
        type = with types; listOf attrs;
        default = [ ];
      };
    };
  };

  config = mkIf cfg.enable {
    networking.policyrouting.rules = [{
      Table = "main";
      Family = "both";
      Priority = 32000;
    }];

    systemd.network.netdevs.policy.netdevConfig = {
      Name = "policy";
      Kind = "dummy";
    };
    systemd.network.networks.policy = {
      matchConfig.Name = "policy";
      networkConfig.LinkLocalAddressing = "no";
      routingPolicyRules = map (x: { routingPolicyRuleConfig = x; }) cfg.rules;
    };
  };
}
