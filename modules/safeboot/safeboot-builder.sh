#! @bash@/bin/sh -e

shopt -s nullglob

export PATH=/empty
for i in @path@; do PATH=$PATH:$i/bin; done

espvol=$(mount | grep /boot | awk '{print $1;}')

cleanName() {
    local path="$1"
    echo "$path" | sed 's|^/nix/store/||' | sed 's|/|-|g'
}

addEntry() {
    local path=$1
    local generation=$2
    local name="nixos-$generation"

    if [ ! -f "@esp@/EFI/safeboot/$name.efi" ]; then
        echo "init=$(readlink "$path")/init $(cat $path/kernel-params)" > /tmp/cmdline

        echo "Building generation $generation..."
	
        local stub_line=$(objdump -h "$path/systemd/lib/systemd/boot/efi/linuxx64.efi.stub" | tail -2 | head -1)
        local stub_size=0x$(echo "$stub_line" | awk '{print $3}')
        local stub_offs=0x$(echo "$stub_line" | awk '{print $4}')
        local osrel_offs=$((stub_size + stub_offs))
        local cmdline_offs=$((osrel_offs + $(stat -c%s "$(readlink -f "$path/etc/os-release")")))
        local linux_offs=$((cmdline_offs + $(stat -c%s "/tmp/cmdline")))
        local initrd_offs=$((linux_offs + $(stat -c%s "$(readlink -f "$path/kernel")")))

        objcopy \
            --add-section .osrel="$path/etc/os-release" --change-section-vma .osrel=$(printf 0x%x $osrel_offs) \
            --add-section .cmdline="/tmp/cmdline" --change-section-vma .cmdline=$(printf 0x%x $cmdline_offs) \
            --add-section .linux="$path/kernel" --change-section-vma .linux=$(printf 0x%x $linux_offs) \
            --add-section .initrd="$path/initrd" --change-section-vma .initrd=$(printf 0x%x $initrd_offs) \
            "$path/systemd/lib/systemd/boot/efi/linuxx64.efi.stub" "@esp@/EFI/safeboot/$name.efi"

        efibootmgr -q -cb$(printf "%04x" $(($generation+256))) -d $espvol --loader "\\EFI\\safeboot\\$name.efi" -L \
            "NixOS - Gen $generation $(date '+%Y-%m-%d')"
    fi
}

echo "Setting up safeboot..."

mkdir -p @esp@/EFI/safeboot

generations=$(
    (cd /nix/var/nix/profiles && ls -d system-*-link) \
        | sed 's/system-\([0-9]\+\)-link/\1/' \
        | sort -n -r)
bootorder=""

existing=$((cd "@esp@/EFI/safeboot" && ls nixos-* || echo "") | sed 's/nixos-\([0-9]\+\).efi\(.signed\)\?/\1/' | uniq)

for gen in $existing; do
    echo $generations | grep -q -w "$gen" || (
        echo "Removing outdated generation $gen"
        rm "@esp@/EFI/safeboot/nixos-$gen"*
        efibootmgr -b$(printf "%04x" $(($gen+256))) -B -q
    )
done

for generation in $generations; do
    link=/nix/var/nix/profiles/system-$generation-link
    addEntry $link $generation
    bootorder="$bootorder$(printf "%04x" $(($generation+256))),"
done

echo "Setting bootorder: $(echo $bootorder)@extraOrder@"
efibootmgr -q -o "$(echo $bootorder)@extraOrder@"
