{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.boot.loader.safeboot;

  safebootBuilder = pkgs.substituteAll {
    src = ./safeboot-builder.sh;
    isExecutable = true;
    inherit (pkgs) bash;
    path = with pkgs; [coreutils gnused gnugrep gawk efibootmgr binutils mount];
    esp = config.boot.loader.efi.efiSysMountPoint;
    extraOrder = concatStringsSep "," cfg.extraOrder;
  };

in {
  options.boot.loader.safeboot = {
    enable = mkOption {
      default = false;
      type = types.bool;
    };
    extraOrder = mkOption {
      default = [];
      type = types.listOf types.str;
    };
    # TODO: actual safeboot, for now just efistub; that's painful enough tbh.
  };

  config = mkIf cfg.enable {
    system.build.installBootLoader = safebootBuilder;
    system.boot.loader.id = "safeboot";
  };
}
