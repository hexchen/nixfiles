{ config, lib, pkgs, hosts ? { a.nixosConfiguration = { inherit config; }; }, ... }:

with lib;

let
  cfg = config.hexchen.dns;
  dns = pkgs.dns;
in {
  options.hexchen.dns = with lib; {
    enable = mkEnableOption "hexchens dns stuff";
    primary = mkOption {
      type = types.bool;
      description = "make this server a primary server";
      default = true;
    };
    xfrIPs = mkOption {
      type = types.listOf types.str;
      description = "Own IPs to do AXFR with";
      default = [ ];
    };
    zones = mkOption {
      type = types.attrsOf dns.types.subzone;
      description = "DNS zones";
      default = { };
    };
    allZones = mkOption {
      type = types.attrsOf dns.types.zone;
      description = "all zones, merged from all hosts (if applicable)";
      default = { };
    };
    dnssec.enable = mkOption {
      type = types.bool;
      default = false;
      description = "enable dnssec signing";
    };
    symlinkZones = mkOption {
      type = types.bool;
      default = false;
    };
    extraAcls = mkOption {
      type = types.listOf types.str;
      default = [ ];
    };
    extraAclDefs = mkOption {
      type = types.attrs;
      default = {};
    };
  };

  config = mkIf cfg.enable {
    networking.firewall.allowedUDPPorts = [ 53 ];
    networking.firewall.allowedTCPPorts = [ 53 ];

    hexchen.dns.allZones = mkIf cfg.primary (mkMerge
      (mapAttrsToList (name: host: host.nixosConfiguration.config.hexchen.dns.zones or {}) hosts));

    services.kresd = {
      enable = true;
      listenPlain = mkOverride 800 [ "127.0.0.1:5353" ];
      instances = 1;
      extraConfig = ''
        policy.add(policy.all(policy.FLAGS({'NO_CACHE'})))
      '';
    };

    services.knot = let
      cfgs = filterAttrs (h: c: c.enable)
        (mapAttrs (name: host: host.nixosConfiguration.config.hexchen.dns or { enable = false; }) hosts);
      primaries = filterAttrs (h: c: c.primary) cfgs;
      secondaries = filterAttrs (h: c: !c.primary) cfgs;
      peers = if cfg.primary then secondaries else primaries;

      frontPad = padding: string: concatMapStringsSep "\n" (x: padding + x) (splitString "\n" string);
    in {
      enable = true;
      settings = mkMerge [
        {
          server.listen = [ "0.0.0.0@53" "::@53" ];
          log = [{
            target = "syslog";
            any = "info";
          }];
          acl = {
            xfr_ipauth = {
              address = flatten (mapAttrsToList (h: c: c.xfrIPs) peers);
              action = if cfg.primary then "transfer" else "notify";
            };
            update_ipauth = {
              address = [ "127.0.0.1" "::1" ];
              action = "update";
            };
          } // cfg.extraAclDefs;
          remote = [{
            id = "local_kresd";
            address = "127.0.0.1@5353";
          }] ++ (mapAttrsToList (host: c: {
            id = host;
            address = c.xfrIPs;
          }) peers);

          template = [
            (
              {
                id = "default";
                storage = "/var/lib/knot/zones";
                zonefile-sync = "-1";
                journal-content = "all";
                zonefile-load = "difference-no-serial";
                semantic-checks = "on";
                acl = [ "xfr_ipauth" "update_ipauth" ] ++ cfg.extraAcls;
              }
              // (optionalAttrs cfg.primary { notify = attrNames peers; })
              // (optionalAttrs (!cfg.primary) { master = attrNames peers; })
              // (optionalAttrs cfg.dnssec.enable {
                dnssec-signing = "on";
                dnssec-policy = "custom_policy";
              })
            )
          ];

          zone = mapAttrsToList (domain: _: { inherit domain; }) cfg.allZones;
        }

        (optionalAttrs cfg.dnssec.enable {
          submission = [{ id = "verify_kresd"; parent = ["local_kresd"]; }];
          policy = [{
            id = "custom_policy";
            signing-threads = 4;
            algorithm = "ECDSAP256SHA256";
            zsk-lifetime = "30d";
            ksk-lifetime = "365d";
            ksk-submission = "verify_kresd";
            cds-cdnskey-publish = "rollover";
          }];
        })
      ];
    };

    systemd.services.knot.preStart =
      "chmod -R u+w /var/lib/knot/zones && cp --dereference /etc/hexdns/zones/* /var/lib/knot/zones/";
    systemd.services.knot.serviceConfig.ExecReload = mkForce (pkgs.writeShellScript "knot-reload" ''
      set -e
      /run/current-system/sw/bin/chmod -R u+w /var/lib/knot/zones
      /run/current-system/sw/bin/cp --dereference /etc/hexdns/zones/* /var/lib/knot/zones/
      ${config.services.knot.package}/bin/knotc reload
    '');
    systemd.services.knot.reloadTriggers =
      mapAttrsToList (name: zone: pkgs.writeText "${name}.zone" (toString zone)) cfg.allZones;

    environment.etc = mkIf cfg.symlinkZones (mapAttrs' (name: zone: {
      name = "hexdns/zones/${name}.zone";
      value = { source = pkgs.writeText "${name}.zone" (toString zone); };
    }) cfg.allZones);
  } // {
    nixpkgs.overlays = [ (import ../../overlays/dns) ];
  };
}
