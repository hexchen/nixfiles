{ config, lib, pkgs, overlays, modules, ... }:

with lib;

# implementation is all in services/icinga2.
# it's a mess.

let
  cfg = config.monitoring;
  settingsType = types.oneOf [ types.bool types.str types.int (types.listOf (types.oneOf [ types.str types.int ])) ];
in {
  imports = [
    modules.sshmon
    ./vmagent.nix
    ./promtail.nix
  ];

  options.monitoring = {
    enable = mkOption {
      type = types.bool;
      default = true;
    };
    target = mkOption {
      type = types.str;
      default = config.networking.fqdn;
    };
    settings = mkOption {
      type = types.attrsOf settingsType;
      default = {};
    };
    services = mkOption {
      type = types.attrsOf (types.attrsOf settingsType);
      default = {};
    };
    sshmonServices = mkOption {
      type = types.attrsOf (types.submodule {
        options = {
          enable = mkOption { type = types.bool; default = true; };
          check_command = mkOption { type = types.str; };
          target = mkOption { type = types.str; default = "self"; };
          timeout = mkOption { type = types.int; default = -1; };
          settings = mkOption { type = types.attrsOf settingsType; default = {}; };
        };
      });
      default = {};
    };
    sshmon.port = mkOption {
      type = types.port;
      default = head config.services.openssh.ports;
    };
    sshmon.pubkey = mkOption {
      type = types.str;
    };
    generators.nginx = mkOption {
      type = types.bool;
      default = true;
    };
  };
  config = mkIf cfg.enable {
    warnings = optional (cfg.target != config.networking.fqdn) "monitoring.target is not set to the FQDN (${cfg.target}). Please adjust DNS.";

    nixpkgs.overlays = [ overlays.monitoring ];

    monitoring.settings.address = mkDefault cfg.target;
    monitoring.settings."vars.notification.mail" = mkDefault true;
    monitoring.settings."vars.notification.sms" = mkDefault true;
    monitoring.settings."vars.period" = mkDefault "24x7";

    # herewego, default checks!
    monitoring.services =
      {} //
      (optionalAttrs cfg.generators.nginx (
        (mapAttrs'
          (h: _: nameValuePair "NGINX CERT ${h}" {
            check_command = "check_certificate_at";
            "vars.domain" = h;
            "vars.notification.sms" = mkDefault false;
            "vars.notification.mail" = mkDefault true;
          })
          (filterAttrs (_: c: c.addSSL || c.forceSSL) config.services.nginx.virtualHosts)
        )
      ));
    monitoring.sshmonServices =
      {
        "SYSTEMD UNITS" = {
          check_command = "${pkgs.check_systemd}/bin/check_systemd -t";
          settings."vars.notification.sms" = mkDefault false;
          settings."vars.notification.mail" = mkDefault true;
        };
        "SYSTEM LOAD" = {
          check_command = "${pkgs.monitoring-plugins}/bin/check_load -r -w 4,2,1 -c 6,3,2";
          settings."vars.notification.sms" = mkDefault false;
          settings."vars.notification.mail" = mkDefault true;
        };
      } //
      (mapAttrs'
        (mp: fs: nameValuePair "DISK ${mp}" {
          check_command = "${pkgs.monitoring-plugins}/bin/check_disk -w 20% -c 10% -W 20% -K 10% -p ${mp}";
          settings."vars.notification.sms" = mkDefault false;
          settings."vars.notification.mail" = mkDefault true;
        })
        (filterAttrs (_: fs: !(elem fs.fsType [ "tmpfs" "nfs" "zfs" ]) && !(elem "bind" fs.options)) config.fileSystems)
      ) // optionalAttrs (elem "zfs" (mapAttrsToList (_: fs: fs.fsType) config.fileSystems)) {
        "ZFS POOLS" = {
          check_command = "${pkgs.icinga-checks.check_zfs}";
          settings."vars.notification.sms" = mkDefault true;
        };
        "ZFS DATASETS" = {
          check_command = "sudo ${pkgs.monitoring-plugins}/bin/check_disk -w 20% -c 10% -W 20% -K 10% -A -N zfs";
          settings."vars.notification.sms" = mkDefault false;
          settings."vars.notification.mail" = mkDefault true;
        };
      };
  };
}
