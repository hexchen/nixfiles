{ config, lib, pkgs, ... }:

let
  cfg = config.monitoring.promtail;
in {
  options.monitoring.promtail = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = true;
    };
  };

  config = lib.mkIf cfg.enable {
    sops.secrets.promtail = {
      key = "vmagent";
      sopsFile = ./monitoring.sops.yaml;
      owner = "promtail";
    };
    users.users.promtail.extraGroups = [ config.users.groups.keys.name ];

    services.promtail = {
      enable = true;
      configuration = {
        server.disable = true;
        positions.filename = "/tmp/positions.yaml";
        clients = [{
          url = "https://loki.chaoswit.ch/loki/api/v1/push";
          basic_auth.username = "loki-write";
          basic_auth.password_file = config.sops.secrets.promtail.path;
        }];
        scrape_configs = [
          {
            job_name = "journal";
            journal = {
              json = true;
              labels.job = "systemd-journal";
              labels.node = config.networking.fqdn;
            };
            relabel_configs = [
              {
                source_labels = [ "__journal__systemd_unit" ];
                target_label = "unit";
              }
              {
                source_labels = [ "__journal_syslog_identifier" ];
                target_label = "syslog_identifier";
              }
            ];
          }
        ];
      };
    };
  };
}
