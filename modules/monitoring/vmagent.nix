{ config, lib, pkgs, ... }:

let
  cfg = config.monitoring.vmagent;
  ignores = [ "assertions" "warnings" "blackbox" "unifi-poller" "domain" "minio" "tor" ];
  _exporters = lib.filterAttrs (n: _: !lib.elem n ignores) config.services.prometheus.exporters;
  exporters = (lib.filterAttrs (_: v: v.enable) _exporters) // cfg.extraExporters;

  scrape_config = n: v: {
    job_name = "${n}";
    static_configs = [{
      targets = ["${v.host or "localhost"}:${toString v.port}"];
      labels = v.labels or {};
    }];
  };

in {
  options.monitoring.vmagent = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = true;
    };
    extraExporters = lib.mkOption {
      type = lib.types.attrs;
      default = {};
    };
  };

  config = lib.mkIf cfg.enable {
    sops.secrets.vmagent = {
      sopsFile = ./monitoring.sops.yaml;
    };

    monitoring.vmagent.extraExporters =
      (lib.mapAttrs' (n: v:
        {
          name = "node-${n}";
          value = {
            host = n;
            port = 9100;
            labels.container = n;
          };
        }) config.containers)
      // (lib.mapAttrs' (n: v:
        {
          name = "systemd-${n}";
          value = {
            host = n;
            port = 9558;
            labels.container = n;
          };
        }) config.containers)
      // { vmagent.port = 8429; };

    services.vmagent = {
      enable = true;
      remoteWrite = {
        url = "https://metrics.chaoswit.ch/api/v1/write";
        basicAuthUsername = "vmagent";
        basicAuthPasswordFile = config.sops.secrets.vmagent.path;
      };
      prometheusConfig = {
        scrape_configs = lib.mapAttrsToList scrape_config exporters;
        global = {
          external_labels = { instance = config.networking.fqdn; };
          scrape_interval = "15s";
        };
      };
      extraArgs = [
        "-enableTCP6"
      ];
    };

    services.prometheus.exporters = {
      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
      };
      systemd = {
        enable = true;
        extraFlags = [ "--systemd.collector.enable-ip-accounting" "--systemd.collector.enable-restart-count" ];
      };
      zfs.enable = true;
      bird.enable = config.services.bird.enable;
      knot.enable = config.services.knot.enable;
      postfix.enable = config.services.postfix.enable;
      modemmanager = {
        enable = config.networking.networkmanager.enable;
        refreshRate = config.services.vmagent.prometheusConfig.global.scrape_interval;
        user = "root";
      };
      nginx = {
        enable = config.services.nginx.enable;
      };
      scaphandre = {
        # don't enable power tracing in VMs
        enable = !lib.elem "virtio_pci" config.boot.initrd.availableKernelModules;
        port = 8149;
        user = "root";
        telemetryPath = "metrics";
      };
    };

    # scraper service config
    services.nginx.statusPage = true;

    boot.kernelModules = lib.mkIf (!lib.elem "virtio_pci" config.boot.initrd.availableKernelModules) [ "intel_rapl_common" ];
  };
}
