{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.crs-worker;
in {
  options.services.crs-worker = {
    enable = mkEnableOption "CRS Worker Scripts";
    scripts.recording-scheduler.enable = mkOption { type = types.bool; default = false; };
    scripts.mount4cut.enable = mkOption { type = types.bool; default = false; };
    scripts.cut-postprocessor.enable = mkOption { type = types.bool; default = false; };
    scripts.encoding.enable = mkOption { type = types.bool; default = false; };
    scripts.encoding.workerCount = mkOption { type = types.int; default = 1; };
    scripts.postencoding-auphonic.enable = mkOption { type = types.bool; default = false; };
    scripts.tagging.enable = mkOption { type = types.bool; default = false; };
    scripts.postprocessing-dummy.enable = mkOption { type = types.bool; default = false; };
    scripts.postprocessing-upload.enable = mkOption { type = types.bool; default = false; };

    sleep.work = mkOption { type = types.int; default = 2; };
    sleep.idle = mkOption { type = types.int; default = 30; };

    envFile = mkOption { type = types.str; default = "/etc/crs-worker/env"; };
    package = mkOption { type = types.package; default = pkgs.crs-worker; };
  };

  config = mkIf cfg.enable {
    # this breaks shit
    # nixpkgs.overlays = [ overlays.voc ];

    assertions = [
      { assertion = ! (cfg.scripts.postencoding-auphonic.enable && cfg.scripts.tagging.enable); message = "Enable only one Postencoding script"; }
      { assertion = ! (cfg.scripts.postprocessing-dummy.enable && cfg.scripts.postprocessing-upload.enable); message = "Enable only one Postprocessing/Upload script"; }
    ];

    users.users.crs-worker.uid = 912;
    users.users.crs-worker.isSystemUser = true;
    users.users.crs-worker.group = "crs-worker";
    users.groups.crs-worker.gid = 912;

    systemd.services = let
      script = scriptName: ''
        set +e
        export PATH=$PATH:/run/wrappers/bin
        while true; do
          ${cfg.package}/bin/${scriptName} $@
          EC=$?
          if [ $EC -eq 100 ]; then
            sleep ${toString cfg.sleep.work}
          elif [ $EC -ne 200 ]; then
            sleep ${toString cfg.sleep.idle}
          else
            sleep 300
          fi
        done
      '';
      unit = scriptName: {
        after = [ "network.target" ];
        startAt = "*-*-* *:00,05,10,15,20,25,30,35,40,45,50,55:42";
        script = script scriptName;
        serviceConfig = {
          StartLimitIntervalSec = 0;
          Restart = "on-failure";
          User = "crs-worker";
          Group = "crs-worker";
          EnvironmentFile = cfg.envFile;
          WorkingDirectory = "/video";
          Type = "simple";
        };
      };
    in {
      crs-recording-scheduler = mkIf cfg.scripts.recording-scheduler.enable
        (unit "script-A-recording-scheduler.pl");
      crs-mount4cut = mkIf cfg.scripts.mount4cut.enable
        (unit "script-B-mount4cut.pl");
      crs-cut-postprocessor = mkIf cfg.scripts.cut-postprocessor.enable
        (unit "script-C-cut-postprocessor.pl");
      crs-postencoding-auphonic = mkIf cfg.scripts.postencoding-auphonic.enable
        (unit "script-E-postencoding-auphonic.pl");
      crs-tagging = mkIf cfg.scripts.tagging.enable
        (unit "script-E-tagging.pl");
      crs-postprocessing-dummy = mkIf cfg.scripts.postprocessing-dummy.enable
        (unit "script-F-postprocessing-dummy.pl");
      crs-postprocessing-upload = mkIf cfg.scripts.postprocessing-upload.enable
        (unit "script-F-postprocessing-upload.pl");
    } // (listToAttrs (map
      (x: { name = "crs-encoding-${toString x}"; value = (unit "script-D-encoding.pl"); })
      (genList id cfg.scripts.encoding.workerCount)
    ));
  };
}
