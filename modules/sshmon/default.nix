{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.monitoring.sshmon;
  sshmonScript = pkgs.writeShellApplication {
    name = "sshmon";
    text = (readFile ./sshmon.sh);
  };
  sanitize = s: (concatMapStrings (s: if lib.isList s then "-" else s) (strings.split "[^a-zA-Z0-9]" s));
  commands = filterAttrs (_: c: c.enable) config.monitoring.sshmonServices;
in {
  users.users.sshmon = {
    isSystemUser = true;
    openssh.authorizedKeys.keys = [ ''command="${sshmonScript}/bin/sshmon" ${cfg.pubkey}'' ];
    home = "/var/empty";
    group = "sshmon";
    useDefaultShell = true;
  };
  users.groups.sshmon = {};

  environment.etc."sshmon.cfg".text = concatStringsSep "\n"
    (mapAttrsToList (n: c: "command[${sanitize n}]='${c.check_command}'") commands);

  security.sudo.extraRules = let
    sudoCommands = filter (hasPrefix "sudo ") (mapAttrsToList (_: c: c.check_command) commands);
  in [{
    users = [ "sshmon" ];
    commands = map (x: { command = removePrefix "sudo " x; options = [ "NOPASSWD" ]; }) sudoCommands;
  }];

  monitoring.services = mapAttrs (name: check: {
    check_command = if check.target == "self" then "sshmon" else "sshmon_other_host";
    "vars.run_check_on" = mkIf (check.target != "self") check.target;
    "vars.sshmon_timeout" = mkIf (check.timeout > 0) check.timeout;
    "vars.sshmon_port" = cfg.port;
    "vars.sshmon_command" = sanitize name;
  } // check.settings) commands;
}
