set -e

# we have our own handling for this
set +o nounset

UNKNOWN=3

unset command
declare -A command
# shellcheck disable=SC1091
. /etc/sshmon.cfg

if [[ -z "$SSH_ORIGINAL_COMMAND" ]]
then
    echo "No command given" >&2
    exit $UNKNOWN
fi

execute=${command["$SSH_ORIGINAL_COMMAND"]}
if [[ -z "$execute" ]]
then
    echo "Unknown command" >&2
    exit $UNKNOWN
fi

exec sh -c "$execute"
